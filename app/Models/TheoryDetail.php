<?php

namespace App\Models;

use Webpatser\Uuid\Uuid;
use Illuminate\Database\Eloquent\Model;

class TheoryDetail extends Model
{
    protected $table = 'theory_detail';
}