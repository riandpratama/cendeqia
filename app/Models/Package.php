<?php

namespace App\Models;

use Auth;
use Webpatser\Uuid\Uuid;
use Illuminate\Database\Eloquent\Model;

class Package extends Model
{
    protected $table = 'package';

    /**
     * Get the value indicating whether the IDs are incrementing.
     *
     * @return bool
     */
    public function getIncrementing()
    {
        return false;
    }

    /**
     * Get the route key for the model.
     *
     * @return string
     */
    public function getRouteKeyName()
    {
        return 'id';
    }

    /**
     *  Setup model event hooks
     */
    public static function boot()
    {
        parent::boot();
        self::creating(function ($model) {
            $model->id = (string) Uuid::generate(4);
        });
    }

    public function answer()
    {
        return $this->hasOne(QuestionUserDetail::class, 'package_id')->where('user_id', Auth::user()->id)->orderBy('end_time', 'desc');
    }

    public function answerKedinasan()
    {
        return $this->hasOne(QuestionUserDetailKD::class, 'package_id')->where('user_id', Auth::user()->id)->orderBy('end_time', 'desc');
    }

    public function question()
    {
        return $this->hasMany(Question::class, 'package_id');
    }
}
