<?php

namespace App\Models;

use DB;
use Webpatser\Uuid\Uuid;
use Illuminate\Database\Eloquent\Model;

class QuestionUserDetailKD extends Model
{
    protected $table = 'question_user_details_kd';

    protected $guarded = [];

    /**
     * Get the value indicating whether the IDs are incrementing.
     *
     * @return bool
     */
    public function getIncrementing()
    {
        return false;
    }

    /**
     * Get the route key for the model.
     *
     * @return string
     */
    public function getRouteKeyName()
    {
        return 'id';
    }

    /**
     *  Setup model event hooks
     */
    public static function boot()
    {
        parent::boot();
        self::creating(function ($model) {
            $model->id = (string) Uuid::generate(4);
        });
    }

    protected $dates = [
        'end_time'
    ];

    public function package()
    {
        return $this->belongsTo(Package::class, 'package_id')->withDefault();
    }

    public function twk()
    {
        return $this->hasMany(QuestionUserAnswerKD::class, 'question_user_detail_kd_id')->where('jenis', 'TWK');
    }

    public function tiu()
    {
        return $this->hasMany(QuestionUserAnswerKD::class, 'question_user_detail_kd_id')->where('jenis', 'TIU');
    }

    public function tkp()
    {
        return $this->hasMany(QuestionUserAnswerKD::class, 'question_user_detail_kd_id')->where('jenis', 'TKP');
    }

    public function total()
    {
        return $this->hasMany(QuestionUserAnswerKD::class, 'question_user_detail_kd_id');
    }

    public function user()
    {
        return $this->belongsTo('App\User', 'user_id')->withDefault();
    }
}
