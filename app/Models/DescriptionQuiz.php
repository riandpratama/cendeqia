<?php

namespace App\Models;

use Webpatser\Uuid\Uuid;
use Illuminate\Database\Eloquent\Model;

class DescriptionQuiz extends Model
{
    protected $table = 'description_quiz';
}