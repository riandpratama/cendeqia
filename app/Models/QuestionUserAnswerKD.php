<?php

namespace App\Models;

use Webpatser\Uuid\Uuid;
use Illuminate\Database\Eloquent\Model;

class QuestionUserAnswerKD extends Model
{
	protected $table = 'question_user_answers_kd';

	protected $guarded = [];

	public function question()
	{
		return $this->belongsTo(Question::class, 'question_id')->withDefault();
	}
}
