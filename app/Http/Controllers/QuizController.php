<?php

namespace App\Http\Controllers;

use DB;
use Auth;
use Illuminate\Http\Request;

use App\Models\Question;
use App\Models\QuestionUserDetail;
use App\Models\QuestionUserAnswer;
use App\Models\QuestionUserDetailKD;
use App\Models\QuestionUserAnswerKD;

class QuizController extends Controller
{
    public function quiz($id, $user_id)
    {
        $detail = QuestionUserDetail::findOrFail($id);

        $datas = QuestionUserAnswer::where('question_user_detail_id', $id)->orderBy('urut', 'asc')->get();

        return view('backend.quiz.index', compact('datas', 'detail'));
    }

    public function quizKedinasan($id, $user_id)
    {
        $detail = QuestionUserDetailKD::findOrFail($id);

        $datas = QuestionUserAnswerKD::where('question_user_detail_kd_id', $id)->orderBy('urut', 'asc')->get();

        return view('backend.quiz.index', compact('datas', 'detail'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function quizStore(Request $request, $id)
    {
        $data = QuestionUserAnswer::findOrFail($id);

        if ($request->jawab == 'a') {
            $data->update([
                'option_a' => 1,
                'option_b' => 0,
                'option_c' => 0,
                'option_d' => 0,
                'option_e' => 0,
                'answer' => $request->jawab,
                'point' => $data->question->value_a
            ]);
        } elseif ($request->jawab == 'b') {
            $data->update([
                'option_a' => 0,
                'option_b' => 1,
                'option_c' => 0,
                'option_d' => 0,
                'option_e' => 0,
                'answer' => $request->jawab,
                'point' => $data->question->value_b
            ]);
        } elseif ($request->jawab == 'c') {
            $data->update([
                'option_a' => 0,
                'option_b' => 0,
                'option_c' => 1,
                'option_d' => 0,
                'option_e' => 0,
                'answer' => $request->jawab,
                'point' => $data->question->value_c
            ]);
        } elseif ($request->jawab == 'd') {
            $data->update([
                'option_a' => 0,
                'option_b' => 0,
                'option_c' => 0,
                'option_d' => 1,
                'option_e' => 0,
                'answer' => $request->jawab,
                'point' => $data->question->value_d
            ]);
        } elseif ($request->jawab == 'e') {
            $data->update([
                'option_a' => 0,
                'option_b' => 0,
                'option_c' => 0,
                'option_d' => 0,
                'option_e' => 1,
                'answer' => $request->jawab,
                'point' => $data->question->value_e
            ]);
        } else {
            $data->update([
                'option_a' => 0,
                'option_b' => 0,
                'option_c' => 0,
                'option_d' => 0,
                'option_e' => 0,
                'point' => 0
            ]);
        }

        return "OK";
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function quizStoreKedinasan(Request $request, $id)
    {
        $data = QuestionUserAnswerKD::findOrFail($id);

        if ($request->jawab == 'a') {
            $data->update([
                'option_a' => 1,
                'option_b' => 0,
                'option_c' => 0,
                'option_d' => 0,
                'option_e' => 0,
                'answer' => $request->jawab,
                'point' => $data->question->value_a
            ]);
        } elseif ($request->jawab == 'b') {
            $data->update([
                'option_a' => 0,
                'option_b' => 1,
                'option_c' => 0,
                'option_d' => 0,
                'option_e' => 0,
                'answer' => $request->jawab,
                'point' => $data->question->value_b
            ]);
        } elseif ($request->jawab == 'c') {
            $data->update([
                'option_a' => 0,
                'option_b' => 0,
                'option_c' => 1,
                'option_d' => 0,
                'option_e' => 0,
                'answer' => $request->jawab,
                'point' => $data->question->value_c
            ]);
        } elseif ($request->jawab == 'd') {
            $data->update([
                'option_a' => 0,
                'option_b' => 0,
                'option_c' => 0,
                'option_d' => 1,
                'option_e' => 0,
                'answer' => $request->jawab,
                'point' => $data->question->value_d
            ]);
        } elseif ($request->jawab == 'e') {
            $data->update([
                'option_a' => 0,
                'option_b' => 0,
                'option_c' => 0,
                'option_d' => 0,
                'option_e' => 1,
                'answer' => $request->jawab,
                'point' => $data->question->value_e
            ]);
        } else {
            $data->update([
                'option_a' => 0,
                'option_b' => 0,
                'option_c' => 0,
                'option_d' => 0,
                'option_e' => 0,
                'point' => 0
            ]);
        }

        return "OK";
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function quizDone($id)
    {
        $detail = QuestionUserDetail::findOrFail($id);

        $detail->update([
            'status' => 1
        ]);

        return redirect()->route('store.quiz.value', $detail->id);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function quizDoneKedinasan($id)
    {
        $detail = QuestionUserDetailKD::findOrFail($id);

        $detail->update([
            'status' => 1
        ]);

        return redirect()->route('store.quiz.value.kedinasan', $detail->id);
    }

    public function quizValue($id)
    {
        $detail = QuestionUserDetail::findOrFail($id);

        $akumulasi = QuestionUserAnswer::select('jenis', DB::raw('SUM(point) as total_point'))->where('question_user_detail_id', $id)->groupBy('jenis')->get();
        $total = QuestionUserAnswer::select('question_user_detail_id', DB::raw('SUM(point) as total_point'))->where('question_user_detail_id', $id)->groupBy('question_user_detail_id')->first();

        return view('backend.quiz.show', compact('detail', 'akumulasi', 'total'));
    }

    public function quizValueKedinasan($id)
    {
        $detail = QuestionUserDetailKD::findOrFail($id);

        $akumulasi = QuestionUserAnswerKD::select('jenis', DB::raw('SUM(point) as total_point'))->where('question_user_detail_kd_id', $id)->groupBy('jenis')->get();
        $total = QuestionUserAnswerKD::select('question_user_detail_kd_id', DB::raw('SUM(point) as total_point'))->where('question_user_detail_kd_id', $id)->groupBy('question_user_detail_kd_id')->first();

        return view('backend.quiz.show', compact('detail', 'akumulasi', 'total'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function timeOver($id)
    {
        $datas = QuestionUserAnswer::where('question_user_detail_id', $id)->get();
        $detail = QuestionUserDetail::findOrFail($id);

        return view('backend.quiz.submit', compact('datas', 'id', 'detail'));
    }
}
