<?php

namespace App\Http\Controllers;

use DB;
use Illuminate\Http\Request;

use App\Models\QuestionUserAnswer;

class MainController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        return view('frontend.index');
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function about()
    {
        $data = DB::table('abouts')->first();

        return view('frontend.about', compact('data'));
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function faq()
    {
        $data = DB::table('faq')->orderBy('id', 'asc')->get();

        return view('frontend.faq', compact('data'));
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function contact()
    {
        $data = DB::table('contacts')->orderBy('id', 'asc')->get();

        return view('frontend.contact', compact('data'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request, $id)
    {
        $data = QuestionUserAnswer::findOrFail($id);
        // dd($data->question);
        if ($request->jawab == 'a') {
            $data->update([
                'option_a' => 1,
                'option_b' => 0,
                'option_c' => 0,
                'option_d' => 0,
                'option_e' => 0,
                'answer' => $request->jawab,
                'point' => $data->question->value_a
            ]);
        } elseif ($request->jawab == 'b') {
            $data->update([
                'option_a' => 0,
                'option_b' => 1,
                'option_c' => 0,
                'option_d' => 0,
                'option_e' => 0,
                'answer' => $request->jawab,
                'point' => $data->question->value_b
            ]);
        } elseif ($request->jawab == 'c') {
            $data->update([
                'option_a' => 0,
                'option_b' => 0,
                'option_c' => 1,
                'option_d' => 0,
                'option_e' => 0,
                'answer' => $request->jawab,
                'point' => $data->question->value_c
            ]);
        }elseif ($request->jawab == 'd') {
            $data->update([
                'option_a' => 0,
                'option_b' => 0,
                'option_c' => 0,
                'option_d' => 1,
                'option_e' => 0,
                'answer' => $request->jawab,
                'point' => $data->question->value_d
            ]);
        }elseif ($request->jawab == 'e') {
            $data->update([
                'option_a' => 0,
                'option_b' => 0,
                'option_c' => 0,
                'option_d' => 0,
                'option_e' => 1,
                'answer' => $request->jawab,
                'point' => $data->question->value_e
            ]);
        } else {
            $data->update([
                'option_a' => 0,
                'option_b' => 0,
                'option_c' => 0,
                'option_d' => 0,
                'option_e' => 0,
                'point' => 0
            ]);
        }

        return "OK";
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function review()
    {
        $datas = QuestionUserAnswer::all();
        $akumulasi = QuestionUserAnswer::select('jenis', DB::raw('SUM(point) as total_point'))->groupBy('jenis')->get();

        return view('review', compact('datas', 'akumulasi'));
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
