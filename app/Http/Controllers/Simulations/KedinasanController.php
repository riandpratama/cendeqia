<?php

namespace App\Http\Controllers\Simulations;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Session;
use App\User;
use App\Models\Question;
use App\Models\QuestionUserDetail;
use App\Models\QuestionUserAnswer;
use App\Models\DescriptionQuiz;

class KedinasanController extends Controller
{

  /**
   * Store a newly created resource in storage.
   *
   * @param  \Illuminate\Http\Request  $request
   * @return \Illuminate\Http\Response
   */
  public function store(Request $request)
  {
    $request->validate([
      'email' => ['required', 'string', 'email', 'max:255', 'unique:users'],
    ]);
    try {
      $user = User::create([
        'name' => $request->name,
        'email' => $request->email,
        'password' => bcrypt($request->phone),
        'phone' => $request->phone,
        'kedinasan' => 1,
        'isActive' => 0,
        'status' => 'simulation',
        'login_active' => 'kedinasan'
      ]);

      // $paket = DB::table('package')->where('urut', 3)->first()->id;
      // $data = Question::where('package_id', $paket)->orderBy('urut', 'asc')->get();

      // $detail = QuestionUserDetail::create([
      //   'package_id' => $paket,
      //   'user_id' => $user->id,
      //   'start_time' => \Carbon\Carbon::now()->format('Y-m-d H:i:s'),
      //   'end_time' => \Carbon\Carbon::now()->add(90, 'minutes')->format('Y-m-d H:i:s') //minutes
      // ]);

      // foreach ($data as $item) {
      //   QuestionUserAnswer::create([
      //     'question_user_detail_id' => $detail->id,
      //     'urut' => $item->urut,
      //     'user_id' => $user->id,
      //     'question_id' => $item->id,
      //     'jenis' => $item->jenis,
      //   ]);
      // }

      return redirect()->route('simulation.kedinasan.keterangan', [$user->id]);
    } catch (\Throwable $th) {
      return redirect()->back();
    }
  }

  public function keterangan($userId)
  {
    $paket = DB::table('package')->where('urut', 3)->first()->id;
    $descriptionQuiz = DescriptionQuiz::first();

    return view('simulation.kedinasan.petunjuk', compact('userId', 'paket', 'descriptionQuiz'));
  }

  public function keteranganStore($userId)
  {
    $paket = DB::table('package')->where('urut', 3)->first()->id;
    $data = Question::where('package_id', $paket)->orderBy('urut', 'asc')->get();
    $time = DB::table('times')->first();

    $detail = QuestionUserDetail::create([
      'package_id' => $paket,
      'user_id' => $userId,
      'start_time' => \Carbon\Carbon::now()->format('Y-m-d H:i:s'),
      'end_time' => \Carbon\Carbon::now()->add($time->time, 'minutes')->format('Y-m-d H:i:s') //minutes
    ]);

    foreach ($data as $item) {
      QuestionUserAnswer::create([
        'question_user_detail_id' => $detail->id,
        'urut' => $item->urut,
        'user_id' => $userId,
        'question_id' => $item->id,
        'jenis' => $item->jenis,
      ]);
    }

    return redirect()->route('simulation.kedinasan.quiz', [$detail->id, $userId]);
  }

  /**
   * Display a listing of the resource.
   *
   * @return \Illuminate\Http\Response
   */
  public function index($id, $userId)
  {
    $datas = QuestionUserAnswer::where('question_user_detail_id', $id)->orderBy('urut', 'asc')->get();
    $detail = QuestionUserDetail::findOrFail($id);
    $user = User::where('id', $userId)->first();

    return view('simulation.kedinasan.index', compact('datas', 'detail', 'user'));
  }

  /**
   * Update the specified resource in storage.
   *
   * @param  \Illuminate\Http\Request  $request
   * @param  int  $id
   * @return \Illuminate\Http\Response
   */
  public function quizStore(Request $request, $id)
  {
    $data = QuestionUserAnswer::findOrFail($id);

    if ($request->jawab == 'a') {
      $data->update([
        'option_a' => 1,
        'option_b' => 0,
        'option_c' => 0,
        'option_d' => 0,
        'option_e' => 0,
        'answer' => $request->jawab,
        'point' => $data->question->value_a
      ]);
    } elseif ($request->jawab == 'b') {
      $data->update([
        'option_a' => 0,
        'option_b' => 1,
        'option_c' => 0,
        'option_d' => 0,
        'option_e' => 0,
        'answer' => $request->jawab,
        'point' => $data->question->value_b
      ]);
    } elseif ($request->jawab == 'c') {
      $data->update([
        'option_a' => 0,
        'option_b' => 0,
        'option_c' => 1,
        'option_d' => 0,
        'option_e' => 0,
        'answer' => $request->jawab,
        'point' => $data->question->value_c
      ]);
    } elseif ($request->jawab == 'd') {
      $data->update([
        'option_a' => 0,
        'option_b' => 0,
        'option_c' => 0,
        'option_d' => 1,
        'option_e' => 0,
        'answer' => $request->jawab,
        'point' => $data->question->value_d
      ]);
    } elseif ($request->jawab == 'e') {
      $data->update([
        'option_a' => 0,
        'option_b' => 0,
        'option_c' => 0,
        'option_d' => 0,
        'option_e' => 1,
        'answer' => $request->jawab,
        'point' => $data->question->value_e
      ]);
    } else {
      $data->update([
        'option_a' => 0,
        'option_b' => 0,
        'option_c' => 0,
        'option_d' => 0,
        'option_e' => 0,
        'point' => 0
      ]);
    }

    return "OK";
  }

  /**
   * Store a newly created resource in storage.
   *
   * @param  \Illuminate\Http\Request  $request
   * @return \Illuminate\Http\Response
   */
  public function timeOver($id)
  {
    $datas = QuestionUserAnswer::where('question_user_detail_id', $id)->get();
    $detail = QuestionUserDetail::findOrFail($id);

    return view('simulation.kedinasan.submit', compact('datas', 'id', 'detail'));
  }

  /**
   * Store a newly created resource in storage.
   *
   * @param  \Illuminate\Http\Request  $request
   * @return \Illuminate\Http\Response
   */
  public function quizDone($id)
  {
    $detail = QuestionUserDetail::findOrFail($id);

    $detail->update([
      'status' => 1
    ]);

    return redirect()->route('simulation.kedinasan.store.quiz.value', $detail->id);
  }

  /**
   * Store a newly created resource in storage.
   *
   * @param  \Illuminate\Http\Request  $request
   * @return \Illuminate\Http\Response
   */
  public function quizValue($id)
  {
    $detail = QuestionUserDetail::findOrFail($id);

    $akumulasi = QuestionUserAnswer::select('jenis', DB::raw('SUM(point) as total_point'))->where('question_user_detail_id', $id)->groupBy('jenis')->get();
    $total = QuestionUserAnswer::select('question_user_detail_id', DB::raw('SUM(point) as total_point'))->where('question_user_detail_id', $id)->groupBy('question_user_detail_id')->first();

    return view('simulation.kedinasan.show-value', compact('detail', 'akumulasi', 'total'));
  }
}
