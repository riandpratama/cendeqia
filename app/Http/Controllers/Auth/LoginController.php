<?php

namespace App\Http\Controllers\Auth;

use DB;
use Auth;
use Session;
use App\User;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Providers\RouteServiceProvider;
use Illuminate\Foundation\Auth\AuthenticatesUsers;

class LoginController extends Controller
{
    /*
    |--------------------------------------------------------------------------
    | Login Controller
    |--------------------------------------------------------------------------
    |
    | This controller handles authenticating users for the application and
    | redirecting them to your home screen. The controller uses a trait
    | to conveniently provide its functionality to your applications.
    |
    */

    use AuthenticatesUsers;

    /**
     * Where to redirect users after login.
     *
     * @var string
     */
    protected $redirectTo = '/login';

    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('guest')->except('logout');
    }

    /**
     * Show the application's login form.
     *
     * @return \Illuminate\View\View
     */
    public function showLoginForm()
    {
        $dataSimulation = DB::table('simulations')->where('status', 'cpns')->first();

        return view('auth.login', compact('dataSimulation'));
    }

    /**
     * Post Login.
     *
     * @return string
     */
    public function postLogin(Request $request)
    {
        $check = DB::table('users')->where('email', $request->email)->first();

        if (!is_null($check)) {
            $user = User::findOrFail($check->id);
            if (!is_null($user->email_verified_at)) {
                if ($user->isActive == 0) {
                    Session::flash('Info', 'Akun anda sedang di proses oleh Admin.');
                    return redirect()->intended('/login');
                } else {
                    if (Auth::guard()->attempt(['email' => $request->email, 'password' => $request->password, 'cpns' => 1])) {
                        $user->update(['login_active' => 'cpns']);
                        return redirect()->intended('/home');
                    } else {
                        Session::flash('Info', 'Mohon Maaf, Email atau password salah!');
                        return redirect()->intended('/login');
                    }
                }
            } else {
                Session::flash('Info', 'Mohon Maaf, Anda belum melakukan verifikasi email!');
                return redirect()->intended('/login');
            }
        } else {
            Session::flash('Danger', 'Mohon Maaf, Akun anda tidak tesedia!');
            return redirect()->to('login');
        }
    }
}
