<?php

namespace App\Http\Controllers\Auth\Kedinasan;

use DB;
use Mail;
use Session;

use Illuminate\Support\Str;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Providers\RouteServiceProvider;
use App\User;
use Illuminate\Foundation\Auth\RegistersUsers;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Date;
use Illuminate\Support\Facades\Validator;

use App\Mail\EmailVerificationKedinasan;

class RegisterController extends Controller
{
  /*
    |--------------------------------------------------------------------------
    | Register Controller
    |--------------------------------------------------------------------------
    |
    | This controller handles the registration of new users as well as their
    | validation and creation. By default this controller uses a trait to
    | provide this functionality without requiring any additional code.
    |
    */

  use RegistersUsers;

  /**
   * Where to redirect users after registration.
   *
   * @var string
   */
  protected $redirectTo = RouteServiceProvider::HOME;

  /**
   * Create a new controller instance.
   *
   * @return void
   */
  public function __construct()
  {
    $this->middleware('guest');
  }

  /**
   * redirect to create register page.
   */
  public function index()
  {
    return view('auth.kedinasan.register');
  }

  /**
   * Create a new user instance after a valid registration.
   *
   * @param  array  $data
   * @return \App\User
   */
  public function store(Request $request)
  {
    Session::flash('email', $request->email);
    Session::flash('name', $request->name);

    $request->validate([
      'name' => ['required', 'string', 'max:255'],
      'email' => ['required', 'string', 'email', 'max:255', 'unique:users'],
      'password' => ['required', 'string', 'min:8', 'confirmed'],
    ]);

    try {
      $user = User::create([
        'id' => Str::uuid(),
        'name' => $request->name,
        'email' => $request->email,
        'status' => 'user',
        'kedinasan' => 1,
        'password' => Hash::make($request->password)
      ]);

      Mail::to($request->email)->send(new EmailVerificationKedinasan($user));

      Session::flash('Success', 'Registrasi Berhasil, Mohon verifikasi melalui email yang telah anda daftarkan!');

      return redirect()->to('/kedinasan/login');
    } catch (Exception $e) {
      return redirect()->back();
    }
  }

  public function userActivation($uuid)
  {
    $check = DB::table('users')->where('id', $uuid)->first();

    if (!is_null($check)) {
      $user = User::findOrFail($check->id);
      if (!is_null($user->email_verified_at)) {
        Session::flash('Info', 'Akun anda sudah verifikasi email!');
        return redirect()->to('kedinasan/login');
      }
      $user->update(['email_verified_at' => Date::now()]);
      Session::flash('Success', 'Akun Terverifikasi, Tunggu pihak Administrator memprosesnya.');
      return redirect()->to('kedinasan/login');
    }
    Session::flash('Danger', 'Mohon Maaf, Akun anda tidak tesedia!');
    return redirect()->to('kedinasan/login');
  }
}
