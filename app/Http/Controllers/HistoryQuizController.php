<?php

namespace App\Http\Controllers;

use DB;
use Auth;
use Illuminate\Http\Request;

use App\Models\Package;
use App\Models\Question;
use App\Models\QuestionUserDetail;
use App\Models\QuestionUserAnswer;
use App\Models\QuestionUserDetailKD;
use App\Models\QuestionUserAnswerKD;

class HistoryQuizController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $data = QuestionUserDetail::where('user_id', Auth::user()->id)->get();
        $dataKedinasan = QuestionUserDetailKD::where('user_id', Auth::user()->id)->get();
        $package = Package::with('answer')->orderBy('urut', 'asc')->get();
        $packagesEleven = Package::with('question')->where('urut', 11)->get();

        return view('backend.history-quiz.index', compact('data', 'package', 'dataKedinasan', 'packagesEleven'));
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function detail($id)
    {
        $data = QuestionUserDetail::where('user_id', Auth::user()->id)->where('package_id', $id)->orderBy('created_at', 'desc')->get();

        return view('backend.history-quiz.detail', compact('data'));
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function detailNew($id)
    {
        $data = QuestionUserDetail::where('user_id', Auth::user()->id)->where('package_id', $id)->orderBy('created_at', 'desc')->get();

        return view('backend.history-quiz.detail-new', compact('data'));
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function detailKedinasan($id)
    {
        $data = QuestionUserDetailKD::where('user_id', Auth::user()->id)->where('package_id', $id)->orderBy('created_at', 'desc')->get();

        return view('backend.history-quiz.detail', compact('data'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function show($id, $user_id)
    {
        $datas = QuestionUserAnswer::where('question_user_detail_id', $id)->get();
        $akumulasi = QuestionUserAnswer::select('jenis', DB::raw('SUM(point) as total_point'))->where('question_user_detail_id', $id)->groupBy('jenis')->get();

        return view('backend.history-quiz.show', compact('datas', 'akumulasi'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function showKedinasan($id, $user_id)
    {
        $datas = QuestionUserAnswerKD::where('question_user_detail_kd_id', $id)->get();
        $akumulasi = QuestionUserAnswerKD::select('jenis', DB::raw('SUM(point) as total_point'))->where('question_user_detail_kd_id', $id)->groupBy('jenis')->get();

        return view('backend.history-quiz.show', compact('datas', 'akumulasi'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request, $id)
    {
        $data = Question::where('package_id', $id)->get();

        $detail = QuestionUserDetail::create([
            'package_id' => $id,
            'user_id' => Auth::user()->id,
            'start_time' => \Carbon\Carbon::now()->format('Y-m-d H:i:s'),
            'end_time' => \Carbon\Carbon::now()->add(90, 'minutes')->format('Y-m-d H:i:s') //minutes
        ]);

        foreach ($data as $item) {
            QuestionUserAnswer::create([
                'question_user_detail_id' => $detail->id,
                'user_id' => Auth::user()->id,
                'question_id' => $item->id,
                'jenis' => $item->jenis,
            ]);
        }

        return redirect()->route('quiz', [$detail->id, Auth::user()->id]);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function storee(Request $request, $id)
    {
        $data = QuestionUserAnswer::findOrFail($id);

        if ($request->jawab == 'a') {
            $data->update([
                'option_a' => 1,
                'option_b' => 0,
                'option_c' => 0,
                'option_d' => 0,
                'option_e' => 0,
                'answer' => $request->jawab,
                'point' => $data->question->value_a
            ]);
        } elseif ($request->jawab == 'b') {
            $data->update([
                'option_a' => 0,
                'option_b' => 1,
                'option_c' => 0,
                'option_d' => 0,
                'option_e' => 0,
                'answer' => $request->jawab,
                'point' => $data->question->value_b
            ]);
        } elseif ($request->jawab == 'c') {
            $data->update([
                'option_a' => 0,
                'option_b' => 0,
                'option_c' => 1,
                'option_d' => 0,
                'option_e' => 0,
                'answer' => $request->jawab,
                'point' => $data->question->value_c
            ]);
        } elseif ($request->jawab == 'd') {
            $data->update([
                'option_a' => 0,
                'option_b' => 0,
                'option_c' => 0,
                'option_d' => 1,
                'option_e' => 0,
                'answer' => $request->jawab,
                'point' => $data->question->value_d
            ]);
        } elseif ($request->jawab == 'e') {
            $data->update([
                'option_a' => 0,
                'option_b' => 0,
                'option_c' => 0,
                'option_d' => 0,
                'option_e' => 1,
                'answer' => $request->jawab,
                'point' => $data->question->value_e
            ]);
        } else {
            $data->update([
                'option_a' => 0,
                'option_b' => 0,
                'option_c' => 0,
                'option_d' => 0,
                'option_e' => 0,
                'point' => 0
            ]);
        }

        return "OK";
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function review()
    {
        $datas = QuestionUserAnswer::all();
        $akumulasi = QuestionUserAnswer::select('jenis', DB::raw('SUM(point) as total_point'))->groupBy('jenis')->get();

        return view('review', compact('datas', 'akumulasi'));
    }



    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
