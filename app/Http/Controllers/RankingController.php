<?php

namespace App\Http\Controllers;

use DB;
use Auth;
use App\Models\Package;
use Illuminate\Http\Request;
use App\Models\QuestionUserDetail;
use Illuminate\Support\Facades\Hash;

class RankingController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $packages = Package::with('question')->orderBy('urut', 'asc')->get();

        return view('backend.ranking.index', compact('packages'));
    }

    /**
     * Show the form for editing the specified resource.
     */
    public function show($id)
    {
        $data = QuestionUserDetail::where('package_id', $id)->get();

        $data2 = DB::table('question_user_details')
            ->select('package_id', 'user_id')
            ->where('package_id', $id)
            ->groupBy('user_id', 'package_id')
            // ->orderBy('created_at', 'asc')
            ->get();

        // dd($data->toArray(), $data2->toArray());

        return view('backend.ranking.show', compact('data'));
    }
}
