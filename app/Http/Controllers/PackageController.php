<?php

namespace App\Http\Controllers;

use DB;
use Auth;
use Illuminate\Http\Request;

use App\Models\Package;
use App\Models\Question;
use App\Models\QuestionUserDetail;
use App\Models\QuestionUserAnswer;
use App\Models\DescriptionQuiz;

use App\Models\QuestionUserDetailKD;
use App\Models\QuestionUserAnswerKD;

class PackageController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $packages = Package::with('question')->orderBy('urut', 'asc')->get();
        $packagesEleven = Package::with('question')->where('urut', 11)->get();

        return view('backend.package.index', compact('packages', 'packagesEleven'));
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $package = DB::table('package')->where('id', $id)->first();
        $history = QuestionUserDetail::where('user_id', Auth::user()->id)->where('package_id', $id)->orderBy('end_time', 'desc')->first();
        $historyKedinasan = QuestionUserDetailKD::where('user_id', Auth::user()->id)->where('package_id', $id)->orderBy('end_time', 'desc')->first();
        $descriptionQuiz = DescriptionQuiz::first();

        return view('backend.package.show', compact('package', 'history', 'id', 'historyKedinasan', 'descriptionQuiz'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request, $id)
    {
        $data = Question::where('package_id', $id)->orderBy('urut', 'asc')->get();
        $time = DB::table('times')->first();

        $detail = QuestionUserDetail::create([
            'package_id' => $id,
            'user_id' => Auth::user()->id,
            'start_time' => \Carbon\Carbon::now()->format('Y-m-d H:i:s'),
            'end_time' => \Carbon\Carbon::now()->add($time->time, 'minutes')->format('Y-m-d H:i:s') //minutes
        ]);

        foreach ($data as $item) {
            QuestionUserAnswer::create([
                'question_user_detail_id' => $detail->id,
                'urut' => $item->urut,
                'user_id' => Auth::user()->id,
                'question_id' => $item->id,
                'jenis' => $item->jenis,
            ]);
        }

        return redirect()->route('quiz', [$detail->id, Auth::user()->id]);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function storeKedinasan($id)
    {
        $data = Question::where('package_id', $id)->orderBy('urut', 'asc')->get();

        $detail = QuestionUserDetailKD::create([
            'package_id' => $id,
            'user_id' => Auth::user()->id,
            'start_time' => \Carbon\Carbon::now()->format('Y-m-d H:i:s'),
            'end_time' => \Carbon\Carbon::now()->add(90, 'minutes')->format('Y-m-d H:i:s') //minutes
        ]);

        foreach ($data as $item) {
            QuestionUserAnswerKD::create([
                'question_user_detail_kd_id' => $detail->id,
                'urut' => $item->urut,
                'user_id' => Auth::user()->id,
                'question_id' => $item->id,
                'jenis' => $item->jenis,
            ]);
        }

        return redirect()->route('quiz.kedinasan', [$detail->id, Auth::user()->id]);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function storee(Request $request, $id)
    {
        $data = QuestionUserAnswer::findOrFail($id);

        if ($request->jawab == 'a') {
            $data->update([
                'option_a' => 1,
                'option_b' => 0,
                'option_c' => 0,
                'option_d' => 0,
                'option_e' => 0,
                'answer' => $request->jawab,
                'point' => $data->question->value_a
            ]);
        } elseif ($request->jawab == 'b') {
            $data->update([
                'option_a' => 0,
                'option_b' => 1,
                'option_c' => 0,
                'option_d' => 0,
                'option_e' => 0,
                'answer' => $request->jawab,
                'point' => $data->question->value_b
            ]);
        } elseif ($request->jawab == 'c') {
            $data->update([
                'option_a' => 0,
                'option_b' => 0,
                'option_c' => 1,
                'option_d' => 0,
                'option_e' => 0,
                'answer' => $request->jawab,
                'point' => $data->question->value_c
            ]);
        } elseif ($request->jawab == 'd') {
            $data->update([
                'option_a' => 0,
                'option_b' => 0,
                'option_c' => 0,
                'option_d' => 1,
                'option_e' => 0,
                'answer' => $request->jawab,
                'point' => $data->question->value_d
            ]);
        } elseif ($request->jawab == 'e') {
            $data->update([
                'option_a' => 0,
                'option_b' => 0,
                'option_c' => 0,
                'option_d' => 0,
                'option_e' => 1,
                'answer' => $request->jawab,
                'point' => $data->question->value_e
            ]);
        } else {
            $data->update([
                'option_a' => 0,
                'option_b' => 0,
                'option_c' => 0,
                'option_d' => 0,
                'option_e' => 0,
                'point' => 0
            ]);
        }

        return "OK";
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function review()
    {
        $datas = QuestionUserAnswer::all();
        $akumulasi = QuestionUserAnswer::select('jenis', DB::raw('SUM(point) as total_point'))->groupBy('jenis')->get();

        return view('review', compact('datas', 'akumulasi'));
    }



    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
