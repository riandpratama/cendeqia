<?php

namespace App\Http\Controllers;

use DB;
use Auth;
use Illuminate\Http\Request;

use App\Models\Photo;
use App\Models\Question;
use App\Models\QuestionUserDetail;
use App\Models\QuestionUserAnswer;

class HomeController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth');
    }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Contracts\Support\Renderable
     */
    public function index()
    {
        $packages = DB::table('package')->get();

        $photo1 = Photo::where('status', 1)->where('role', 1)->orderBy('id', 'asc')->get();
        $photo2 = Photo::where('status', 2)->where('role', 1)->orderBy('id', 'asc')->get();
        $photo3 = Photo::where('status', 3)->where('role', 1)->orderBy('id', 'asc')->get();

        $photok1 = Photo::where('status', 1)->where('role', 2)->orderBy('id', 'asc')->get();
        $photok2 = Photo::where('status', 2)->where('role', 2)->orderBy('id', 'asc')->get();
        $photok3 = Photo::where('status', 3)->where('role', 2)->orderBy('id', 'asc')->get();

        // dd($photok1->toArray(), $photok2->toArray(), $photok3->toArray());
        return view('home', compact('packages', 'photo1', 'photo2', 'photo3', 'photok1', 'photok2', 'photok3'));
    }

    public function detail($id)
    {
        $package = DB::table('package')->where('id', $id)->first();

        return view('detail', compact('package'));
    }

    public function store(Request $request, $id)
    {
        $data = Question::where('package_id', $id)->get();

        $detail = QuestionUserDetail::create([
            'package_id' => $id,
            'user_id' => Auth::user()->id,
            'start_time' => \Carbon\Carbon::now()->format('Y-m-d H:i:s'),
            'end_time' => \Carbon\Carbon::now()->add(90, 'minutes')->format('Y-m-d H:i:s') //minutes
        ]);

        foreach($data as $item) {
            QuestionUserAnswer::create([
                'question_user_detail_id' => $detail->id,
                'user_id' => Auth::user()->id,
                'question_id' => $item->id,
                'jenis' => $item->jenis,
            ]);
        }

        return redirect()->route('quiz', [$detail->id, Auth::user()->id]);
    }

    public function quiz($id, $user_id)
    {
        $detail = QuestionUserDetail::findOrFail($id);

        $datas = QuestionUserAnswer::where('question_user_detail_id', $id)->get();

        return view('quiz', compact('datas','detail'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function quizStore(Request $request, $id)
    {
        $data = QuestionUserAnswer::findOrFail($id);

        if ($request->jawab == 'a') {
            $data->update([
                'option_a' => 1,
                'option_b' => 0,
                'option_c' => 0,
                'option_d' => 0,
                'option_e' => 0,
                'answer' => $request->jawab,
                'point' => $data->question->value_a
            ]);
        } elseif ($request->jawab == 'b') {
            $data->update([
                'option_a' => 0,
                'option_b' => 1,
                'option_c' => 0,
                'option_d' => 0,
                'option_e' => 0,
                'answer' => $request->jawab,
                'point' => $data->question->value_b
            ]);
        } elseif ($request->jawab == 'c') {
            $data->update([
                'option_a' => 0,
                'option_b' => 0,
                'option_c' => 1,
                'option_d' => 0,
                'option_e' => 0,
                'answer' => $request->jawab,
                'point' => $data->question->value_c
            ]);
        } elseif ($request->jawab == 'd') {
            $data->update([
                'option_a' => 0,
                'option_b' => 0,
                'option_c' => 0,
                'option_d' => 1,
                'option_e' => 0,
                'answer' => $request->jawab,
                'point' => $data->question->value_d
            ]);
        } elseif ($request->jawab == 'e') {
            $data->update([
                'option_a' => 0,
                'option_b' => 0,
                'option_c' => 0,
                'option_d' => 0,
                'option_e' => 1,
                'answer' => $request->jawab,
                'point' => $data->question->value_e
            ]);
        } else {
            $data->update([
                'option_a' => 0,
                'option_b' => 0,
                'option_c' => 0,
                'option_d' => 0,
                'option_e' => 0,
                'point' => 0
            ]);
        }

        return "OK";
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function review($id, $user_id)
    {
        $datas = QuestionUserAnswer::where('question_user_detail_id', $id)->get();
        $akumulasi = QuestionUserAnswer::select('jenis', DB::raw('SUM(point) as total_point'))->where('question_user_detail_id', $id)->groupBy('jenis')->get();
        // dd($datas->toArray(), $akumulasi->toArray());
        return view('review', compact('datas', 'akumulasi'));
    }

     /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function history()
    {
        $data = QuestionUserDetail::where('user_id', Auth::user()->id)->get();

        return view('history', compact('data'));
    }
}
