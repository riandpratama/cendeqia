<?php

use Illuminate\Support\Facades\Route;

Route::get('/cache', function () {
	Artisan::call('config:cache');
});

Route::get('/', 'MainController@index');
Route::get('/faq', 'MainController@faq');
Route::get('/contact', 'MainController@contact');
Route::get('/about', 'MainController@about');
// Route::post('store/{id}', 'MainController@store');
// Route::get('review', 'MainController@review');

// Auth::routes();

//Simulation
Route::group(['namespace' => 'Simulations', 'prefix' => 'simulation'], function () {
	Route::post('create/cpns', 'CPNSController@store')->name('simulation.cpns.create');
	Route::get('petunjuk-cpns/{user_id}', 'CPNSController@keterangan')->name('simulation.cpns.keterangan');
	Route::post('petunjuk-cpns/{user_id}', 'CPNSController@keteranganStore')->name('simulation.cpns.keterangan.store');
	Route::get('cpns/{id}/{user_id}', 'CPNSController@index')->name('simulation.cpns.quiz');
	Route::post('store-quiz/cpns/store/{id}', 'CPNSController@quizStore');
	Route::get('time-over/cpns/{id}', 'CPNSController@timeOver');
	Route::post('done-quiz/cpns/store/{id}', 'CPNSController@quizDone')->name('simulation.cpns.store.quiz.done');
	Route::get('value-quiz/cpns/{id}', 'CPNSController@quizValue')->name('simulation.cpns.store.quiz.value');

	Route::post('create/kedinasan', 'KedinasanController@store')->name('simulation.kedinasan.create');
	Route::get('petunjuk-kedinasan/{user_id}', 'KedinasanController@keterangan')->name('simulation.kedinasan.keterangan');
	Route::post('petunjuk-kedinasan/{user_id}', 'KedinasanController@keteranganStore')->name('simulation.kedinasan.keterangan.store');
	Route::get('kedinasan/{id}/{user_id}', 'KedinasanController@index')->name('simulation.kedinasan.quiz');
	Route::post('store-quiz/kedinasan/store/{id}', 'KedinasanController@quizStore');
	Route::get('time-over/kedinasan/{id}', 'KedinasanController@timeOver');
	Route::post('done-quiz/kedinasan/store/{id}', 'KedinasanController@quizDone')->name('simulation.kedinasan.store.quiz.done');
	Route::get('value-quiz/kedinasan/{id}', 'KedinasanController@quizValue')->name('simulation.kedinasan.store.quiz.value');
});

Route::group(['namespace' => 'Auth'], function () {
	Route::get('register', 'RegisterController@index')->name('register');
	Route::post('register', 'RegisterController@store')->name('register.create');

	Route::get('email/resend', 'VerificationController@resend')->name('verification.resend');
	Route::get('email/verify', 'VerificationController@show')->name('verification.notice');
	Route::get('email/verify/{id}', 'VerificationController@verify')->name('verification.verify');

	// Route::post('password/email', 'ForgotPasswordController@sendResetLinkEmail')->name('password.email');
	// Route::get('password/reset', 'ForgotPasswordController@showLinkRequestForm')->name('password.request');
	// Route::get('password/reset/{token}', 'ResetPasswordController@showResetForm')->name('password.reset');

	Route::get('verify/user/{id}', 'RegisterController@userActivation')->name('verify.user');

	Route::get('login', 'LoginController@showLoginForm')->name('login');
	Route::post('login', 'LoginController@postLogin');
	Route::post('logout', 'LoginController@logout')->name('logout');

	Route::get('kedinasan/register', 'Kedinasan\RegisterController@index')->name('kedinasan.register');
	Route::post('kedinasan/register', 'Kedinasan\RegisterController@store')->name('kedinasan.register.create');
	Route::get('kedinasan/login', 'Kedinasan\LoginController@showLoginForm')->name('kedinasan.login');
	Route::post('kedinasan/login', 'Kedinasan\LoginController@postLogin')->name('kedinasan.login.post');
	Route::get('kedinasan/verify/user/{id}', 'Kedinasan\RegisterController@userActivation')->name('kedinasan.verify.user');

	Route::post('password/reset', 'ResetPasswordController@reset')->name('password.update');

	Route::get('password/reset', 'ForgotPasswordController@showLinkRequestForm')->name('password.request');
	Route::post('password/email', 'ForgotPasswordController@sendResetLinkEmail')->name('password.email');
	Route::get('password/reset/{token}', 'ResetPasswordController@showResetForm')->name('password.reset');
});

Route::group(['middleware' => 'auth'], function () {
	Route::get('home', 'HomeController@index')->name('home');
	Route::get('detail/{id}', 'HomeController@detail')->name('detail');
	Route::post('detail/store/{id}', 'HomeController@store')->name('store');

	Route::get('review-quiz/{id}/{user_id}', 'HomeController@review')->name('review.quiz');

	// Route::get('history-quiz', 'HomeController@history')->name('history.quiz');

	Route::get('package', 'PackageController@index')->name('package.index');
	Route::get('package/{id}/show', 'PackageController@show')->name('package.show');
	Route::post('package/{id}/store', 'PackageController@store')->name('package.store');
	Route::post('package/{id}/store/kedinasan', 'PackageController@storeKedinasan')->name('package.store.kedinasan');

	Route::get('quiz/{id}/{user_id}', 'QuizController@quiz')->name('quiz');
	Route::post('store-quiz/store/{id}', 'QuizController@quizStore')->name('store.quiz');
	Route::post('done-quiz/store/{id}', 'QuizController@quizDone')->name('store.quiz.done');
	Route::get('value-quiz/{id}', 'QuizController@quizValue')->name('store.quiz.value');
	Route::get('time-over/{id}', 'QuizController@timeOver');

	Route::get('quiz/{id}/{user_id}/kedinasan', 'QuizController@quizKedinasan')->name('quiz.kedinasan');
	Route::post('store-quiz/kedinasan/store/{id}', 'QuizController@quizStoreKedinasan')->name('store.quiz.kedinasan');
	Route::post('done-quiz/store/{id}/kedinasan', 'QuizController@quizDoneKedinasan')->name('store.quiz.done.kedinasan');
	Route::get('value-quiz/{id}/kedinasan', 'QuizController@quizValueKedinasan')->name('store.quiz.value.kedinasan');

	Route::get('history-quiz', 'HistoryQuizController@index')->name('history.quiz');
	Route::get('history-quiz-detail/{id}/{user_id}', 'HistoryQuizController@detail')->name('history.quiz.detail');
	Route::get('history-quiz-details/{id}/{user_id}', 'HistoryQuizController@detailNew')->name('history.quiz.detail.new');
	Route::get('history-quiz-show/{id}/{user_id}', 'HistoryQuizController@show')->name('history.show');

	Route::get('history-quiz-show/{id}/{user_id}/kedinasan', 'HistoryQuizController@showKedinasan')->name('history.show.kedinasan');
	Route::get('history-quiz-detail/{id}/{user_id}/kedinasan', 'HistoryQuizController@detailKedinasan')->name('history.quiz.detail.kedinasan');

	Route::get('kritik-saran', 'KritikSaranController@index')->name('kritik-saran.index');
	Route::get('kritik-saran/create', 'KritikSaranController@create')->name('kritik-saran.create');
	Route::post('kritik-saran/store', 'KritikSaranController@store')->name('kritik-saran.store');

	Route::get('account', 'AccountController@index')->name('account.index');
	Route::get('account/edit', 'AccountController@edit')->name('account.edit');
	Route::post('account/update', 'AccountController@update')->name('account.update');
	Route::get('account/change-password/edit', 'AccountController@editChangePassword')->name('account.editChangePassword');
	Route::post('account/change-password/update', 'AccountController@updateChangePassword')->name('account.updateChangePassword');

	Route::get('ranking', 'RankingController@index')->name('ranking.index');
	Route::get('ranking/{id}/show', 'RankingController@show')->name('ranking.show');

	Route::get('theory', 'TheoryController@index')->name('theory.index');
	Route::get('theory/{id}/show', 'TheoryController@show')->name('theory.show');
	// Route::get('theory/show-twk', 'TheoryController@showTWK')->name('theory.showTWK');
	// Route::get('theory/show-tkp', 'TheoryController@showTKP')->name('theory.showTKP');
	// Route::get('theory/show-tiu', 'TheoryController@showTIU')->name('theory.showTIU');
});
