@extends('template.default')
<style>
  .card-body {
    flex: 1 1 auto;
    padding: 0.5rem !important;
  }
</style>
@section('content')

<div class="app-page-title">
    <div class="page-title-wrapper">
        <div class="page-title-heading">
            <div class="page-title-icon">
                <i class="pe-7s-rocket icon-gradient bg-strong-bliss">
                </i>
            </div>
            <div>Selamat Datang <b>{{ Auth::user()->name }} </b>
            </div>
        </div>
    </div>
</div>

<div class="">
    <div class="row">
      @if (Auth::user()->login_active === 'cpns')
      <div class="col-md-4">
        <div class="main-card mb-3 card">
            <div class="card-body">
                <div id="carouselExampleControls1" class="carousel slide" data-ride="carousel">
                    <div class="carousel-inner">
                      @foreach($photo1 as $item)
                        <div class="carousel-item {{$item->active}}">
                          <img class="d-block w-100" src="{{ config('app.image') }}/{{ $item->image_path }}" alt="First slide">
                        </div>
                      @endforeach
                    </div>
                    <a class="carousel-control-prev" href="#carouselExampleControls1" role="button" data-slide="prev">
                        <span class="carousel-control-prev-icon" aria-hidden="true"></span>
                        <span class="sr-only">Previous</span>
                    </a>
                    <a class="carousel-control-next" href="#carouselExampleControls1" role="button" data-slide="next">
                        <span class="carousel-control-next-icon" aria-hidden="true"></span>
                        <span class="sr-only">Next</span>
                    </a>
                </div>
            </div>
        </div>
      </div>
      <div class="col-md-4">
        <div class="main-card mb-3 card">
            <div class="card-body">
                <div id="carouselExampleControls2" class="carousel slide" data-ride="carousel">
                    <div class="carousel-inner">
                      @foreach($photo2 as $item)
                        <div class="carousel-item {{$item->active}}">
                          <img class="d-block w-100" src="{{ config('app.image') }}/{{ $item->image_path }}" alt="First slide">
                        </div>
                      @endforeach
                    </div>
                    <a class="carousel-control-prev" href="#carouselExampleControls2" role="button" data-slide="prev">
                        <span class="carousel-control-prev-icon" aria-hidden="true"></span>
                        <span class="sr-only">Previous</span>
                    </a>
                    <a class="carousel-control-next" href="#carouselExampleControls2" role="button" data-slide="next">
                        <span class="carousel-control-next-icon" aria-hidden="true"></span>
                        <span class="sr-only">Next</span>
                    </a>
                </div>
            </div>
        </div>
      </div>
      <div class="col-md-4">
        <div class="main-card mb-3 card">
            <div class="card-body">
                <div id="carouselExampleControls3" class="carousel slide" data-ride="carousel">
                    <div class="carousel-inner">
                      @foreach($photo3 as $item)
                        <div class="carousel-item {{$item->active}}">
                          <img class="d-block w-100" src="{{ config('app.image') }}/{{ $item->image_path }}" alt="{{$item->id}} slide">
                        </div>
                      @endforeach
                    </div>
                    <a class="carousel-control-prev" href="#carouselExampleControls3" role="button" data-slide="prev">
                        <span class="carousel-control-prev-icon" aria-hidden="true"></span>
                        <span class="sr-only">Previous</span>
                    </a>
                    <a class="carousel-control-next" href="#carouselExampleControls3" role="button" data-slide="next">
                        <span class="carousel-control-next-icon" aria-hidden="true"></span>
                        <span class="sr-only">Next</span>
                    </a>
                </div>
            </div>
        </div>
      </div>
      @elseif (Auth::user()->login_active === 'kedinasan')
      <div class="col-md-4">
        <div class="main-card mb-3 card">
            <div class="card-body">
                <div id="carouselExampleControls1" class="carousel slide" data-ride="carousel">
                    <div class="carousel-inner">
                    @foreach($photok1 as $item)
                        <div class="carousel-item {{$item->active}}">
                          <img class="d-block w-100" src="{{ config('app.image') }}/{{ $item->image_path }}" alt="First slide">
                        </div>
                    @endforeach
                    <a class="carousel-control-prev" href="#carouselExampleControls1" role="button" data-slide="prev">
                        <span class="carousel-control-prev-icon" aria-hidden="true"></span>
                        <span class="sr-only">Previous</span>
                    </a>
                    <a class="carousel-control-next" href="#carouselExampleControls1" role="button" data-slide="next">
                        <span class="carousel-control-next-icon" aria-hidden="true"></span>
                        <span class="sr-only">Next</span>
                    </a>
                </div>
            </div>
        </div>
      </div>
      <div class="col-md-4">
        <div class="main-card mb-3 card">
            <div class="card-body">
                <div id="carouselExampleControls2" class="carousel slide" data-ride="carousel">
                    <div class="carousel-inner">
                    @foreach($photok2 as $item)
                        <div class="carousel-item {{$item->active}}">
                          <img class="d-block w-100" src="{{ config('app.image') }}/{{ $item->image_path }}" alt="First slide">
                        </div>
                    @endforeach
                    <a class="carousel-control-prev" href="#carouselExampleControls2" role="button" data-slide="prev">
                        <span class="carousel-control-prev-icon" aria-hidden="true"></span>
                        <span class="sr-only">Previous</span>
                    </a>
                    <a class="carousel-control-next" href="#carouselExampleControls2" role="button" data-slide="next">
                        <span class="carousel-control-next-icon" aria-hidden="true"></span>
                        <span class="sr-only">Next</span>
                    </a>
                </div>
            </div>
        </div>
      </div>
      <div class="col-md-4">
        <div class="main-card mb-3 card">
            <div class="card-body">
                <div id="carouselExampleControls3" class="carousel slide" data-ride="carousel">
                    <div class="carousel-inner">
                    @foreach($photok3 as $item)
                        <div class="carousel-item {{$item->active}}">
                          <img class="d-block w-100" src="{{ config('app.image') }}/{{ $item->image_path }}" alt="First slide">
                        </div>
                    @endforeach
                    <a class="carousel-control-prev" href="#carouselExampleControls3" role="button" data-slide="prev">
                        <span class="carousel-control-prev-icon" aria-hidden="true"></span>
                        <span class="sr-only">Previous</span>
                    </a>
                    <a class="carousel-control-next" href="#carouselExampleControls3" role="button" data-slide="next">
                        <span class="carousel-control-next-icon" aria-hidden="true"></span>
                        <span class="sr-only">Next</span>
                    </a>
                </div>
            </div>
        </div>
      </div>
      @endif
    </div>
</div>
@endsection



{{-- <!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <link rel="icon" type="image/ico" href="http://cendeqia.com/dev/public/images/logo/favicon.ico">
    <link rel="stylesheet" href="http://cendeqia.com/dev/public/css/font-awesome.min.css">
    <!--[if IE]>
    <link rel="shortcut icon" href="/favicon.ico" type="image/vnd.microsoft.icon">
    <![endif]-->

    <!-- CSRF Token -->
    <meta name="csrf-token" content="tQoLjzQzVxRTXGz6kZF0ebPa1ucabDuhpdLjqEd7">

    <title>Cendeqia</title>

    <link rel="preconnect" href="https://fonts.gstatic.com">
    <link href="https://fonts.googleapis.com/css2?family=Lato:ital,wght@0,300;0,400;0,700;0,900;1,300;1,400;1,700&display=swap" rel="stylesheet">

    <!-- Styles -->
  <link href="http://cendeqia.com/dev/public/css/app.css" rel="stylesheet">
  <link rel="stylesheet" href="http://cendeqia.com/dev/public/css/font-awesome.min.css">
  <script type="text/javascript">
    window.Laravel =  {"csrfToken":"tQoLjzQzVxRTXGz6kZF0ebPa1ucabDuhpdLjqEd7"}  </script>
  <link rel="stylesheet" href="http://cendeqia.com/dev/public/css/styles.css">
</head>
<body>
    <div id="app" style="position: relative;">
      <nav class="navbar navbar-default navbar-static-top bg-white">
        <div class="nav-bar">
          <div class="container">
            <div class="row">
              <div class="col-md-6">
                <div class="navbar-header">
                    <h4 class="heading">
                    <a href="http://cendeqia.com/dev/public" title="Cendeqia">
                      <img src="http://cendeqia.com/dev/public/images/logo/logo_1606580724Artboard 1.png" class="img-responsive" alt="Cendeqia">
                    </a>
                  </h4>
                </div>
              </div>
              <div class="col-md-6">
                <div class="collapse navbar-collapse" id="app-navbar-collapse">
                  <ul class="nav navbar-nav navbar-right">
                    <!-- Authentication Links -->
                      <li class="dropdown">
                        <a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-expanded="false" aria-haspopup="true">
                          {{ Auth::user()->name }} <span class="caret"></span>
                        </a>
                        <ul class="dropdown-menu">
                          <li><a href="http://cendeqia.com/dev/public/profil" title="Profil">Profil</a></li>
                          <li>
                            <a href="{{ route('logout') }}"
                              onclick="event.preventDefault();
                              document.getElementById('logout-form').submit();">
                              Logout
                            </a>
                            <form id="logout-form" action="{{ route('logout') }}" method="POST" style="display: none;">
                              @csrf
                            </form>
                          </li>
                        </ul>
                      </li>
                      <li><a href="http://cendeqia.com/dev/public/pages/about-us">TENTANG</a></li>
                      <li><a href="http://cendeqia.com/dev/public/faqs">FAQ</a></li>
                      <li><a href="http://cendeqia.com/dev/public/pages/contact-us">KONTAK</a></li>
                  </ul>
                </div>
              </div>
            </div>
          </div>
        </div>
      </nav>
      <div class="outers_wrap_dashboard ins_page">
        <div class="container">
              <div class="row">
                <div class="col-md-3">
                  <div class="blocks_left_menu outer_left_m">
                    <div class="py-3"></div>
                      <ul class="list-unstyled">
                          <li class="disabled">- Akun</li>
                          <li class="disabled" ><a href="{{ route('home') }}">- Paket Soal</a></li>
                          <li class="disabled"><a href="{{ route('history.quiz') }}">- History</a></li>
                          <li class="disabled">- Materi</li>
                          <li class="disabled">- Rapor</li>
                          <li class="disabled">- Saran/ Komentar</li>
                      </ul>
                  </div>
                </div>
                <div class="col-md-9 back-white2">
                  <div class="p-4 blocks_def_listpakets">
                    <div class="py-2"></div>
                        <div class="row">
                          @foreach($packages as $item)
                          <div class="col-md-2">
                            <div class="items box-itm  actived ">
                              <a href="{{ route('detail', $item->id) }}">
                              <span class="top">Paket</span>
                              <div class="clear"></div>
                              <span class="names"> {{ $item->name }}</span>
                              </a>
                            </div>
                          </div>
                          @endforeach
                        </div>
                      <div class="py-5"></div>
                      <div class="py-5"></div>
                    <div class="clear clearfix"></div>
                  </div>
                  <div class="py-3"></div>
                </div>
              </div>
          </div>
        </div>
        <br>
        <br>
      </div>
      <div class="flying_foot">
        <div class="container" >
            <div class="row">
                <div class="col-md-6">
                    © 2021 Cendeqia.id All Rights Reserved.
                </div>
                <div class="col-md-6">
                </div>
            </div>
        </div>
    </div>
    <!-- Scripts -->
    <script src="http://cendeqia.com/dev/public/js/app.js"></script>

  <script type="text/javascript">
     $( document ).ready(function() {
         $('.sessionmodal').addClass("active");
         setTimeout(function() {
             $('.sessionmodal').removeClass("active");
        }, 4500);

        $('body').addClass('home_dashboard');
      });
  </script>
<style type="text/css">
  .navbar-default{
    margin-bottom: 0 !important;
  }
</style>

</body>
</html>
 --}}