<!DOCTYPE html>
<html lang="en">

<head>
  <meta charset="utf-8">
  <meta content="width=device-width, initial-scale=1.0" name="viewport">

  <title>CENDEQIA - Reset Password</title>
  <meta content="" name="descriptison">
  <meta content="" name="keywords">

  <!-- Favicons -->
  <link href="{{ asset('frontend/assets/image/ICON.png')}}" rel="icon">
  <link href="{{ asset('frontend/assets/img/apple-touch-icon.png')}}" rel="apple-touch-icon">

  <!-- Google Fonts -->
  <link href="https://fonts.googleapis.com/css?family=Open+Sans:300,300i,400,400i,600,600i,700,700i|Raleway:300,300i,400,400i,500,500i,600,600i,700,700i|Poppins:300,300i,400,400i,500,500i,600,600i,700,700i" rel="stylesheet">

  <!-- Vendor CSS Files -->
  <link href="{{ asset('frontend/assets/vendor/bootstrap/css/bootstrap.min.css') }}" rel="stylesheet">
  <link href="{{ asset('frontend/assets/vendor/icofont/icofont.min.css') }}" rel="stylesheet">
  <link href="{{ asset('frontend/assets/vendor/boxicons/css/boxicons.min.css') }}" rel="stylesheet">
  <link href="{{ asset('frontend/assets/vendor/animate.css/animate.min.css') }}" rel="stylesheet">
  <link href="{{ asset('frontend/assets/vendor/remixicon/remixicon.css') }}" rel="stylesheet">
  <link href="{{ asset('frontend/assets/vendor/owl.carousel/assets/owl.carousel.min.css') }}" rel="stylesheet">
  <link href="{{ asset('frontend/assets/vendor/venobox/venobox.css') }}" rel="stylesheet">
  <link href="{{ asset('frontend/assets/vendor/aos/aos.css') }}" rel="stylesheet">

  <!-- Template Main CSS File -->
  <link rel="stylesheet" href="{{ asset('login-register/fonts/material-icon/css/material-design-iconic-font.min.css') }}">
  <link rel="stylesheet" href="{{ asset('login-register/css/style.css') }}">

  <link href="{{ asset('frontend/assets/css/style.css') }}" rel="stylesheet">

  <style>
    .form-group:last-child {
        margin-bottom: 50px !important;
    }
    .form-submit {
        padding: 10px 40px !important;
    }
  </style>  
</head>

<body style="background-color: #f1f8ff;">
  <header id="header" class="fixed-top " style="background-color: #2A75AE">
    <div class="container d-flex align-items-center" style="width: none; background: none;">
      <h1 class="logo mr-auto" style="font-weight: bold;color: #2A75AE;">
      <img src="{{ asset('frontend/assets/image/LOGO.png') }}" style="width: 30%" alt="">
      </h1>
      <nav class="nav-menu d-none d-lg-block">
        <ul>
          <li><a href="/">HOME</a></li>
          <li><a href="/about">TENTANG</a></li>
          <li><a href="/faq">FAQ</a></li>
          <li><a href="/contact">KONTAK</a></li>
        </ul>
      </nav>
    </div>
  </header>

  <main id="main">
    <section id="icon-boxes" class="icon-boxes sign-in" style="padding-top: 10px;"></section>
    <section id="why-us" class="why-us" style="padding-top: 40px;padding-bottom:30px;">
        <div class="container">
            <div class="row">
                <div class="col-lg-7 d-flex flex-column justify-content-center align-items-stretch aos-init aos-animate" data-aos="fade-left">
                    <div class="content" data-aos="fade-left" data-aos-delay="100">
                        @if (session('status'))
                            <div class="alert alert-success" role="alert">
                                {{ session('status') }}
                            </div>
                        @endif
                        @if (Session::has('Danger'))
                            <div class="alert alert-danger">
                                {{ $message }}
                            </div>
                        @endif

                        <h4 class="form-title"><b>Reset Password</b></h4>
                        
                        <form method="POST" class="register-form" id="reset-password-form" action="{{ route('password.email') }}">
                            @csrf
                            <div class="form-group">
                                <label for="email"><i class="zmdi zmdi-account material-icons-name"></i></label>
                                <input type="email" name="email" id="email" placeholder="Email" autocomplete="off" required="" />
                            </div>
                            <div class="form-group has-feedback">
                                <div id="captcha"></div>
                            </div>
                            <div class="form-group form-button">
                                <input type="button" name="signin" id="signin" class="form-submit" value="RESET" style="background: #2A75AE;" onclick="verifyCaptcha(grecaptcha.getResponse(widget))" />
                                &nbsp;&nbsp;&nbsp;&nbsp;
                                <a href="{{ route('login') }}" id="signin" class="form-submit" style="background: #FCB71A; color: white;"> LOGIN</a>
                            </div>
                        </form>
                    </div>
                </div>

                <div class="col-lg-5 align-items-stretch position-relative video-box aos-init aos-animate" 
                    style="background-image: url({{ asset('frontend/assets/image/Thumbnail-Login.jpg') }});border-radius: 0px 15px 15px 0px;" 
                    data-aos="fade-right">
                    <a href="https://youtu.be/uE6M-vaWvR4" class="venobox play-btn mb-4 vbox-item" data-vbtype="video" data-autoplay="true"></a>
                </div>
            </div>
        </div>
    </section>
  </main>

  <a href="#" class="back-to-top"><i class="ri-arrow-up-line"></i></a>
  <div id="preloader"></div>

  <!-- Vendor JS Files -->
  <script src="{{ asset('frontend/assets/vendor/jquery/jquery.min.js') }}"></script>
  <script src="{{ asset('frontend/assets/vendor/bootstrap/js/bootstrap.bundle.min.js') }}"></script>
  <script src="{{ asset('frontend/assets/vendor/jquery.easing/jquery.easing.min.js') }}"></script>
  <script src="{{ asset('frontend/assets/vendor/php-email-form/validate.js') }}"></script>
  <script src="{{ asset('frontend/assets/vendor/owl.carousel/owl.carousel.min.js') }}"></script>
  <script src="{{ asset('frontend/assets/vendor/venobox/venobox.min.js') }}"></script>
  <script src="{{ asset('frontend/assets/vendor/isotope-layout/isotope.pkgd.min.js') }}"></script>
  <script src="{{ asset('frontend/assets/vendor/aos/aos.js') }}"></script>

  <!-- Template Main JS File -->
  <script src="{{ asset('frontend/assets/js/main.js') }}"></script>
  {{-- <script src="{{ asset('frontend/assets/js/main.js') }}"></script> --}}
  <script src="https://www.google.com/recaptcha/api.js?render=reCAPTCHA_site_key"></script>
    <script src="https://www.google.com/recaptcha/api.js?onload=onloadCallback&render=explicit" async defer></script>
    <script type="text/javascript">
        var widget;
        var onloadCallback = function () {
            widget =
                grecaptcha.render('captcha', {
                    'sitekey': '{{ config('app.recaptcha') }}',
                    'theme': 'light'
                });
        };
        function verifyCaptcha(result) {
            if (result != "") {
                $('#reset-password-form').submit();
            } else {
                alert('Harap isi reCAPTCHA terlebih dahulu');
            }
        }
    </script>
</body>

</html>