<!DOCTYPE html>
<html lang="en">

<head>
  <meta charset="utf-8">
  <meta content="width=device-width, initial-scale=1.0" name="viewport">

  <title>CENDEQIA - Login Kedinasan</title>
  <meta content="" name="descriptison">
  <meta content="" name="keywords">

  <!-- Favicons -->
  <link href="{{ asset('frontend/assets/image/ICON.png')}}" rel="icon">
  <link href="{{ asset('frontend/assets/img/apple-touch-icon.png')}}" rel="apple-touch-icon">

  <!-- Google Fonts -->
  <link href="https://fonts.googleapis.com/css?family=Open+Sans:300,300i,400,400i,600,600i,700,700i|Raleway:300,300i,400,400i,500,500i,600,600i,700,700i|Poppins:300,300i,400,400i,500,500i,600,600i,700,700i" rel="stylesheet">

  <!-- Vendor CSS Files -->
  <link href="{{ asset('frontend/assets/vendor/bootstrap/css/bootstrap.min.css') }}" rel="stylesheet">
  <link href="{{ asset('frontend/assets/vendor/icofont/icofont.min.css') }}" rel="stylesheet">
  <link href="{{ asset('frontend/assets/vendor/boxicons/css/boxicons.min.css') }}" rel="stylesheet">
  <link href="{{ asset('frontend/assets/vendor/animate.css/animate.min.css') }}" rel="stylesheet">
  <link href="{{ asset('frontend/assets/vendor/remixicon/remixicon.css') }}" rel="stylesheet">
  <link href="{{ asset('frontend/assets/vendor/owl.carousel/assets/owl.carousel.min.css') }}" rel="stylesheet">
  <link href="{{ asset('frontend/assets/vendor/venobox/venobox.css') }}" rel="stylesheet">
  <link href="{{ asset('frontend/assets/vendor/aos/aos.css') }}" rel="stylesheet">

  <!-- Template Main CSS File -->
  <link rel="stylesheet" href="{{ asset('login-register/fonts/material-icon/css/material-design-iconic-font.min.css') }}">
  <link rel="stylesheet" href="{{ asset('login-register/css/style.css') }}">

  <link href="{{ asset('frontend/assets/css/style.css') }}" rel="stylesheet">

  <style>
    .form-group:last-child {
        margin-bottom: 50px !important;
    }
    .form-submit {
        padding: 10px 40px !important;
    }

    #slider {
        position: relative;
        overflow: hidden;
        margin: 45px auto 0 auto;
        border-radius: 4px;
    }

    #slider ul {
        position: relative;
        margin: 0;
        padding: 0;
        height: 600px;
        list-style: none;
    }

    #slider ul li {
        position: relative;
        display: block;
        float: left;
        margin: 0;
        padding: 0;
        width: 300px;
        height: 390px;
        text-align: center;
        line-height: 300px;
    }

    a.control_prev, a.control_next {
        position: absolute;
        top: 40%;
        z-index: 999;
        display: block;
        padding: 4% 3%;
        width: auto;
        height: auto;
        background: #FCB71A;
        color: #fff;
        text-decoration: none;
        font-weight: 600;
        font-size: 18px;
        opacity: 0.8;
        cursor: pointer;
    }

    a.control_prev:hover, a.control_next:hover {
        opacity: 1;
        -webkit-transition: all 0.2s ease;
    }

    a.control_prev {
        border-radius: 0 2px 2px 0;
    }

    a.control_next {
        right: 0;
        border-radius: 2px 0 0 2px;
    }

    .slider_option {
        position: relative;
        margin: 10px auto;
        width: 160px;
        font-size: 18px;
    }

    .buttonanimate {
        background-color: #004A7F;
        -webkit-border-radius: 5px;
        border-radius: 5px;
        border: none;
        color: #FFFFFF;
        cursor: pointer;
        display: inline-block;
        font-size: 15px;
        padding: 5px 10px;
        text-align: center;
        text-decoration: none;
        -webkit-animation: glowing 1500ms infinite;
        -moz-animation: glowing 1500ms infinite;
        -o-animation: glowing 1500ms infinite;
        animation: glowing 1500ms infinite;
    }
    @-webkit-keyframes glowing {
        0% { background-color: #2A75AE; -webkit-box-shadow: 0 0 3px #2A75AE; }
        50% { background-color: #FCB71A; -webkit-box-shadow: 0 0 40px #FCB71A; }
        100% { background-color: #2A75AE; -webkit-box-shadow: 0 0 3px #2A75AE; }
    }

    @-moz-keyframes glowing {
        0% { background-color: #2A75AE; -moz-box-shadow: 0 0 3px #2A75AE; }
        50% { background-color: #FCB71A; -moz-box-shadow: 0 0 40px #FCB71A; }
        100% { background-color: #2A75AE; -moz-box-shadow: 0 0 3px #2A75AE; }
    }

    @-o-keyframes glowing {
        0% { background-color: #2A75AE; box-shadow: 0 0 3px #2A75AE; }
        50% { background-color: #FCB71A; box-shadow: 0 0 40px #FCB71A; }
        100% { background-color: #2A75AE; box-shadow: 0 0 3px #2A75AE; }
    }

    @keyframes glowing {
        0% { background-color: #2A75AE; box-shadow: 0 0 3px #2A75AE; }
        50% { background-color: #FCB71A; box-shadow: 0 0 40px #FCB71A; }
        100% { background-color: #2A75AE; box-shadow: 0 0 3px #2A75AE; }
    }
  </style>
</head>

<body style="background-color: #f1f8ff;">
  <header id="header" class="fixed-top " style="background-color: #2A75AE">
    <div class="container d-flex align-items-center" style="width: none; background: none;">
      {{-- <h1 class="logo mr-auto" style="font-weight: bold;color: #2A75AE;"><a href="#header" class="scrollto">CENDE<span style="color: #FCB71A;">Q</span>IA</a></h1> --}}
      <h1 class="logo mr-auto" style="font-weight: bold;color: #2A75AE;">
      <img src="{{ asset('frontend/assets/image/LOGO.png') }}" style="width: 30%" alt="">
      </h1>
      <nav class="nav-menu d-none d-lg-block">
        <ul>
          <li><a href="/">HOME</a></li>
          <li><a href="/about">TENTANG</a></li>
          <li><a href="/faq">FAQ</a></li>
          <li><a href="/contact">KONTAK</a></li>
        </ul>
      </nav>
    </div>
  </header>

  <main id="main">
    <section id="icon-boxes" class="icon-boxes sign-in" style="padding-top: 10px;"></section>
    <section id="why-us" class="why-us" style="padding-top: 40px;padding-bottom:30px;">
        <div class="container">
            <div class="row">
                <div class="col-lg-7 d-flex flex-column justify-content-center align-items-stretch aos-init aos-animate" data-aos="fade-left">
                    <div class="content" data-aos="fade-left" data-aos-delay="100">
                        @if (Session::has('Success'))
                            <div class="alert alert-success">
                                {{ Session::get('Success') }}
                            </div>
                        @endif
                        @if (Session::has('Info'))
                            <div class="alert alert-info">
                                {{ Session::get('Info') }}
                            </div>
                        @endif
                        @if (Session::has('Danger'))
                            <div class="alert alert-danger">
                                {{ Session::get('Danger') }}
                            </div>
                        @endif
                        @if ($errors->has('email'))
                            <div class="alert alert-danger">
                                <strong>Email telah tedafatar, harap gunakan email lain.</strong>
                            </div>
                        @endif
                        <h4 class="form-title"><b>Login Area</b></h4>

                        <form method="POST" class="register-form" id="login-form" action="{{ route('kedinasan.login.post') }}">
                            @csrf
                            <div class="form-group">
                                <label for="email"><i class="zmdi zmdi-account material-icons-name"></i></label>
                                <input type="email" name="email" id="email" placeholder="Email" autocomplete="off" required="" />
                            </div>
                            <div class="form-group">
                                <label for="password"><i class="zmdi zmdi-lock"></i></label>
                                <input type="password" name="password" id="password" placeholder="Password" required="" />
                            </div>
                            <div class="form-group has-feedback">
                                <div id="captcha"></div>
                            </div>
                            <div class="form-group">
                              <a href="{{ route('password.request') }}">Lupa Password?</a>
                            </div>
                            <div class="form-group form-button">
                                <input type="button" name="signin" id="signin" class="form-submit" value="MASUK" style="background: #2A75AE;" onclick="verifyCaptcha(grecaptcha.getResponse(widget))" />
                                &nbsp;&nbsp;&nbsp;&nbsp;
                                <a href="{{ route('kedinasan.register') }}" id="signin" class="form-submit" style="background: #FCB71A; color: white;"> DAFTAR</a>
                            </div>
                            @if ($dataSimulation->is_active == 1)
                            <button type="button" class="buttonanimate btn-block" data-toggle="modal" data-target=".bd-example-modal-lg"> TRY OUT GRATIS</button>
                            <div class="form-group">
                            </div>
                            @endif
                        </form>
                    </div>
                </div>
                <div class="col-lg-5">
                    <div id="slider">
                        <a class="control_next">></a>
                        <a class="control_prev"><</a>
                        <ul>
                          <li><img src="{{ asset('frontend/assets/image/kedinasan/1.jpg')}}"/></li>
                          <li><img src="{{ asset('frontend/assets/image/kedinasan/3.jpg')}}" /></li>
                          <li><img src="{{ asset('frontend/assets/image/kedinasan/4.jpg')}}" /></li>
                          <li><img src="{{ asset('frontend/assets/image/kedinasan/5.jpg')}}" /></li>
                          <li><img src="{{ asset('frontend/assets/image/kedinasan/6.jpg')}}" /></li>
                          <li><img src="{{ asset('frontend/assets/image/kedinasan/7.jpg')}}" /></li>
                          <li><img src="{{ asset('frontend/assets/image/kedinasan/8.jpg')}}" /></li>
                          <li><img src="{{ asset('frontend/assets/image/kedinasan/9.jpg')}}" /></li>
                          <li><img src="{{ asset('frontend/assets/image/kedinasan/10.jpg')}}" /></li>
                          <li><img src="{{ asset('frontend/assets/image/kedinasan/11.jpg')}}" /></li>
                          <li><img src="{{ asset('frontend/assets/image/kedinasan/12.jpg')}}" /></li>
                          <li><img src="{{ asset('frontend/assets/image/kedinasan/13.jpg')}}" /></li>
                          <li><img src="{{ asset('frontend/assets/image/kedinasan/14.jpg')}}" /></li>
                          <li><img src="{{ asset('frontend/assets/image/kedinasan/15.jpg')}}" /></li>
                          <li><img src="{{ asset('frontend/assets/image/kedinasan/16.jpg')}}" /></li>
                          <li><img src="{{ asset('frontend/assets/image/kedinasan/17.jpg')}}" /></li>
                          <li><img src="{{ asset('frontend/assets/image/kedinasan/18.jpg')}}" /></li>
                          <li><img src="{{ asset('frontend/assets/image/kedinasan/19.jpg')}}" /></li>

                        </ul>
                      </div>
                </div>
            </div>
        </div>

        @if ($dataSimulation->is_active == 1)
        <div class="modal fade bd-example-modal-lg show" tabindex="-1" role="dialog" aria-labelledby="myLargeModalLabel" aria-modal="true">
            <div class="modal-dialog modal-dialog-centered">
              <div class="modal-content">
                <div class="modal-header">
                  <h5 class="modal-title" id="exampleModalLabel"><p id="title">Masukkan informasi</p></h5>
                  <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">×</span>
                  </button>
                </div>
                <div class="modal-body">
                    <form method="POST" class="register-form" action="{{ route('simulation.kedinasan.create') }}">
                        @csrf
                        <div class="form-group">
                            <label for="name"><i class="zmdi zmdi-account material-icons-name"></i></label>
                            <input type="text" name="name" placeholder="Nama" autocomplete="off" required="" />
                        </div>
                        <div class="form-group">
                            <label for="email"><i class="zmdi zmdi-email material-icons-name"></i></label>
                            <input type="email" name="email" placeholder="Email" autocomplete="off" required="" />
                        </div>
                        <div class="form-group">
                            <label for="sphone"><i class="zmdi zmdi-phone material-icons-name"></i></label>
                            <input type="sphone" name="phone" placeholder="No. Handphone" autocomplete="off" required="" />
                        </div>
                        <i style="color: #FCB71A">NB: TRY OUT gratis hanya bisa dilakukan 1 kali</i>
                        <br>
                        <br>
                        <div class="form-group">
                            <button type="submit" class="btn btn-block" style="background: #2A75AE; color: white;">
                                <i class="zmdi zmdi-edit material-icons-name"></i>
                                &nbsp; LANJUTKAN
                            </button>
                        </div>
                    </form>
                </div>
                <div class="modal-footer">
                  <button type="button" class="btn btn-secondary btn-block" data-dismiss="modal">BATAL</button>
                </div>
              </div>
            </div>
        </div>
        @endif
    </section>
  </main>

  <a href="#" class="back-to-top"><i class="ri-arrow-up-line"></i></a>
  <div id="preloader"></div>

  <!-- Vendor JS Files -->
  <script src="{{ asset('frontend/assets/vendor/jquery/jquery.min.js') }}"></script>
  <script src="{{ asset('frontend/assets/vendor/bootstrap/js/bootstrap.bundle.min.js') }}"></script>
  <script src="{{ asset('frontend/assets/vendor/jquery.easing/jquery.easing.min.js') }}"></script>
  <script src="{{ asset('frontend/assets/vendor/php-email-form/validate.js') }}"></script>
  <script src="{{ asset('frontend/assets/vendor/owl.carousel/owl.carousel.min.js') }}"></script>
  <script src="{{ asset('frontend/assets/vendor/venobox/venobox.min.js') }}"></script>
  <script src="{{ asset('frontend/assets/vendor/isotope-layout/isotope.pkgd.min.js') }}"></script>
  <script src="{{ asset('frontend/assets/vendor/aos/aos.js') }}"></script>

  <!-- Template Main JS File -->
  <script src="{{ asset('frontend/assets/js/main.js') }}"></script>
  <script src="https://www.google.com/recaptcha/api.js?render=reCAPTCHA_site_key"></script>
    <script src="https://www.google.com/recaptcha/api.js?onload=onloadCallback&render=explicit" async defer></script>
    <script type="text/javascript">
        var widget;
        var onloadCallback = function () {
            widget =
                grecaptcha.render('captcha', {
                    'sitekey': '{{ config('app.recaptcha') }}',
                    'theme': 'light'
                });
        };
        function verifyCaptcha(result) {
            if (result != "") {
                $('#login-form').submit();
            } else {
                alert('Harap isi reCAPTCHA terlebih dahulu');
            }
        }
    </script>
    <script>
        $(document).ready(function ($) {

            $('#checkbox').change(function(){
                setInterval(function () {
                    moveRight();
                }, 3000);
            });

            var slideCount = $('#slider ul li').length;
            var slideWidth = $('#slider ul li').width();
            var slideHeight = $('#slider ul li').height();
            var sliderUlWidth = slideCount * slideWidth;

            $('#slider').css({ width: slideWidth, height: slideHeight });

            $('#slider ul').css({ width: sliderUlWidth, marginLeft: - slideWidth });

            $('#slider ul li:last-child').prependTo('#slider ul');

            function moveLeft() {
                $('#slider ul').animate({
                    left: + slideWidth
                }, 200, function () {
                    $('#slider ul li:last-child').prependTo('#slider ul');
                    $('#slider ul').css('left', '');
                });
            };

            function moveRight() {
                $('#slider ul').animate({
                    left: - slideWidth
                }, 200, function () {
                    $('#slider ul li:first-child').appendTo('#slider ul');
                    $('#slider ul').css('left', '');
                });
            };

            $('a.control_prev').click(function () {
                moveLeft();
            });

            $('a.control_next').click(function () {
                moveRight();
            });

        });

    </script>
</body>

</html>