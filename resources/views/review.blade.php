<!DOCTYPE html>
<html lang="en">

<head><meta http-equiv="Content-Type" content="text/html; charset=euc-kr">
  
  <meta content="width=device-width, initial-scale=1.0" name="viewport">

  <title>Ujian</title>
  <meta content="" name="descriptison">
  <meta content="" name="keywords">
  <meta name="csrf-token" content="{{ csrf_token() }}" />

  <!-- Favicons -->
  <link rel="shortcut icon" type="image/x-icon" href="https://demo.njuah-njuah.com/assets/img/logo.png" />

  <!-- Google Fonts -->
  <link href="https://fonts.googleapis.com/css?family=Open+Sans:300,300i,400,400i,600,600i,700,700i|Raleway:300,300i,400,400i,600,600i,700,700i,900" rel="stylesheet">

  <!-- Vendor CSS Files -->
  <link href="https://njuah-njuah.com/site/assets/vendor/bootstrap/css/bootstrap.min.css" rel="stylesheet">
  <link href="https://njuah-njuah.com/site/assets/vendor/icofont/icofont.min.css" rel="stylesheet">
  <link href="https://njuah-njuah.com/site/assets/vendor/boxicons/css/boxicons.min.css" rel="stylesheet">
  <link href="https://njuah-njuah.com/site/assets/vendor/animate.css/animate.min.css" rel="stylesheet">
  <link href="https://njuah-njuah.com/site/assets/vendor/venobox/venobox.css" rel="stylesheet">
  <link href="https://njuah-njuah.com/site/assets/vendor/aos/aos.css" rel="stylesheet">

  <!-- Template Main CSS File -->
  <link href="https://demo.njuah-njuah.com/assets/css/style.css" rel="stylesheet">

  <!-- Tree Family -->
  <link href="https://njuah-njuah.com/site/assets/vendor/jquery-ui-1.12.1/jquery-ui.min.css" rel="stylesheet"></script>
  <link href="https://njuah-njuah.com/site/assets/vendor/family-tree/css/jHTree.css" rel="stylesheet">


  <!-- =======================================================
  * Template Name: Mamba - v2.0.1
  * Template URL: https://bootstrapmade.com/mamba-one-page-bootstrap-template-free/
  * Author: BootstrapMade.com
  * License: https://bootstrapmade.com/license/
  ======================================================== -->
</head>
<style type="text/css">
  .jst-hours {
    float: left;
  }
  .jst-minutes {
    float: left;
  }
  .jst-seconds {
    float: left;
  }
  .jst-clearDiv {
    clear: both;
  }
</style><body>
  <main id="main">
    <!-- ======= About Lists Section ======= -->
    <section class="about-lists container">
        <br>      
        <div class="row contact">
        <div class="col-lg-9 col-sm-12" id="t4_ujian">
          <div class="tab-content">
         	 	@foreach($akumulasi as $total)
         	 	<p>{{ $total->jenis }}: {{ $total->total_point }}</p>
         	 	@endforeach
            @foreach ($datas as $item)
            <div class="tab-pane {{ ($loop->iteration == 1) ? 'active' : '' }} " id="soal{{ $loop->iteration }}" role="tabpanel" aria-labelledby="soal{{ $loop->iteration }}-tab">
              <label><b>Soal : {{ $item->question->jenis }}</b></label>
              <p>{{ $loop->iteration }}. {{ $item->question->question }}</p><br />
                <form class="form" action="#" method="post" id="simpansoalno{{ $loop->iteration }}">
                <ol type="A">
                	@php
			              if ($item->question->answer === $item->answer && $item->option_a === 1) { $colorA = 'green'; }
                    elseif ($item->question->answer !== $item->answer && $item->option_a === 1) { $colorA = 'red'; }
			              else { $colorA = ''; }

                    if ($item->question->answer === $item->answer && $item->option_b === 1) { $colorB = 'green'; }
                    elseif ($item->question->answer !== $item->answer && $item->option_b === 1) { $colorB = 'red'; }
                    else { $colorB = ''; }

                    if ($item->question->answer === $item->answer && $item->option_c === 1) { $colorC = 'green'; }
                    elseif ($item->question->answer !== $item->answer && $item->option_c === 1) { $colorC = 'red'; }
                    else { $colorC = ''; }
			            @endphp
                  {{-- {{ dd($item->question->answer !== $item->answer && $item->option_a === 1) }} --}}
                    <li>
                        <input type="radio" name="jawab" value="a" {{ ($item->option_a === 1) ? 'checked' : '' }}>
                        &ensp;<label>
                        @if ($item->question->answer === $item->answer && $item->option_a === 1)
                        <p style="color: green; ">{{ $item->question->option_a }}</p> 
                        @elseif ($item->question->answer !== $item->answer && $item->option_a === 1)
                        <p style="color: red; ">{{ $item->question->option_a }}</p> 
                        @elseif ($item->question->answer === 'a')
                        <p style="color: green; ">{{ $item->question->option_a }}</p> 
                        @else
                        <p>{{ $item->question->option_a }}</p> 
                        @endif
                    </li>
                    <li>
                        <input type="radio" name="jawab" value="b" {{ ($item->option_b === 1) ? 'checked' : '' }}>
                        &ensp;<label>
                        @if ($item->question->answer === $item->answer && $item->option_b === 1)
                        <p style="color: green; ">{{ $item->question->option_b }}</p> 
                        @elseif ($item->question->answer !== $item->answer && $item->option_b === 1)
                        <p style="color: red; ">{{ $item->question->option_b }}</p> 
                        @elseif ($item->question->answer === 'b')
                        <p style="color: green; ">{{ $item->question->option_b }}</p> 
                        @else
                        <p>{{ $item->question->option_b }}</p> 
                        @endif
                    </li>
                    <li>
                        <input type="radio" name="jawab" value="c" {{ ($item->option_c === 1) ? 'checked' : '' }}>
                        &ensp;<label>
                        @if ($item->question->answer === $item->answer && $item->option_c === 1)
                        <p style="color: green; ">{{ $item->question->option_c }}</p> 
                        @elseif ($item->question->answer !== $item->answer && $item->option_c === 1)
                        <p style="color: red; ">{{ $item->question->option_c }}</p> 
                        @elseif ($item->question->answer === 'c')
                        <p style="color: green; ">{{ $item->question->option_c }}</p> 
                        @else
                        <p>{{ $item->question->option_c }}</p> 
                        @endif
                    </li>
                    <li>
                      <input type="radio" name="jawab" value="d" {{ ($item->option_d === 1) ? 'checked' : '' }}>
                      &ensp;<label>
                      @if ($item->question->answer === $item->answer && $item->option_d === 1)
                      <p style="color: green; ">{{ $item->question->option_d }}</p> 
                      @elseif ($item->question->answer !== $item->answer && $item->option_d === 1)
                      <p style="color: red; ">{{ $item->question->option_d }}</p> 
                      @elseif ($item->question->answer === 'd')
                      <p style="color: green; ">{{ $item->question->option_d }}</p> 
                      @else
                      <p>{{ $item->question->option_d }}</p> 
                      @endif
                    </li>
                    <li>
                      <input type="radio" name="jawab" value="e" {{ ($item->option_e === 1) ? 'checked' : '' }}>
                      &ensp;<label>
                      @if ($item->question->answer === $item->answer && $item->option_e === 1)
                      <p style="color: green; ">{{ $item->question->option_e }}</p> 
                      @elseif ($item->question->answer !== $item->answer && $item->option_e === 1)
                      <p style="color: red; ">{{ $item->question->option_e }}</p> 
                      @elseif ($item->question->answer === 'e')
                      <p style="color: green; ">{{ $item->question->option_e }}</p> 
                      @else
                      <p>{{ $item->question->option_e }}</p> 
                      @endif
                    </li>
                </ol>
                @if ($item->question->answer === $item->answer)
                <p style="color: green;">Jawaban Benar</p>
                @else
                <p style="color: red;">Jawaban Salah</p>
                <p>Jawaban : {{ $item->question->answer }}</p>
                @endif
                <p>{{ $item->question->discussion }}</p>
                @if($loop->iteration != 1)
                <button type="button" class="btn btn-warning btn-flat btn-sm" onclick="kembali({{ $loop->iteration }}-1)">Kembali</button>
                @endif
                @if($loop->iteration != $datas->count())
                <button type="button" class="btn btn-success btn-flat btn-sm" onclick="lewati({{ $loop->iteration }}+1)">Lewati</button>
                @endif
              </form>
              </p>
            </div>
            @endforeach
            </div>
        </div>
        <div class="col-lg-3 col-sm-12" id="t4_nomor">
          <p>TIU</p>
            @foreach ($datas as $item)
            @php
              if ($item->question->answer === $item->answer) { $button = 'btn-success'; }
              else { $button = 'btn-danger'; }
            @endphp
            @if ($item->jenis == 'TIU')
            <button 
              class="btn btn-sm {{ $button }}" 
              onclick="openSoal({{ $loop->iteration }})" 
              style="width: 35px;margin-bottom:2px;" data="jawabid{{ $item->id }}" id="no{{ $loop->iteration }}">
              {{ $loop->iteration }}
            </button>
            @endif
            @endforeach
            <br>
            <hr>
            <p>TWK</p>
            @foreach ($datas as $item)
            @php
              if ($item->question->answer === $item->answer) { $button = 'btn-success'; }
              else { $button = 'btn-danger'; }
            @endphp
            @if ($item->jenis == 'TWK')
            <button 
              class="btn btn-sm {{ $button }}" 
              onclick="openSoal({{ $loop->iteration }})" 
              style="width: 35px;margin-bottom:2px;" data="jawabid{{ $item->id }}" id="no{{ $loop->iteration }}">
              {{ $loop->iteration }}
            </button>
            @endif
            @endforeach
            <br>
            <hr>
            <p>TKP</p>
            @foreach ($datas as $item)
            @php
              if ($item->question->answer === $item->answer) { $button = 'btn-success'; }
              else { $button = 'btn-danger'; }
            @endphp
            @if ($item->jenis == 'TKP')
            <button 
              class="btn btn-sm {{ $button }}" 
              onclick="openSoal({{ $loop->iteration }})" 
              style="width: 35px;margin-bottom:2px;" data="jawabid{{ $item->id }}" id="no{{ $loop->iteration }}">
              {{ $loop->iteration }}
            </button>
            @endif
            @endforeach
            <br>
            <hr>
            <a href="{{ route('history.quiz') }}" class="btn btn-primary btn-sm">Lihat Semua Pembahasan</a>
          <br>
        </div>
      </div>
    </section><!-- End About Lists Section -->
    <!-- Modal Gambar -->
    <!-- Modal -->
    <div class="modal fade" id="exampleModalCenter" tabindex="-1" role="dialog" aria-labelledby="exampleModalCenterTitle" aria-hidden="true">
      <div class="modal-dialog modal-dialog-centered modal-lg" role="document">
        <div class="modal-content">
          <div class="modal-header">
            <button type="button" class="close" data-dismiss="modal" aria-label="Close">
              <span aria-hidden="true">&times;</span>
            </button>
          </div>
          <div class="modal-body">
            
          </div>
        </div>
      </div>
    </div>
    <!-- END Modal Gambar -->
  </main><!-- End #main -->

  
  <!-- Vendor JS Files -->
  <script src="https://njuah-njuah.com/site/assets/vendor/jquery/jquery.min.js"></script>
  <script src="https://njuah-njuah.com/site/assets/vendor/bootstrap/js/bootstrap.bundle.min.js"></script>
  <script src="https://njuah-njuah.com/site/assets/vendor/jquery.easing/jquery.easing.min.js"></script>
  <script src="https://njuah-njuah.com/site/assets/vendor/php-email-form/validate.js"></script>
  <script src="https://njuah-njuah.com/site/assets/vendor/jquery-sticky/jquery.sticky.js"></script>
  <script src="https://njuah-njuah.com/site/assets/vendor/venobox/venobox.min.js"></script>
  <script src="https://njuah-njuah.com/site/assets/vendor/waypoints/jquery.waypoints.min.js"></script>
  <script src="https://njuah-njuah.com/site/assets/vendor/counterup/counterup.min.js"></script>
  <script src="https://njuah-njuah.com/site/assets/vendor/isotope-layout/isotope.pkgd.min.js"></script>
  <script src="https://njuah-njuah.com/site/assets/vendor/aos/aos.js"></script>

  <!-- Template Main JS File -->
  <script src="https://demo.njuah-njuah.com/assets/js/main.js"></script>

  <!-- Tree Family -->
  <script src="https://njuah-njuah.com/site/assets/vendor/jquery-ui-1.12.1/jquery-ui.min.js"></script>
  <script src="https://njuah-njuah.com/site/assets/vendor/family-tree/js/jQuery.jHTree.js"></script>

  <script src="https://demo.njuah-njuah.com/assets/countdown/jquery.countdownTimer.js"></script>
  <!-- auto scroll -->
  <script src="https://njuah-njuah.com/site/assets/vendor/auto-scroll/js/jquery.autoscroll.js"></script>
</body>

</html>  
<script type="text/javascript">
  $.ajaxSetup({
      headers: {
          'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
      }
  });
</script>
<script type="text/javascript">
    // $(document).on("keydown", disableF5);
    $(document).on("keydown", disableCtrlR);

    // window.onbeforeunload = function(){
    //     return "Yay! Selesai ujian?";
    //   }

    $(function(){
      // document.documentElement.requestFullscreen();

      window.history.pushState(null, "", window.location.href);        
      window.onpopstate = function() {
            window.history.pushState(null, "", window.location.href);
        };

      $(window).on("contextmenu",function(){
        return false;
      });

      $("#clock").countdowntimer({
          startDate : '2021-01-18 20:39:37',
          dateAndTime : '2021-01-18 20:42:35',
          size : "lg",
          displayFormat: "HMS",
          timeUp : load_hasil_ujian,
      });
    })

    function selesai_ujian(){
      if(confirm("Anda yakin sudah selesai?")){
        load_hasil_ujian();
      }
    }

    load_hasil_ujian = function(){
      $.ajax({
        url:'/review',
        type:'get',
        beforeSend:function(){
          $("#t4_ujian").html("Memuat hasil ujian...");
          $("#t4_nomor").html("");
          // setTimeout(function() { 
          //     location.reload();
          // }, 30000); //reload
        },
        success:function(e){
          $("#t4_ujian").html(e);
        }
      })
    }

    function disableF5(e){
      if ((e.which || e.keyCode)== 116) e.preventDefault();
    }

    function disableCtrlR(e){
      if (e.ctrlKey) e.preventDefault();
    }

    function simpan_lanjutkan(no,id){
      var now = no+1;
      var jlh_soal = {{ $datas->count() }};
      
      if(now > jlh_soal){
        now = 1;
      }

      $(".tab-pane").removeClass('active');
      $("#soal"+now).addClass('active');

      $("#simpansoalno"+no).on("submit",function(){
        $.ajax({
          url : 'store/'+id,
          method : 'POST',
          data : $(this).serialize(),
          beforeSend:function(){

          },
          success:function(e){
            if(e=="OK"){
              $("#no"+no).attr("class","btn btn-sm btn-success");
            }
          },
          error:function(x){
            $("#no"+no).attr("class","btn btn-sm btn-warning");
          }
        })
        return false;
      })
    }

    function lewati(no){
      var jlh_soal = 30;
      
      if(no > jlh_soal){
        no = 1;
      }

      $(".tab-pane").removeClass('active');
      $("#soal"+no).addClass('active');
    }

    function openSoal(no){
      $(".tab-pane").removeClass('active');
      $("#soal"+no).addClass('active');
    }

    function kembali(no){
      $(".tab-pane").removeClass('active');
      $("#soal"+no).addClass('active');
    }

    function zoomx(pict){
      $(".modal-body").html('<img class="img img-fluid img-thumbnail" src="https://demo.njuah-njuah.com/assets/uploads/'+pict+'">');
      $("#exampleModalCenter").modal('toggle');
    }

  </script>