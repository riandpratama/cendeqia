@extends('template.simulation-quiz')
    <meta name="csrf-token" content="{{ csrf_token() }}" />
    <link href="{{ asset('quiz/assets/vendor/css/icofont.min.css') }}" rel="stylesheet">
    <link href="{{ asset('quiz/assets/vendor/css/boxicons.min.css') }}" rel="stylesheet">
    <link href="{{ asset('quiz/assets/vendor/css/animate.min.css') }}" rel="stylesheet">

    <link href="{{ asset('quiz/assets/vendor/css/venobox.css') }}" rel="stylesheet">

    <!-- Template Main CSS File -->
    <link href="{{ asset('quiz/assets/vendor/css/style.css') }}" rel="stylesheet">

    <!-- Tree Family -->
    <link href="{{ asset('quiz/assets/vendor/css/jquery-ui.min.css') }}" rel="stylesheet"></script>
    <link href="{{ asset('quiz/assets/vendor/css/jHTree.css') }}" rel="stylesheet">
    <link href="{{ asset('frontend/assets/image/ICON.png')}}" rel="icon">
    <link href="{{ asset('backend/assets/css/main.css') }}" rel="stylesheet">
    <style>
        .btn {
            font-size: .6rem !important;
        }

        /* The container */
        .inputcontainer {
          display: block;
          position: relative;
          padding-left: 30px;
          margin-bottom: 12px;
          cursor: pointer;
          font-size: 14px;
          -webkit-user-select: none;
          -moz-user-select: none;
          -ms-user-select: none;
          user-select: none;
        }

        /* Hide the browser's default checkbox */
        .inputcontainer input {
          position: absolute;
          opacity: 0;
          cursor: pointer;
          height: 0;
          width: 0;
        }

        /* Create a custom checkbox */
        .checkmark {
          position: absolute;
          top: 0;
          left: 0;
          height: 20px;
          width: 20px;
          background-color: #eee;
        }

        /* On mouse-over, add a grey background color */
        .inputcontainer:hover input ~ .checkmark {
          background-color: #ccc;
        }

        /* When the checkbox is checked, add a blue background */
        .inputcontainer input:checked ~ .checkmark {
          background-color: #2196F3;
        }

        /* Create the checkmark/indicator (hidden when not checked) */
        .checkmark:after {
          content: "";
          position: absolute;
          display: none;
        }

        /* Show the checkmark when checked */
        .inputcontainer input:checked ~ .checkmark:after {
          display: block;
        }

        /* Style the checkmark/indicator */
        .inputcontainer .checkmark:after {
          left: 9px;
          top: 5px;
          width: 5px;
          height: 10px;
          border: solid white;
          border-width: 0 3px 3px 0;
          -webkit-transform: rotate(45deg);
          -ms-transform: rotate(45deg);
          transform: rotate(45deg);
        }
    </style>
</head>

@section('content')
    <div class="row">
        <div class="card-shadow-primary border mb-3 card card-body border-primary">
            <h4>INSTRUKSI :</h4>
            <p style="color: #2A75AE;">Pilihlah salah satu jawaban yang menurut anda benar. Kemudian klik Simpan dan lanjutkan</p>
            <p style="font-weight: bold; color: #d92550;">Waktu Selesai : </p>
            <div id="clock" style="font-weight: bold; font-size: 30px; color: #d92550;" ></div>
        </div>
    </div>
    <div class="row">
        <input type="hidden" id="url-quiz" value="/simulation/store-quiz/cpns/store/">
        <div class="col-md-8 main-card mb-3 card">
            <div class="card-body">
                <div class="col-sm-12" id="t4_ujian">
                    <div class="tab-content">
                        @foreach ($datas as $item)
                        <div class="tab-pane {{ ($loop->iteration == 1) ? 'active' : '' }} " id="soal{{ $loop->iteration }}" role="tabpanel" aria-labelledby="soal{{ $loop->iteration }}-tab">
                            <label><b>Soal: {{ $item->question->jenis }}</b></label>
                            <hr>
                            <p style="padding-left: 25px">
                                <span style="position: absolute;  margin-left: -2rem; font-weight: bold;">{{ $loop->iteration }}.</span>
                                @if (!is_null($item->question->img_question))
                                <img src="{{ config('app.image') }}/{{ $item->question->img_question }}" style="max-width:300px; max-height:200px">
                                @endif
                                <b>{!! $item->question->question !!}</b>
                            </p>
                            <br />

                            <form class="form" action="#" method="post" id="simpansoalno{{ $loop->iteration }}">
                                <ol type="A">
                                  <li>
                                    <label class="inputcontainer">
                                      {!! $item->question->option_a !!}
                                      @if (!is_null($item->question->img_option_a))
                                        <img src="{{ config('app.image') }}/{{ $item->question->img_option_a }}" style="max-width:300px; max-height:200px">
                                      @endif
                                      <input type="radio" type="radio" name="jawab" value="a" required="" {{ ($item->option_a == 1) ? 'checked' : '' }}>
                                      @if (!is_null($item->question->img_option_a))
                                      <span class="checkmark" style="top:40 !important;"></span>
                                      @else
                                      <span class="checkmark"></span>
                                      @endif
                                    </label>
                                  </li>
                                  <li>
                                    <label class="inputcontainer">
                                      {!! $item->question->option_b !!}
                                      @if (!is_null($item->question->img_option_b))
                                        <img src="{{ config('app.image') }}/{{ $item->question->img_option_b }}" style="max-width:300px; max-height:200px">
                                      @endif
                                      <input type="radio" type="radio" name="jawab" value="b" required="" {{ ($item->option_b == 1) ? 'checked' : '' }}>
                                      @if (!is_null($item->question->img_option_b))
                                      <span class="checkmark" style="top:40 !important;"></span>
                                      @else
                                      <span class="checkmark"></span>
                                      @endif
                                    </label>
                                  </li>
                                  <li>
                                    <label class="inputcontainer">
                                      {!! $item->question->option_c !!}
                                      @if (!is_null($item->question->img_option_c))
                                        <img src="{{ config('app.image') }}/{{ $item->question->img_option_c }}" style="max-width:300px; max-height:200px">
                                      @endif
                                      <input type="radio" type="radio" name="jawab" value="c" required="" {{ ($item->option_c == 1) ? 'checked' : '' }}>
                                      @if (!is_null($item->question->img_option_c))
                                      <span class="checkmark" style="top:40 !important;"></span>
                                      @else
                                      <span class="checkmark"></span>
                                      @endif
                                    </label>
                                  </li>
                                  <li>
                                    <label class="inputcontainer">
                                      {!! $item->question->option_d !!}
                                      @if (!is_null($item->question->img_option_d))
                                        <img src="{{ config('app.image') }}/{{ $item->question->img_option_d }}" style="max-width:300px; max-height:200px">
                                      @endif
                                      <input type="radio" type="radio" name="jawab" value="d" required="" {{ ($item->option_d == 1) ? 'checked' : '' }}>
                                      @if (!is_null($item->question->img_option_d))
                                      <span class="checkmark" style="top:40 !important;"></span>
                                      @else
                                      <span class="checkmark"></span>
                                      @endif
                                    </label>
                                  </li>
                                  <li>
                                    <label class="inputcontainer">
                                      {!! $item->question->option_e !!}
                                      @if (!is_null($item->question->img_option_e))
                                        <img src="{{ config('app.image') }}/{{ $item->question->img_option_e }}" style="max-width:300px; max-height:200px">
                                      @endif
                                      <input type="radio" type="radio" name="jawab" value="e" required="" {{ ($item->option_e == 1) ? 'checked' : '' }}>
                                      @if (!is_null($item->question->img_option_e))
                                      <span class="checkmark" style="top:40 !important;"></span>
                                      @else
                                      <span class="checkmark"></span>
                                      @endif
                                    </label>
                                  </li>
                                </ol>
                                <hr>
                                @if($loop->iteration != 1)
                                <button type="button" class="btn btn-warning btn-flat" onclick="kembali({{ $loop->iteration }}-1)">Kembali</button>
                                @endif
                                <button type="submit" class="btn btn-success btn-flat" onclick="simpan_lanjutkan({{ $loop->iteration }},{{ $item->id }})">Simpan & lanjutkan</button>
                                @if($loop->iteration != $datas->count())
                                <button type="button" class="btn btn-primary btn-flat" onclick="lewati({{ $loop->iteration }}+1)">Lewati</button>
                                @endif
                            </form>
                        </div>
                        @endforeach
                    </div>
                </div>
            </div>
        </div>
        <div class="col-md-4">
            <div class="main-card mb-3 card">
                <div class="card-body">
                    <div class="col-sm-12" id="t4_nomor">
                        <p>TWK</p>
                        @foreach ($datas as $item)
                        @php
                            if ($item->option_a == 1) { $button = 'btn-success'; }
                            elseif ($item->option_b == 1) { $button = 'btn-success'; }
                            elseif ($item->option_c == 1) { $button = 'btn-success'; }
                            elseif ($item->option_d == 1) { $button = 'btn-success'; }
                            elseif ($item->option_e == 1) { $button = 'btn-success'; }
                            else { $button = 'btn-warning'; }
                        @endphp
                        @if ($item->jenis == 'TWK')
                        <button
                            class="btn btn-sm {{ $button }}"
                            onclick="openSoal({{ $loop->iteration }})"
                            style="width: 35px;margin-bottom:2px;" data="jawabid{{ $item->id }}" id="no{{ $loop->iteration }}">
                            {{ $loop->iteration }}
                        </button>
                        @endif
                        @endforeach
                        <hr>
                        <p>TIU</p>
                        @foreach ($datas as $item)
                        @php
                            if ($item->option_a == 1) { $button = 'btn-success'; }
                            elseif ($item->option_b == 1) { $button = 'btn-success'; }
                            elseif ($item->option_c == 1) { $button = 'btn-success'; }
                            elseif ($item->option_d == 1) { $button = 'btn-success'; }
                            elseif ($item->option_e == 1) { $button = 'btn-success'; }
                            else { $button = 'btn-warning'; }
                        @endphp
                        @if ($item->jenis == 'TIU')
                        <button
                            class="btn btn-sm {{ $button }}"
                            onclick="openSoal({{ $loop->iteration }})"
                            style="width: 35px;margin-bottom:2px;" data="jawabid{{ $item->id }}" id="no{{ $loop->iteration }}">
                            {{ $loop->iteration }}
                        </button>
                        @endif
                        @endforeach
                        <br>
                        <hr>
                        <p>TKP</p>
                        @foreach ($datas as $item)
                        @php
                            if ($item->option_a == 1) { $button = 'btn-success'; }
                            elseif ($item->option_b == 1) { $button = 'btn-success'; }
                            elseif ($item->option_c == 1) { $button = 'btn-success'; }
                            elseif ($item->option_d == 1) { $button = 'btn-success'; }
                            elseif ($item->option_e == 1) { $button = 'btn-success'; }
                            else { $button = 'btn-warning'; }
                        @endphp
                        @if ($item->jenis == 'TKP')
                        <button
                            class="btn btn-sm {{ $button }}"
                            onclick="openSoal({{ $loop->iteration }})"
                            style="width: 35px;margin-bottom:2px;" data="jawabid{{ $item->id }}" id="no{{ $loop->iteration }}">
                            {{ $loop->iteration }}
                        </button>
                        @endif
                        @endforeach
                        <br>
                        <hr>
                        <form action="{{ route('simulation.cpns.store.quiz.done', $detail->id) }}" method="POST">
                            @csrf
                            <button type="submit" class="btn btn-success btn-flat btn-block" onclick="return confirm('Anda yakin ingin menyelesaikan?')"><span style="color: black;">Selesai Ujian</span></button>
                        </form>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection

  <!-- Vendor JS Files -->
    <script src="{{ asset('quiz/assets/vendor/js/jquery.min.js') }}"></script>
    <script src="{{ asset('quiz/assets/vendor/js/jquery.easing.min.js') }}"></script>
    <script src="{{ asset('quiz/assets/vendor/js/validate.js') }}"></script>
    <script src="{{ asset('quiz/assets/vendor/js/jquery.sticky.js') }}"></script>
    <script src="{{ asset('quiz/assets/vendor/js/venobox.min.js') }}"></script>
    <script src="{{ asset('quiz/assets/vendor/js/jquery.waypoints.min.js') }}"></script>
    <script src="{{ asset('quiz/assets/vendor/js/counterup.min.js') }}"></script>
    <script src="{{ asset('quiz/assets/vendor/js/isotope.pkgd.min.js') }}"></script>

    <!-- Template Main JS File -->
    <script src="{{ asset('quiz/assets/vendor/js/main.js') }}"></script>

    <!-- Tree Family -->
    <script src="{{ asset('quiz/assets/vendor/js/jquery-ui.min.js') }}"></script>
    <script src="{{ asset('quiz/assets/vendor/js/jQuery.jHTree.js') }}"></script>

    <script src="{{ asset('quiz/assets/vendor/js/jquery.countdownTimer.js') }}"></script>
    <!-- auto scroll -->
    </body>

    </html>
    <script type="text/javascript">
    $.ajaxSetup({
      headers: {
          'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
      }
    });
    </script>
    <script type="text/javascript">
    // $(document).on("keydown", disableF5);
    $(document).on("keydown", disableCtrlR);

    // window.onbeforeunload = function(){
    //     return "Yay! Selesai ujian?";
    //   }

    $(function(){
      // document.documentElement.requestFullscreen();

      window.history.pushState(null, "", window.location.href);
      window.onpopstate = function() {
            window.history.pushState(null, "", window.location.href);
        };

      $(window).on("contextmenu",function(){
        return false;
      });

      $("#clock").countdowntimer({
          startDate : Date.parse(new Date()),
          dateAndTime : {{ \Carbon\Carbon::parse($detail->end_time)->timestamp * 1000 }},
          size : "lg",
          displayFormat: "HMS",
          timeUp : load_hasil_ujian,
      });
    })

    function selesai_ujian(){
      if(confirm("Anda yakin sudah selesai?")){
        load_hasil_ujian();
      }
    }

    load_hasil_ujian = function(){
      $.ajax({
        url:`/simulation/time-over/cpns/{{ Request::segment(3) }}`,
        type:'get',
        beforeSend:function(){
          $("#t4_ujian").html("Memuat hasil ujian...");
          $("#t4_nomor").html("");
          // setTimeout(function() {
          //     location.reload();
          // }, 30000); //reload
        },
        success:function(e){
          $("#t4_ujian").html(e);
        }
      })
    }

    function disableF5(e){
      if ((e.which || e.keyCode)== 116) e.preventDefault();
    }

    function disableCtrlR(e){
      if (e.ctrlKey) e.preventDefault();
    }

    function simpan_lanjutkan(no,id){
      var now = no+1;
      var jlh_soal = {{ $datas->count() }};

      if(now > jlh_soal){
        now = 1;
      }

      $(".tab-pane").removeClass('active');
      $("#soal"+now).addClass('active');

      $("#simpansoalno"+no).on("submit",function(){
        var urlquiz = $("#url-quiz").val();
        console.log(urlquiz)

        $.ajax({
          url : urlquiz+id,
          method : 'POST',
          data : $(this).serialize(),
          beforeSend:function(){

          },
          success:function(e){
            if(e=="OK"){
              $("#no"+no).attr("class","btn btn-sm btn-success");
            }
          },
          error:function(x){
            $("#no"+no).attr("class","btn btn-sm btn-warning");
          }
        })
        return false;
      })
    }

    function lewati(no){
      var jlh_soal = {{$datas->count()}};

      if(no > jlh_soal){
        no = 1;
      }

      $(".tab-pane").removeClass('active');
      $("#soal"+no).addClass('active');
    }

    function openSoal(no){
      $(".tab-pane").removeClass('active');
      $("#soal"+no).addClass('active');
    }

    function kembali(no){
      $(".tab-pane").removeClass('active');
      $("#soal"+no).addClass('active');
    }

    </script>