<div class="col-lg-9 col-sm-12" id="t4_ujian">
  <h4>Waktu simulasi ujian anda telah berakhir.</h4>
  <br>
  <i style="color:red">*Harap submit Selesai Ujian , untuk mengetahui nilai anda. </i>
  <hr>
  <form action="{{ route('simulation.cpns.store.quiz.done', $id) }}" method="POST">
  @csrf
    <button type="submit" class="btn btn-sm btn-primary btn-flat btn-block">Selesai Ujian</button>
  </form>
</div>