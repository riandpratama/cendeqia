@extends('template.simulation-quiz')

@section('content')

<div class="app-page-title">
    <div class="page-title-wrapper">
        <div class="page-title-heading">
            <div class="page-title-icon">
                <i class="pe-7s-display2 icon-gradient bg-love-kiss">
                </i>
            </div>
            <div>
              Petunjuk Pengerjaan Soal
            </div>
        </div>
    </div>
</div>

<div class="main-card card">
    <div class="card-body">
        <div class="tops text-center">
          <span style="color: #feb81b; font-size: 35px;" class="numbers d-block">
          </span>
        </div>
        <div class="py-2"></div>
            <div class="infos">
              <span style="color: #2575b1; font-size: 20px;" class="d-block text-center">PETUNJUK</span>
              <div class="py-1 my-1"></div>
              <br>
              <span class=" ml-2" style="color: #2575b1;">
                {!! $descriptionQuiz->keterangan !!}
              </span>

              <div class="py-2"></div>

              <div class="text-center bloc_nsl_btn">
                <form action="{{route('simulation.cpns.keterangan.store', [$paket, $userId] )}}" method="post" style="display: inline;">
                    @csrf
                    <button type="submit" class="btn btn-warning btnxl_set"
                        onclick="return confirm('Waktu akan berjalan ketika anda klik OK. Anda yakin ingin memulai nya?')">
                        UJI COBA GRATIS
                    </button>
                </form>
              </div>

              <div class="clear"></div>
            </div>
    </div>
</div>

@endsection