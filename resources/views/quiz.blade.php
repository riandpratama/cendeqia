<!DOCTYPE html>
<html lang="en">

<head><meta http-equiv="Content-Type" content="text/html; charset=euc-kr">
  
  <meta content="width=device-width, initial-scale=1.0" name="viewport">

  <title>Ujian</title>
  <meta content="" name="descriptison">
  <meta content="" name="keywords">
  <meta name="csrf-token" content="{{ csrf_token() }}" />

  <!-- Favicons -->
  <link rel="shortcut icon" type="image/x-icon" href="{{ asset('backend/assets/css/assets/images/logo-inverse-new.png') }}" />

  <!-- Google Fonts -->
  <link href="https://fonts.googleapis.com/css?family=Open+Sans:300,300i,400,400i,600,600i,700,700i|Raleway:300,300i,400,400i,600,600i,700,700i,900" rel="stylesheet">

  <!-- Vendor CSS Files -->
  <link href="{{ asset('quiz/assets/vendor/css/bootstrap.min.css') }}" rel="stylesheet">
  <link href="{{ asset('quiz/assets/vendor/css/icofont.min.css') }}" rel="stylesheet">
  <link href="{{ asset('quiz/assets/vendor/css/boxicons.min.css') }}" rel="stylesheet">
  <link href="{{ asset('quiz/assets/vendor/css/animate.min.css') }}" rel="stylesheet">

  <link href="{{ asset('quiz/assets/vendor/css/venobox.css') }}" rel="stylesheet">
  <link href="{{ asset('quiz/assets/vendor/css/aos.css') }}" rel="stylesheet">

  <!-- Template Main CSS File -->
  <link href="{{ asset('quiz/assets/vendor/css/style.css') }}" rel="stylesheet">

  <!-- Tree Family -->
  <link href="{{ asset('quiz/assets/vendor/css/jquery-ui.min.css') }}" rel="stylesheet"></script>
  <link href="{{ asset('quiz/assets/vendor/css/jHTree.css') }}" rel="stylesheet">


  <!-- =======================================================
  * Template Name: Mamba - v2.0.1
  * Template URL: https://bootstrapmade.com/mamba-one-page-bootstrap-template-free/
  * Author: BootstrapMade.com
  * License: https://bootstrapmade.com/license/
  ======================================================== -->
</head>
<style type="text/css">
  .jst-hours {
    float: left;
  }
  .jst-minutes {
    float: left;
  }
  .jst-seconds {
    float: left;
  }
  .jst-clearDiv {
    clear: both;
  }
</style><body>
  <main id="main">
    <!-- ======= About Lists Section ======= -->
    <section class="about-lists container">
        <div class="row">
            <div class="col-md-12 text-center">
                <img src="{{ asset('backend/assets/css/assets/images/logo-inverse-new.png') }}" class="img img-fluid" width="150" style="padding-bottom=20px;">
            </div>
        </div>
        <br>      
        <div class="row contact">
        <div class="col-md-12">
          <div class="alert alert-success table-responsive" style="color: black;padding: 0px;">
            <table class="table">
              <tr>
                <td>Nama Member: {{ Auth::user()->name }}</td>
                <td width="50%">Paket : {{ $detail->package->name }}</td>
                <td>
                  {{-- Waktu Selesai : <div id="clock" style="font-weight: bold;padding: 0px 2px 0px;" class="btn btn-danger btn-disable"></div> --}}
                </td>
              </tr>
            </table>
          </div>
        </div>
        <div class="col-lg-9 col-sm-12" id="t4_ujian">
          <div class="alert alert-primary" style="padding: 0px 10px 0px;">
            <hr>
            <h4>INSTRUKSI :</h4>
            <p>Pilihlah salah satu jawaban yang menurut anda benar. Kemudian klik Simpan dan lanjutkan</p>
            Waktu Selesai : <div id="clock" style="font-weight: bold;padding: 0px 2px 0px;" class="btn btn-danger btn-disable"></div>
            <hr>
          </div>
          <div class="tab-content">
            @foreach ($datas as $item)
            <div class="tab-pane {{ ($loop->iteration == 1) ? 'active' : '' }} " id="soal{{ $loop->iteration }}" role="tabpanel" aria-labelledby="soal{{ $loop->iteration }}-tab">
              <label><b>Soal: {{ $item->question->jenis }}</b></label>
              <p>{{ $loop->iteration }}. {{ $item->question->question }}</p><br />
                <form class="form" action="#" method="post" id="simpansoalno{{ $loop->iteration }}">
                <ol type="A">
                    <li>
                        <input type="radio" name="jawab" value="a" required="" {{ ($item->option_a === 1) ? 'checked' : '' }}>
                        &ensp;<label><p>{{ $item->question->option_a }}</p></label>
                    </li>
                    <li>
                        <input type="radio" name="jawab" value="b" required="" {{ ($item->option_b === 1) ? 'checked' : '' }}>
                        &ensp;<label><p>{{ $item->question->option_b }}</p></label>
                    </li>
                    <li>
                        <input type="radio" name="jawab" value="c" required="" {{ ($item->option_c === 1) ? 'checked' : '' }}>
                        &ensp;<label><p>{{ $item->question->option_c }}</p></label>
                    </li>
                    <li>
                      <input type="radio" name="jawab" value="d" required="" {{ ($item->option_d === 1) ? 'checked' : '' }}>
                      &ensp;<label><p>{{ $item->question->option_d }}</p></label>
                    </li>
                    <li>
                      <input type="radio" name="jawab" value="e" required="" {{ ($item->option_e === 1) ? 'checked' : '' }}>
                      &ensp;<label><p>{{ $item->question->option_e }}</p></label>
                    </li>
                </ol>
                @if($loop->iteration != 1)
                <button type="button" class="btn btn-warning btn-flat btn-sm" onclick="kembali({{ $loop->iteration }}-1)">Kembali</button>
                @endif
                <button type="submit" class="btn btn-success btn-flat btn-sm" onclick="simpan_lanjutkan({{ $loop->iteration }},{{ $item->id }})">Simpan & lanjutkan</button>
                @if($loop->iteration != $datas->count())
                <button type="button" class="btn btn-primary btn-flat btn-sm" onclick="lewati({{ $loop->iteration }}+1)">Lewati</button>
                @endif
              </form>
              </p>
            </div>
            @endforeach
            </div>
        </div>
        <div class="col-lg-3 col-sm-12" id="t4_nomor">
            <p>TIU</p>
            @foreach ($datas as $item)
              @php
                if ($item->option_a === 1) { $button = 'btn-success'; }
                elseif ($item->option_b === 1) { $button = 'btn-success'; }
                elseif ($item->option_c === 1) { $button = 'btn-success'; }
                elseif ($item->option_d === 1) { $button = 'btn-success'; }
                elseif ($item->option_e === 1) { $button = 'btn-success'; }
                else { $button = 'btn-warning'; }
              @endphp
              @if ($item->jenis == 'TIU')
              <button 
                class="btn btn-sm {{ $button }}" 
                onclick="openSoal({{ $loop->iteration }})" 
                style="width: 35px;margin-bottom:2px;" data="jawabid{{ $item->id }}" id="no{{ $loop->iteration }}">
                {{ $loop->iteration }}
              </button>
              @endif
            @endforeach
            <br>
            <hr>
            <p>TWK</p>
            @foreach ($datas as $item)
            @php
              if ($item->option_a === 1) { $button = 'btn-success'; }
              elseif ($item->option_b === 1) { $button = 'btn-success'; }
              elseif ($item->option_c === 1) { $button = 'btn-success'; }
              elseif ($item->option_d === 1) { $button = 'btn-success'; }
              elseif ($item->option_e === 1) { $button = 'btn-success'; }
              else { $button = 'btn-warning'; }
            @endphp
            @if ($item->jenis == 'TWK')
            <button 
              class="btn btn-sm {{ $button }}" 
              onclick="openSoal({{ $loop->iteration }})" 
              style="width: 35px;margin-bottom:2px;" data="jawabid{{ $item->id }}" id="no{{ $loop->iteration }}">
              {{ $loop->iteration }}
            </button>
            @endif
            @endforeach
            <hr>
            <p>TKP</p>
            @foreach ($datas as $item)
            @php
              if ($item->option_a === 1) { $button = 'btn-success'; }
              elseif ($item->option_b === 1) { $button = 'btn-success'; }
              elseif ($item->option_c === 1) { $button = 'btn-success'; }
              elseif ($item->option_d === 1) { $button = 'btn-success'; }
              elseif ($item->option_e === 1) { $button = 'btn-success'; }
              else { $button = 'btn-warning'; }
            @endphp
            @if ($item->jenis == 'TKP')
            <button 
              class="btn btn-sm {{ $button }}" 
              onclick="openSoal({{ $loop->iteration }})" 
              style="width: 35px;margin-bottom:2px;" data="jawabid{{ $item->id }}" id="no{{ $loop->iteration }}">
              {{ $loop->iteration }}
            </button>
            @endif
            @endforeach
          <br>
          <hr>
          <button onclick="selesai_ujian()" class="btn btn-sm btn-success btn-flat btn-block">Selesai Ujian</button>
        </div>
      </div>
    </section><!-- End About Lists Section -->
    <!-- Modal Gambar -->
    <!-- Modal -->
    <div class="modal fade" id="exampleModalCenter" tabindex="-1" role="dialog" aria-labelledby="exampleModalCenterTitle" aria-hidden="true">
      <div class="modal-dialog modal-dialog-centered modal-lg" role="document">
        <div class="modal-content">
          <div class="modal-header">
            <button type="button" class="close" data-dismiss="modal" aria-label="Close">
              <span aria-hidden="true">&times;</span>
            </button>
          </div>
          <div class="modal-body">
            
          </div>
        </div>
      </div>
    </div>
    <!-- END Modal Gambar -->
  </main><!-- End #main -->
  
  <!-- Vendor JS Files -->
  <script src="{{ asset('quiz/assets/vendor/js/jquery.min.js') }}"></script>
  <script src="{{ asset('quiz/assets/vendor/js/bootstrap.bundle.min.js') }}"></script>
  <script src="{{ asset('quiz/assets/vendor/js/jquery.easing.min.js') }}"></script>
  <script src="{{ asset('quiz/assets/vendor/js/validate.js') }}"></script>
  <script src="{{ asset('quiz/assets/vendor/js/jquery.sticky.js') }}"></script>
  <script src="{{ asset('quiz/assets/vendor/js/venobox.min.js') }}"></script>
  <script src="{{ asset('quiz/assets/vendor/js/jquery.waypoints.min.js') }}"></script>
  <script src="{{ asset('quiz/assets/vendor/js/counterup.min.js') }}"></script>
  <script src="{{ asset('quiz/assets/vendor/js/isotope.pkgd.min.js') }}"></script>
  <script src="{{ asset('quiz/assets/vendor/js/aos.js') }}"></script>

  <!-- Template Main JS File -->
  <script src="{{ asset('quiz/assets/vendor/js/main.js') }}"></script>

  <!-- Tree Family -->
  <script src="{{ asset('quiz/assets/vendor/js/jquery-ui.min.js') }}"></script>
  <script src="{{ asset('quiz/assets/vendor/js/jQuery.jHTree.js') }}"></script>

  <script src="{{ asset('quiz/assets/vendor/js/jquery.countdownTimer.js') }}"></script>
  <!-- auto scroll -->
  {{-- <script src="{{ asset('quiz/assets/vendor/') }}assets/vendor/auto-scroll/js/jquery.autoscroll.js"></script> --}}
</body>

</html>  
<script type="text/javascript">
  $.ajaxSetup({
      headers: {
          'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
      }
  });
</script>
<script type="text/javascript">
    // $(document).on("keydown", disableF5);
    $(document).on("keydown", disableCtrlR);

    // window.onbeforeunload = function(){
    //     return "Yay! Selesai ujian?";
    //   }

    $(function(){
      // document.documentElement.requestFullscreen();

      window.history.pushState(null, "", window.location.href);        
      window.onpopstate = function() {
            window.history.pushState(null, "", window.location.href);
        };

      $(window).on("contextmenu",function(){
        return false;
      });

      $("#clock").countdowntimer({
          startDate : new Date().getTime(),
          dateAndTime : '{{ $detail->end_time }}',
          size : "lg",
          displayFormat: "HMS",
          timeUp : load_hasil_ujian,
      });
    })

    function selesai_ujian(){
      if(confirm("Anda yakin sudah selesai?")){
        load_hasil_ujian();
      }
    }

    load_hasil_ujian = function(){
      $.ajax({
        url:`/review-quiz/{{ Request::segment(2) }}/{{ Request::segment(3) }}`,
        type:'get',
        beforeSend:function(){
          $("#t4_ujian").html("Memuat hasil ujian...");
          $("#t4_nomor").html("");
          // setTimeout(function() { 
          //     location.reload();
          // }, 30000); //reload
        },
        success:function(e){
          $("#t4_ujian").html(e);
        }
      })
    }

    function disableF5(e){
      if ((e.which || e.keyCode)== 116) e.preventDefault();
    }

    function disableCtrlR(e){
      if (e.ctrlKey) e.preventDefault();
    }

    function simpan_lanjutkan(no,id){
      var now = no+1;
      var jlh_soal = {{ $datas->count() }};
      
      if(now > jlh_soal){
        now = 1;
      }

      $(".tab-pane").removeClass('active');
      $("#soal"+now).addClass('active');

      $("#simpansoalno"+no).on("submit",function(){
        $.ajax({
          url : '/store-quiz/store/'+id,
          method : 'POST',
          data : $(this).serialize(),
          beforeSend:function(){

          },
          success:function(e){
            if(e=="OK"){
              $("#no"+no).attr("class","btn btn-sm btn-success");
            }
          },
          error:function(x){
            $("#no"+no).attr("class","btn btn-sm btn-warning");
          }
        })
        return false;
      })
    }

    function lewati(no){
      var jlh_soal = 30;
      
      if(no > jlh_soal){
        no = 1;
      }

      $(".tab-pane").removeClass('active');
      $("#soal"+no).addClass('active');
    }

    function openSoal(no){
      $(".tab-pane").removeClass('active');
      $("#soal"+no).addClass('active');
    }

    function kembali(no){
      $(".tab-pane").removeClass('active');
      $("#soal"+no).addClass('active');
    }

    function zoomx(pict){
      $(".modal-body").html('<img class="img img-fluid img-thumbnail" src="https://demo.njuah-njuah.com/assets/uploads/'+pict+'">');
      $("#exampleModalCenter").modal('toggle');
    }

  </script>