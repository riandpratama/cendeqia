<!DOCTYPE html>
<html lang="en">

<head>
  <meta charset="utf-8">
  <meta content="width=device-width, initial-scale=1.0" name="viewport">

  <title>CENDEQIA</title>
  <meta content="Simulasi CBT Online" name="descriptions">
  <meta content="cendeqia, cendeqia.org, cendeqiagroup, cendeqiaedu, cpns, try out, try out online, simulasi cpns" name="keywords">

  <!-- Favicons -->
  <link href="{{ asset('frontend/assets/image/ICON.png')}}" rel="icon">
  <link href="{{ asset('frontend/assets/image/ICON.png')}}" rel="apple-touch-icon">

  <!-- Google Fonts -->
  <link href="https://fonts.googleapis.com/css?family=Open+Sans:300,300i,400,400i,600,600i,700,700i|Raleway:300,300i,400,400i,500,500i,600,600i,700,700i|Poppins:300,300i,400,400i,500,500i,600,600i,700,700i" rel="stylesheet">

  <!-- Vendor CSS Files -->
  <link href="{{ asset('frontend/assets/vendor/bootstrap/css/bootstrap.min.css') }}" rel="stylesheet">
  <link href="{{ asset('frontend/assets/vendor/icofont/icofont.min.css') }}" rel="stylesheet">
  <link href="{{ asset('frontend/assets/vendor/boxicons/css/boxicons.min.css') }}" rel="stylesheet">
  <link href="{{ asset('frontend/assets/vendor/animate.css/animate.min.css') }}" rel="stylesheet">
  <link href="{{ asset('frontend/assets/vendor/remixicon/remixicon.css') }}" rel="stylesheet">
  <link href="{{ asset('frontend/assets/vendor/owl.carousel/assets/owl.carousel.min.css') }}" rel="stylesheet">
  <link href="{{ asset('frontend/assets/vendor/venobox/venobox.css') }}" rel="stylesheet">
  <link href="{{ asset('frontend/assets/vendor/aos/aos.css') }}" rel="stylesheet">

  <!-- Template Main CSS File -->
  <link href="{{ asset('frontend/assets/css/style.css') }}" rel="stylesheet">

</head>

<body>
  <header id="header" class="fixed-top ">
    <div class="container d-flex align-items-center">
      {{-- <h1 class="logo mr-auto" style="font-weight: bold;color: #2A75AE;"><a href="#header" class="scrollto">CENDE<span style="color: #FCB71A;">Q</span>IA</a></h1> --}}
      <h1 class="logo mr-auto" style="font-weight: bold;color: #2A75AE;">
      <img src="{{ asset('frontend/assets/image/LOGO.png') }}" style="width: 25%" alt="">
      </h1>
      <nav class="nav-menu d-none d-lg-block">
        <ul>
          <li class="active"><a href="#header">HOME</a></li>
          <li><a href="/about">TENTANG</a></li>
          <li><a href="/faq">FAQ</a></li>
          <li><a href="/contact">KONTAK</a></li>
        </ul>
      </nav>

    </div>
  </header>

  <main id="main">

    <section id="icon-boxes" class="icon-boxes">
      <div class="container">

        <div class="row">
          <div class="col-md-4 col-lg-4 d-flex align-items-stretch mb-5 mb-lg-0" data-aos="fade-up">
            <a href="{{ route('login') }}" style="width: 100%">
              <div class="icon-box" style="width: 100%">
                <center>
                <div class="icon"><i class="bx bx-file"></i></div>
                <h4 class="title">CPNS<span style="color: #FCB71A">mart</span></h4>
                </center>
              </div>
            </a>
          </div>

          <div class="col-md-4 col-lg-4 d-flex align-items-stretch mb-5 mb-lg-0" data-aos="fade-up">
            <a href="{{ route('kedinasan.login') }}" style="width: 100%">
              <div class="icon-box" style="width: 100%">
                <center>
                <div class="icon"><i class="bx bx-briefcase"></i></div>
                <h4 class="title" style="font-size:30px; padding-top:10px; ">Sekolah<span style="color: #FCB71A"> Kedinasan</span></h4>
                </center>
              </div>
            </a>
          </div>

          <div class="col-md-4 col-lg-4 d-flex align-items-stretch mb-5 mb-lg-0" data-aos="fade-up" data-aos-delay="200">
            <div class="icon-box" style="width: 100%; background-color: #AFAFAF;">
              <center>
              <div class="icon"><i class="bx bx-lock" style="color:#656565;"></i></div>
              <h4 class="title" style="font-size:30px; padding-top:10px;color:#656565;">Beasiswa</h4>
              </center>
            </div>
          </div>
        </div>

      </div>
    </section>

    <section id="icon-boxes" class="icon-boxes-2">
      <div class="container">

        <div class="row">
          <div class="col-md-4 col-lg-4 d-flex align-items-stretch mb-5 mb-lg-0" data-aos="fade-up" data-aos-delay="300">
            <div class="icon-box" style="width: 100%; background-color: #AFAFAF;">
              <center>
              <div class="icon"><i class="bx bx-lock" style="color:#656565;"></i></div>
              <h4 class="title" style="font-size:30px; padding-top:10px;color:#656565;">Ujian Kompetensi</h4>
              </center>
            </div>
          </div>

          <div class="col-md-4 col-lg-4 d-flex align-items-stretch mb-5 mb-lg-0" data-aos="fade-up" data-aos-delay="400">
            <div class="icon-box" style="width: 100%; background-color: #AFAFAF;">
              <center>
              <div class="icon"><i class="bx bx-lock" style="color:#656565;"></i></div>
              <h4 class="title" style="font-size:30px; padding-top:10px;color:#656565;">English Test</h4>
              </center>
            </div>
          </div>

          <div class="col-md-4 col-lg-4 d-flex align-items-stretch mb-5 mb-lg-0" data-aos="fade-up" data-aos-delay="500">
            <div class="icon-box" style="width: 100%; background-color: #AFAFAF;">
              <center>
              <div class="icon"><i class="bx bx-lock" style="color:#656565;"></i></div>
              <h4 class="title" style="font-size:30px; padding-top:10px;color:#656565;">Ujian Kampus</h4>
              </center>
            </div>
          </div>

        </div>
      </div>
    </section>
  </main>

  <a href="#" class="back-to-top"><i class="ri-arrow-up-line"></i></a>
  <div id="preloader"></div>

  <!-- Vendor JS Files -->
  <script src="{{ asset('frontend/assets/vendor/jquery/jquery.min.js') }}"></script>
  <script src="{{ asset('frontend/assets/vendor/bootstrap/js/bootstrap.bundle.min.js') }}"></script>
  <script src="{{ asset('frontend/assets/vendor/jquery.easing/jquery.easing.min.js') }}"></script>
  <script src="{{ asset('frontend/assets/vendor/php-email-form/validate.js') }}"></script>
  <script src="{{ asset('frontend/assets/vendor/owl.carousel/owl.carousel.min.js') }}"></script>
  <script src="{{ asset('frontend/assets/vendor/venobox/venobox.min.js') }}"></script>
  <script src="{{ asset('frontend/assets/vendor/isotope-layout/isotope.pkgd.min.js') }}"></script>
  <script src="{{ asset('frontend/assets/vendor/aos/aos.js') }}"></script>

  <!-- Template Main JS File -->
  <script src="{{ asset('frontend/assets/js/main.js') }}"></script>
  <!-- Hotjar Tracking Code for cendeqia.org -->
  <script>
      (function(h,o,t,j,a,r){
          h.hj=h.hj||function(){(h.hj.q=h.hj.q||[]).push(arguments)};
          h._hjSettings={hjid:2236252,hjsv:6};
          a=o.getElementsByTagName('head')[0];
          r=o.createElement('script');r.async=1;
          r.src=t+h._hjSettings.hjid+j+h._hjSettings.hjsv;
          a.appendChild(r);
      })(window,document,'https://static.hotjar.com/c/hotjar-','.js?sv=');
  </script>

</body>

</html>