<!DOCTYPE html>
<html lang="en">

<head>
  <meta charset="utf-8">
  <meta content="width=device-width, initial-scale=1.0" name="viewport">

  <title>CENDEQIA - KONTAK</title>
  <meta content="Simulasi CBT Online" name="descriptions">
  <meta content="cendeqia, cendeqia.org, cendeqiagroup, cendeqiaedu, cpns, try out, try out online, simulasi cpns" name="keywords">

  <!-- Favicons -->
  <link href="{{ asset('frontend/assets/image/ICON.png')}}" rel="icon">
  <link href="{{ asset('frontend/assets/image/ICON.png')}}" rel="apple-touch-icon">

  <!-- Google Fonts -->
  <link href="https://fonts.googleapis.com/css?family=Open+Sans:300,300i,400,400i,600,600i,700,700i|Raleway:300,300i,400,400i,500,500i,600,600i,700,700i|Poppins:300,300i,400,400i,500,500i,600,600i,700,700i" rel="stylesheet">

  <!-- Vendor CSS Files -->
  <link href="{{ asset('frontend/assets/vendor/bootstrap/css/bootstrap.min.css') }}" rel="stylesheet">
  <link href="{{ asset('frontend/assets/vendor/icofont/icofont.min.css') }}" rel="stylesheet">
  <link href="{{ asset('frontend/assets/vendor/boxicons/css/boxicons.min.css') }}" rel="stylesheet">
  <link href="{{ asset('frontend/assets/vendor/animate.css/animate.min.css') }}" rel="stylesheet">
  <link href="{{ asset('frontend/assets/vendor/remixicon/remixicon.css') }}" rel="stylesheet">
  <link href="{{ asset('frontend/assets/vendor/owl.carousel/assets/owl.carousel.min.css') }}" rel="stylesheet">
  <link href="{{ asset('frontend/assets/vendor/venobox/venobox.css') }}" rel="stylesheet">
  <link href="{{ asset('frontend/assets/vendor/aos/aos.css') }}" rel="stylesheet">

  <!-- Template Main CSS File -->
  <link href="{{ asset('frontend/assets/css/style.css') }}" rel="stylesheet">
  <style>
    body {
      background: #2D77AF !important;
    }
    #header {
    background: #2D77AF !important;
    }
    .contact:before {
        content: "";
        background: rgba(255, 255, 255, 0.9);
        position: absolute;
        bottom: 0;
        top: 0;
        left: 0;
        right: 0;
    }
    #footer {
        background: #2D77AF;
        /*margin-top: 150px;*/
        /*padding: 0 0 0px 0;*/
        bottom: 0px;
        color: #fff;
        font-size: 14px;
    }
  </style>
</head>

<body>
  <header id="header" class="fixed-top ">
    <div class="container d-flex align-items-center">
      <h1 class="logo mr-auto" style="font-weight: bold;color: #2A75AE;">
      <img src="{{ asset('frontend/assets/image/LOGO.png') }}" style="width: 25%" alt="">
      </h1>
      <nav class="nav-menu d-none d-lg-block">
        <ul>
          <li><a href="/">HOME</a></li>
          <li><a href="/about">TENTANG</a></li>
          <li><a href="/faq">FAQ</a></li>
          <li class="active"><a href="/contact">KONTAK</a></li>
        </ul>
      </nav><!-- .nav-menu -->

    </div>
  </header><!-- End Header -->

  <main id="main">
    {{-- <section id="icon-boxes" class="icon-boxes"></section> --}}
    <section id="contact" class="contact" style="padding-top: 150px; padding-bottom: 90px;">
      <div class="container" data-aos="fade-up">

        <div class="section-title">
          <h2>KONTAK</h2>
        </div>

        <div class="row mt-1 d-flex justify-content-end" data-aos="fade-right" data-aos-delay="100">

          <div class="col-lg-8">
            <div class="info">

              @foreach($data as $item)
              <div class="{{$item->class}}">
                <i class="{{$item->icon}}"></i>
                <h4>{{$item->title}}</h4>
                <a href="{{$item->link}}" target="_blank">
                  {!! $item->description !!}
                </a>
              </div>
              @endforeach

              {{-- <div class="address">
                <i class="icofont-google-map"></i>
                <h4>Alamat:</h4>
                <a href="https://www.google.com/maps/place/Jl.+Jambangan+I+No.6,+RT.001%2FRW.01,+Jambangan,+Kec.+Jambangan,+Kota+SBY,+Jawa+Timur+60232,+Indonesia/@-7.314028,112.712864,13z/data=!4m5!3m4!1s0x2dd7fc8228ebe78b:0xc016f702522931b5!8m2!3d-7.314028!4d112.712864?hl=en-US" target="_blank">
                  <p>Jambangan Indah 2 Kavling 3</p>
                  <p>Surabaya, Jawa Timur</p>
                </a>
              </div>

              <div class="email">
                <i class="icofont-envelope"></i>
                <h4>Email:</h4>
                <a href="mailto:sekawancendeqia@gmail.com"><p>sekawancendeqia@gmail.com</p></a>
              </div>

              <div class="phone">
                <i class="icofont-phone"></i>
                <h4>WhatsApp:</h4>
                <a href="https://api.whatsapp.com/send?phone=6285157433521&text=Hai+kak+saya+mau+tanya+nih+%3F" target="_blank">
                  <p>+6285 - 517 - 433 - 521</p>
                </a>
              </div>

              <div class="phone">
                <i class="icofont-instagram"></i>
                <h4>Instagram:</h4>
                <a href="https://www.instagram.com/cendeqia_group/" target="_blank">
                  <p>@cendeqia_group</p>
                </a>
              </div> --}}

            </div>

          </div>

        </div>

      </div>
    </section>

  </main>

  <!-- ======= Footer ======= -->
  <footer id="footer">
    <div class="container">
      <div class="copyright">
        &copy; Copyright <strong><span>Cendeqia</span></strong>. All Rights Reserved
      </div>
    </div>
  </footer><!-- End Footer -->

  <a href="#" class="back-to-top"><i class="ri-arrow-up-line"></i></a>
  <div id="preloader"></div>

  <!-- Vendor JS Files -->
  <script src="{{ asset('frontend/assets/vendor/jquery/jquery.min.js') }}"></script>
  <script src="{{ asset('frontend/assets/vendor/bootstrap/js/bootstrap.bundle.min.js') }}"></script>
  <script src="{{ asset('frontend/assets/vendor/jquery.easing/jquery.easing.min.js') }}"></script>
  <script src="{{ asset('frontend/assets/vendor/php-email-form/validate.js') }}"></script>
  <script src="{{ asset('frontend/assets/vendor/owl.carousel/owl.carousel.min.js') }}"></script>
  <script src="{{ asset('frontend/assets/vendor/venobox/venobox.min.js') }}"></script>
  <script src="{{ asset('frontend/assets/vendor/isotope-layout/isotope.pkgd.min.js') }}"></script>
  <script src="{{ asset('frontend/assets/vendor/aos/aos.js') }}"></script>

  <!-- Template Main JS File -->
  <script src="{{ asset('frontend/assets/js/main.js') }}"></script>

</body>

</html>