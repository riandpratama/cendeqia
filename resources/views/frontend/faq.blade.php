<!DOCTYPE html>
<html lang="en">

<head>
  <meta charset="utf-8">
  <meta content="width=device-width, initial-scale=1.0" name="viewport">

  <title>CENDEQIA - FAQ</title>
  <meta content="Simulasi CBT Online" name="descriptions">
  <meta content="cendeqia, cendeqia.org, cendeqiagroup, cendeqiaedu, cpns, try out, try out online, simulasi cpns" name="keywords">

  <!-- Favicons -->
  <link href="{{ asset('frontend/assets/image/ICON.png')}}" rel="icon">
  <link href="{{ asset('frontend/assets/image/ICON.png')}}" rel="apple-touch-icon">

  <!-- Google Fonts -->
  <link href="https://fonts.googleapis.com/css?family=Open+Sans:300,300i,400,400i,600,600i,700,700i|Raleway:300,300i,400,400i,500,500i,600,600i,700,700i|Poppins:300,300i,400,400i,500,500i,600,600i,700,700i" rel="stylesheet">

  <!-- Vendor CSS Files -->
  <link href="{{ asset('frontend/assets/vendor/bootstrap/css/bootstrap.min.css') }}" rel="stylesheet">
  <link href="{{ asset('frontend/assets/vendor/icofont/icofont.min.css') }}" rel="stylesheet">
  <link href="{{ asset('frontend/assets/vendor/boxicons/css/boxicons.min.css') }}" rel="stylesheet">
  <link href="{{ asset('frontend/assets/vendor/animate.css/animate.min.css') }}" rel="stylesheet">
  <link href="{{ asset('frontend/assets/vendor/remixicon/remixicon.css') }}" rel="stylesheet">
  <link href="{{ asset('frontend/assets/vendor/owl.carousel/assets/owl.carousel.min.css') }}" rel="stylesheet">
  <link href="{{ asset('frontend/assets/vendor/venobox/venobox.css') }}" rel="stylesheet">
  <link href="{{ asset('frontend/assets/vendor/aos/aos.css') }}" rel="stylesheet">

  <!-- Template Main CSS File -->
  <link href="{{ asset('frontend/assets/css/style.css') }}" rel="stylesheet">
  <style>
    body {
      background: #2D77AF !important;
    }
    #header {
    background: #2D77AF !important;
    }
    #footer {
        background: #2D77AF;
        /*padding: 0 0 0px 0;*/
        bottom: 0;
        color: #fff;
        font-size: 14px;
    }
  </style>
</head>

<body>
  <!-- ======= Header ======= -->
  <header id="header" class="fixed-top ">
    <div class="container d-flex align-items-center">
      <h1 class="logo mr-auto" style="font-weight: bold;color: #2A75AE;">
      <img src="{{ asset('frontend/assets/image/LOGO.png') }}" style="width: 25%" alt="">
      </h1>
      <nav class="nav-menu d-none d-lg-block">
        <ul>
          <li><a href="/">HOME</a></li>
          <li><a href="/about">TENTANG</a></li>
          <li class="active"><a href="/faq">FAQ</a></li>
          <li><a href="/contact">KONTAK</a></li>
        </ul>
      </nav><!-- .nav-menu -->

    </div>
  </header><!-- End Header -->

  <main id="main">

    <!-- ======= Icon Boxes Section ======= -->

    <!-- ======= Frequently Asked Questions Section ======= -->
    <section id="faq" class="faq section-bg" style="padding-top: 150px;">
      <div class="container" data-aos="fade-up">

        <div class="section-title">
          <h2>Frequently Asked Questions</h2>
        </div>

        <div class="faq-list">
          <ul>
            @foreach($data as $item)
            <li data-aos="fade-up" data-aos-delay="{{$item->id}}00">
              <i class="bx bx-help-circle icon-help"></i> <a data-toggle="collapse" class="collapse" href="#faq-list-{{$item->id}}">
              {{$item->title}}
               <i class="bx bx-chevron-down icon-show"></i><i class="bx bx-chevron-up icon-close"></i></a>
              <div id="faq-list-{{$item->id}}" class="collapse {{$item->id == 1 ? 'show' : '' }}" data-parent=".faq-list">
                {!!$item->description!!}
              </div>
            </li>
            @endforeach

            {{--

            <li data-aos="fade-up" data-aos-delay="200">
              <i class="bx bx-help-circle icon-help"></i> <a data-toggle="collapse" href="#faq-list-2" class="collapsed">
                Terdapat pilihan CPNSmart, LPDP dll, namun beberapa masih belum aktif?
              <i class="bx bx-chevron-down icon-show"></i><i class="bx bx-chevron-up icon-close"></i></a>
              <div id="faq-list-2" class="collapse" data-parent=".faq-list">
                <p>
                  Saat ini pelatihan yang tersedia masih CPNSmart. Untuk pelatihan lainnya segera akan diaktifkan.
                </p>
              </div>
            </li>

            <li data-aos="fade-up" data-aos-delay="300">
              <i class="bx bx-help-circle icon-help"></i> <a data-toggle="collapse" href="#faq-list-3" class="collapsed">
                Apa itu CPNSmart?
              <i class="bx bx-chevron-down icon-show"></i><i class="bx bx-chevron-up icon-close"></i></a>
              <div id="faq-list-3" class="collapse" data-parent=".faq-list">
                <p>
                  CPNSmart adalah salah satu produk unggulan dari CENDEQIA yang menyediakan paket soal SKD CPNS dengan simulasi CAT.
                </p>
              </div>
            </li>

            <li data-aos="fade-up" data-aos-delay="400">
              <i class="bx bx-help-circle icon-help"></i> <a data-toggle="collapse" href="#faq-list-4" class="collapsed">
                Apa saja fitur CPNSmart?
              <i class="bx bx-chevron-down icon-show"></i><i class="bx bx-chevron-up icon-close"></i></a>
              <div id="faq-list-4" class="collapse" data-parent=".faq-list">
                <p>
                  1. Saat ini Kami menyediakan 40 paket soal SKD CPNS beserta pembahasan. <br>
                  2. Semua paket soal dengan simulasi CAT.  <br>
                  3. Masa aktif 1 tahun. <br>
                </p>
              </div>
            </li>

            <li data-aos="fade-up" data-aos-delay="500">
              <i class="bx bx-help-circle icon-help"></i> <a data-toggle="collapse" href="#faq-list-5" class="collapsed">
                Bagaimana cara mendaftar?
              <i class="bx bx-chevron-down icon-show"></i><i class="bx bx-chevron-up icon-close"></i></a>
              <div id="faq-list-5" class="collapse" data-parent=".faq-list">
                <p>
                  Terdapat 2 cara: <br>
                  1. Kunjugi web cendeqia.com, Anda akan diarahkan ke admin untuk konfirmasi pembayaran.<br>
                  2. Langsung menghubungi admin 0851 5743 3521.<br>
                </p>
              </div>
            </li>

            <li data-aos="fade-up" data-aos-delay="600">
              <i class="bx bx-help-circle icon-help"></i> <a data-toggle="collapse" href="#faq-list-6" class="collapsed">
                Berapa biaya pendaftaran?
              <i class="bx bx-chevron-down icon-show"></i><i class="bx bx-chevron-up icon-close"></i></a>
              <div id="faq-list-6" class="collapse" data-parent=".faq-list">
                <p>
                  Anda hanya perlu membayar Rp. 59.000.
                </p>
              </div>
            </li>

            <li data-aos="fade-up" data-aos-delay="700">
              <i class="bx bx-help-circle icon-help"></i> <a data-toggle="collapse" href="#faq-list-7" class="collapsed">
                Bagaimana cara membayar?
              <i class="bx bx-chevron-down icon-show"></i><i class="bx bx-chevron-up icon-close"></i></a>
              <div id="faq-list-7" class="collapse" data-parent=".faq-list">
                <p>
                  Anda dapat transfer ke salah satu rekening atau dompet digital berikut: <br>
                  • Transfer ke rekening Mandiri 142-00-1857964-8 a.n PRADA WISNU SANJAYA. <br>
                  • OVO <br>
                  • Dana <br>
                  • Gopay <br>
                  • Linkaja <br>
                </p>
              </div>
            </li>

            <li data-aos="fade-up" data-aos-delay="800">
              <i class="bx bx-help-circle icon-help"></i> <a data-toggle="collapse" href="#faq-list-8" class="collapsed">
                Bagaimana konfirmasi pembayaran?
              <i class="bx bx-chevron-down icon-show"></i><i class="bx bx-chevron-up icon-close"></i></a>
              <div id="faq-list-8" class="collapse" data-parent=".faq-list">
                <p>
                  Konfirmasi pembayaran dengan cara mengirim bukti transfer ke nomor admin 0851 5743 3521.
                </p>
              </div>
            </li>

            <li data-aos="fade-up" data-aos-delay="900">
              <i class="bx bx-help-circle icon-help"></i> <a data-toggle="collapse" href="#faq-list-9" class="collapsed">
                Apakah CPNSmart dapat diakses offline?
              <i class="bx bx-chevron-down icon-show"></i><i class="bx bx-chevron-up icon-close"></i></a>
              <div id="faq-list-9" class="collapse" data-parent=".faq-list">
                <p>
                  Saat ini CPNSmart hanya bisa diakses online. CPNSmart diakses melalui website dengan login terlebih dahulu. Bisa dikerjakan melalui PC/Laptop dan HP. Kedepannya Kami akan mengembangkan CPNSmart agar tersedia di playstore dan appstore, untuk bisa dikerjakan offline.
                </p>
              </div>
            </li>

            <li data-aos="fade-up" data-aos-delay="1000">
              <i class="bx bx-help-circle icon-help"></i> <a data-toggle="collapse" href="#faq-list-10" class="collapsed">
                Apakah ada waktu tertentu untuk mengisi paket soal yang disediakan ?
              <i class="bx bx-chevron-down icon-show"></i><i class="bx bx-chevron-up icon-close"></i></a>
              <div id="faq-list-10" class="collapse" data-parent=".faq-list">
                <p>
                  Kami tidak menentukan waktu atau jam tertentu. Anda bisa mengatur waktu atau kapan Anda mau mengerjakannya. Masa aktif 1 tahun setelah melakukan pembayaran.
                </p>
              </div>
            </li>

            <li data-aos="fade-up" data-aos-delay="1100">
              <i class="bx bx-help-circle icon-help"></i> <a data-toggle="collapse" href="#faq-list-11" class="collapsed">
                Apakah bisa mengulangi mengisi paket soal yang sama ?
              <i class="bx bx-chevron-down icon-show"></i><i class="bx bx-chevron-up icon-close"></i></a>
              <div id="faq-list-11" class="collapse" data-parent=".faq-list">
                <p>
                  Bisa, Anda dapat mengulangi paket selama masa aktif masih ada.
                </p>
              </div>
            </li>

            <li data-aos="fade-up" data-aos-delay="1200">
              <i class="bx bx-help-circle icon-help"></i> <a data-toggle="collapse" href="#faq-list-12" class="collapsed">
                Apakah ada bimbingan untuk Seleksi Kemapuan Bidang (SKB)?
              <i class="bx bx-chevron-down icon-show"></i><i class="bx bx-chevron-up icon-close"></i></a>
              <div id="faq-list-12" class="collapse" data-parent=".faq-list">
                <p>
                  Untuk saat ini CPNSmart masih belum ada fitur bimbingan SKB. Kedepannya Kami akan mengembangkan CPNSmart agar terdapat fitur SKB.
                </p>
              </div> --}}
            </li>

          </ul>
        </div>

      </div>
    </section>

  </main><!-- End #main -->

  <!-- ======= Footer ======= -->
  <footer id="footer">
    <div class="container">
      <div class="copyright">
        &copy; Copyright <strong><span>Cendeqia</span></strong>. All Rights Reserved
      </div>
    </div>
  </footer><!-- End Footer -->

  <a href="#" class="back-to-top"><i class="ri-arrow-up-line"></i></a>
  <div id="preloader"></div>

  <!-- Vendor JS Files -->
  <script src="{{ asset('frontend/assets/vendor/jquery/jquery.min.js') }}"></script>
  <script src="{{ asset('frontend/assets/vendor/bootstrap/js/bootstrap.bundle.min.js') }}"></script>
  <script src="{{ asset('frontend/assets/vendor/jquery.easing/jquery.easing.min.js') }}"></script>
  <script src="{{ asset('frontend/assets/vendor/php-email-form/validate.js') }}"></script>
  <script src="{{ asset('frontend/assets/vendor/owl.carousel/owl.carousel.min.js') }}"></script>
  <script src="{{ asset('frontend/assets/vendor/venobox/venobox.min.js') }}"></script>
  <script src="{{ asset('frontend/assets/vendor/isotope-layout/isotope.pkgd.min.js') }}"></script>
  <script src="{{ asset('frontend/assets/vendor/aos/aos.js') }}"></script>

  <!-- Template Main JS File -->
  <script src="{{ asset('frontend/assets/js/main.js') }}"></script>

</body>

</html>