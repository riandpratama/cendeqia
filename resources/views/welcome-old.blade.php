
<!DOCTYPE html>
<html lang="en">

<head><meta http-equiv="Content-Type" content="text/html; charset=euc-kr">
  
  <meta content="width=device-width, initial-scale=1.0" name="viewport">

  <title>Ujian</title>
  <meta content="" name="descriptison">
  <meta content="" name="keywords">

  <!-- Favicons -->
  <link rel="shortcut icon" type="image/x-icon" href="https://demo.njuah-njuah.com/assets/img/logo.png" />

  <!-- Google Fonts -->
  <link href="https://fonts.googleapis.com/css?family=Open+Sans:300,300i,400,400i,600,600i,700,700i|Raleway:300,300i,400,400i,600,600i,700,700i,900" rel="stylesheet">

  <!-- Vendor CSS Files -->
  <link href="https://njuah-njuah.com/site/assets/vendor/bootstrap/css/bootstrap.min.css" rel="stylesheet">
  <link href="https://njuah-njuah.com/site/assets/vendor/icofont/icofont.min.css" rel="stylesheet">
  <link href="https://njuah-njuah.com/site/assets/vendor/boxicons/css/boxicons.min.css" rel="stylesheet">
  <link href="https://njuah-njuah.com/site/assets/vendor/animate.css/animate.min.css" rel="stylesheet">
  <link href="https://njuah-njuah.com/site/assets/vendor/venobox/venobox.css" rel="stylesheet">
  <link href="https://njuah-njuah.com/site/assets/vendor/aos/aos.css" rel="stylesheet">

  <!-- Template Main CSS File -->
  <link href="https://demo.njuah-njuah.com/assets/css/style.css" rel="stylesheet">

  <!-- Tree Family -->
  <link href="https://njuah-njuah.com/site/assets/vendor/jquery-ui-1.12.1/jquery-ui.min.css" rel="stylesheet"></script>
  <link href="https://njuah-njuah.com/site/assets/vendor/family-tree/css/jHTree.css" rel="stylesheet">


  <!-- =======================================================
  * Template Name: Mamba - v2.0.1
  * Template URL: https://bootstrapmade.com/mamba-one-page-bootstrap-template-free/
  * Author: BootstrapMade.com
  * License: https://bootstrapmade.com/license/
  ======================================================== -->
</head>
<style type="text/css">
  .jst-hours {
    float: left;
  }
  .jst-minutes {
    float: left;
  }
  .jst-seconds {
    float: left;
  }
  .jst-clearDiv {
    clear: both;
  }
</style><body>
  <main id="main">
    <!-- ======= About Lists Section ======= -->
    <section class="about-lists">
            <div class="row">
            <div class="col-md-12 text-center">
                <img src="https://demo.njuah-njuah.com/assets/img/logo.png" class="img img-fluid" width="50" style="padding-bottom=20px;">
            </div>
            <div class="col-md-12 text-center">
                <h1>CAT</h1>
            </div>
            
        </div>
        <br>      <div class="row contact">
        <div class="col-md-12">
          <div class="alert alert-success table-responsive" style="color: black;padding: 0px;">
            <table class="table">
              <tr>
                <td>Nama : Tim Surabaya</td>
                <td>Asal Ujian : Surabaya</td>
                <td>Pendidikan : STRATA 3</td>
                <td>
                  Waktu Selesai : <div id="clock" style="font-weight: bold;padding: 0px 2px 0px;" class="btn btn-danger btn-disable"></div>
                </td>
              </tr>
            </table>
          </div>
        </div>
        <div class="col-lg-9 col-sm-12" id="t4_ujian">
          <div class="alert alert-warning" style="padding: 0px 10px 0px;">
            <h4>INSTRUKSI :</h4>
            <p>Pilihlah salah satu jawaban yang menurut anda benar. Kemudian klik Simpan dan lanjutkan</p>
          </div>

          <div class="tab-content">
                        <div class="tab-pane active" id="soal1" role="tabpanel" aria-labelledby="soal1-tab">
              <label><b>Soal : TKP</b></label>
              <p>
                1. <p>Dalam sumber tata hukum di Indonesia Lorem ipsum dolor sit amet, consectetur adipiscing elit. DIPLOMA 3</p><br />
                                <form class="form" action="#" method="post" id="simpansoalno1">
                <ol type="A">
                  <li>
                      <input type="radio" name="jawab" value="a" required="">&ensp;<label><p>Dalam sumber tata hukum di Indonesia Lorem ipsum dolor sit amet, consectetur adipiscing elit.</p>
</label>
                                          </li>
                    <li>
                      <input type="radio" name="jawab" value="b" required="">&ensp;<label><p>Dalam sumber tata hukum di Indonesia Lorem ipsum dolor sit amet, consectetur adipiscing elit.</p>
</label>
                                          </li>
                    <li>
                      <input type="radio" name="jawab" value="c" required="">&ensp;<label><p>Dalam sumber tata hukum di Indonesia Lorem ipsum dolor sit amet, consectetur adipiscing elit.</p>
</label>
                                          </li>
                    <li>
                      <input type="radio" name="jawab" value="d" required="">&ensp;<label><p>Dalam sumber tata hukum di Indonesia Lorem ipsum dolor sit amet, consectetur adipiscing elit.</p>
</label>
                                          </li>
                    <li>
                      <input type="radio" name="jawab" value="e" required="">&ensp;<label><p>Dalam sumber tata hukum di Indonesia Lorem ipsum dolor sit amet, consectetur adipiscing elit.</p>
</label>
                                          </li>
                </ol>
                                <button type="submit" class="btn btn-success btn-flat btn-sm" onclick="simpan_lanjutkan(1,30)">Simpan & lanjutkan</button>
                <button type="button" class="btn btn-success btn-flat btn-sm" onclick="lewati('2')">Lewati</button>
              </form>
              </p>
            </div>
                        <div class="tab-pane " id="soal2" role="tabpanel" aria-labelledby="soal2-tab">
              <label><b>Soal : TKP</b></label>
              <p>
                2. <p>Dalam sumber tata hukum di Indonesia Lorem ipsum dolor sit amet, consectetur adipiscing elit. DIPLOMA 3</p><br />
                                <form class="form" action="#" method="post" id="simpansoalno2">
                <ol type="A">
                  <li>
                      <input type="radio" name="jawab" value="a" required="">&ensp;<label><p>Dalam sumber tata hukum di Indonesia Lorem ipsum dolor sit amet, consectetur adipiscing elit.</p>
</label>
                                          </li>
                    <li>
                      <input type="radio" name="jawab" value="b" required="">&ensp;<label><p>Dalam sumber tata hukum di Indonesia Lorem ipsum dolor sit amet, consectetur adipiscing elit.</p>
</label>
                                          </li>
                    <li>
                      <input type="radio" name="jawab" value="c" required="">&ensp;<label><p>Dalam sumber tata hukum di Indonesia Lorem ipsum dolor sit amet, consectetur adipiscing elit.</p>
</label>
                                          </li>
                    <li>
                      <input type="radio" name="jawab" value="d" required="">&ensp;<label><p>Dalam sumber tata hukum di Indonesia Lorem ipsum dolor sit amet, consectetur adipiscing elit.</p>
</label>
                                          </li>
                    <li>
                      <input type="radio" name="jawab" value="e" required="">&ensp;<label><p>Dalam sumber tata hukum di Indonesia Lorem ipsum dolor sit amet, consectetur adipiscing elit.</p>
</label>
                                          </li>
                </ol>
                                <button type="button" class="btn btn-warning btn-flat btn-sm" onclick="kembali('1')">Kembali</button>
                                <button type="submit" class="btn btn-success btn-flat btn-sm" onclick="simpan_lanjutkan(2,28)">Simpan & lanjutkan</button>
                <button type="button" class="btn btn-success btn-flat btn-sm" onclick="lewati('3')">Lewati</button>
              </form>
              </p>
            </div>
                        <div class="tab-pane " id="soal3" role="tabpanel" aria-labelledby="soal3-tab">
              <label><b>Soal : TKP</b></label>
              <p>
                3. <p>Soal ini jawabannya A</p><br />
                                <form class="form" action="#" method="post" id="simpansoalno3">
                <ol type="A">
                  <li>
                      <input type="radio" name="jawab" value="a" required="">&ensp;<label><p>Jawabannya A</p>
</label>
                                          </li>
                    <li>
                      <input type="radio" name="jawab" value="b" required="">&ensp;<label><p>Dalam sumber tata hukum di Indonesia Lorem ipsum dolor sit amet, consectetur adipiscing elit.</p>
</label>
                                          </li>
                    <li>
                      <input type="radio" name="jawab" value="c" required="">&ensp;<label><p>Dalam sumber tata hukum di Indonesia Lorem ipsum dolor sit amet, consectetur adipiscing elit.</p>
</label>
                                          </li>
                    <li>
                      <input type="radio" name="jawab" value="d" required="">&ensp;<label><p>Dalam sumber tata hukum di Indonesia Lorem ipsum dolor sit amet, consectetur adipiscing elit.</p>
</label>
                                          </li>
                    <li>
                      <input type="radio" name="jawab" value="e" required="">&ensp;<label><p>Dalam sumber tata hukum di Indonesia Lorem ipsum dolor sit amet, consectetur adipiscing elit.</p>
</label>
                                          </li>
                </ol>
                                <button type="button" class="btn btn-warning btn-flat btn-sm" onclick="kembali('2')">Kembali</button>
                                <button type="submit" class="btn btn-success btn-flat btn-sm" onclick="simpan_lanjutkan(3,1)">Simpan & lanjutkan</button>
                <button type="button" class="btn btn-success btn-flat btn-sm" onclick="lewati('4')">Lewati</button>
              </form>
              </p>
            </div>
                        <div class="tab-pane " id="soal4" role="tabpanel" aria-labelledby="soal4-tab">
              <label><b>Soal : TKP</b></label>
              <p>
                4. <p>ibukota jawa timur?</p><br />
                                <form class="form" action="#" method="post" id="simpansoalno4">
                <ol type="A">
                  <li>
                      <input type="radio" name="jawab" value="a" required="">&ensp;<label><p>surabaya</p>
</label>
                                          </li>
                    <li>
                      <input type="radio" name="jawab" value="b" required="">&ensp;<label><p>malang</p>
</label>
                                          </li>
                    <li>
                      <input type="radio" name="jawab" value="c" required="">&ensp;<label><p>madiun</p>
</label>
                                          </li>
                    <li>
                      <input type="radio" name="jawab" value="d" required="">&ensp;<label><p>magetan</p>
</label>
                                          </li>
                    <li>
                      <input type="radio" name="jawab" value="e" required="">&ensp;<label><p>mojokerto</p>
</label>
                                          </li>
                </ol>
                                <button type="button" class="btn btn-warning btn-flat btn-sm" onclick="kembali('3')">Kembali</button>
                                <button type="submit" class="btn btn-success btn-flat btn-sm" onclick="simpan_lanjutkan(4,56)">Simpan & lanjutkan</button>
                <button type="button" class="btn btn-success btn-flat btn-sm" onclick="lewati('5')">Lewati</button>
              </form>
              </p>
            </div>
                        <div class="tab-pane " id="soal5" role="tabpanel" aria-labelledby="soal5-tab">
              <label><b>Soal : TKP</b></label>
              <p>
                5. <p>Dalam sumber tata hukum di Indonesia Lorem ipsum dolor sit amet, consectetur adipiscing elit. DIPLOMA 3</p><br />
                                <form class="form" action="#" method="post" id="simpansoalno5">
                <ol type="A">
                  <li>
                      <input type="radio" name="jawab" value="a" required="">&ensp;<label><p>Dalam sumber tata hukum di Indonesia Lorem ipsum dolor sit amet, consectetur adipiscing elit.</p>
</label>
                                          </li>
                    <li>
                      <input type="radio" name="jawab" value="b" required="">&ensp;<label><p>Dalam sumber tata hukum di Indonesia Lorem ipsum dolor sit amet, consectetur adipiscing elit.</p>
</label>
                                          </li>
                    <li>
                      <input type="radio" name="jawab" value="c" required="">&ensp;<label><p>Dalam sumber tata hukum di Indonesia Lorem ipsum dolor sit amet, consectetur adipiscing elit.</p>
</label>
                                          </li>
                    <li>
                      <input type="radio" name="jawab" value="d" required="">&ensp;<label><p>Dalam sumber tata hukum di Indonesia Lorem ipsum dolor sit amet, consectetur adipiscing elit.</p>
</label>
                                          </li>
                    <li>
                      <input type="radio" name="jawab" value="e" required="">&ensp;<label><p>Dalam sumber tata hukum di Indonesia Lorem ipsum dolor sit amet, consectetur adipiscing elit.</p>
</label>
                                          </li>
                </ol>
                                <button type="button" class="btn btn-warning btn-flat btn-sm" onclick="kembali('4')">Kembali</button>
                                <button type="submit" class="btn btn-success btn-flat btn-sm" onclick="simpan_lanjutkan(5,32)">Simpan & lanjutkan</button>
                <button type="button" class="btn btn-success btn-flat btn-sm" onclick="lewati('6')">Lewati</button>
              </form>
              </p>
            </div>
                        <div class="tab-pane " id="soal6" role="tabpanel" aria-labelledby="soal6-tab">
              <label><b>Soal : TKP</b></label>
              <p>
                6. <p>nama planet?</p><br />
                                <form class="form" action="#" method="post" id="simpansoalno6">
                <ol type="A">
                  <li>
                      <input type="radio" name="jawab" value="a" required="">&ensp;<label><p>bumi</p>
</label>
                                          </li>
                    <li>
                      <input type="radio" name="jawab" value="b" required="">&ensp;<label><p>jupiter</p>
</label>
                                          </li>
                    <li>
                      <input type="radio" name="jawab" value="c" required="">&ensp;<label><p>mars</p>
</label>
                                          </li>
                    <li>
                      <input type="radio" name="jawab" value="d" required="">&ensp;<label><p>pluto</p>
</label>
                                          </li>
                    <li>
                      <input type="radio" name="jawab" value="e" required="">&ensp;<label><p>neptunus</p>
</label>
                                          </li>
                </ol>
                                <button type="button" class="btn btn-warning btn-flat btn-sm" onclick="kembali('5')">Kembali</button>
                                <button type="submit" class="btn btn-success btn-flat btn-sm" onclick="simpan_lanjutkan(6,58)">Simpan & lanjutkan</button>
                <button type="button" class="btn btn-success btn-flat btn-sm" onclick="lewati('7')">Lewati</button>
              </form>
              </p>
            </div>
                        <div class="tab-pane " id="soal7" role="tabpanel" aria-labelledby="soal7-tab">
              <label><b>Soal : TKP</b></label>
              <p>
                7. <p>Dalam sumber tata hukum di Indonesia Lorem ipsum dolor sit amet, consectetur adipiscing elit. DIPLOMA 3</p><br />
                                <form class="form" action="#" method="post" id="simpansoalno7">
                <ol type="A">
                  <li>
                      <input type="radio" name="jawab" value="a" required="">&ensp;<label><p>Jawabannya A</p>
</label>
                                          </li>
                    <li>
                      <input type="radio" name="jawab" value="b" required="">&ensp;<label><p>Dalam sumber tata hukum di Indonesia Lorem ipsum dolor sit amet, consectetur adipiscing elit.</p>
</label>
                                          </li>
                    <li>
                      <input type="radio" name="jawab" value="c" required="">&ensp;<label><p>Dalam sumber tata hukum di Indonesia Lorem ipsum dolor sit amet, consectetur adipiscing elit.</p>
</label>
                                          </li>
                    <li>
                      <input type="radio" name="jawab" value="d" required="">&ensp;<label><p>Dalam sumber tata hukum di Indonesia Lorem ipsum dolor sit amet, consectetur adipiscing elit.</p>
</label>
                                          </li>
                    <li>
                      <input type="radio" name="jawab" value="e" required="">&ensp;<label><p>Dalam sumber tata hukum di Indonesia Lorem ipsum dolor sit amet, consectetur adipiscing elit.</p>
</label>
                                          </li>
                </ol>
                                <button type="button" class="btn btn-warning btn-flat btn-sm" onclick="kembali('6')">Kembali</button>
                                <button type="submit" class="btn btn-success btn-flat btn-sm" onclick="simpan_lanjutkan(7,6)">Simpan & lanjutkan</button>
                <button type="button" class="btn btn-success btn-flat btn-sm" onclick="lewati('8')">Lewati</button>
              </form>
              </p>
            </div>
                        <div class="tab-pane " id="soal8" role="tabpanel" aria-labelledby="soal8-tab">
              <label><b>Soal : TKP</b></label>
              <p>
                8. <p>Soal ini jawabannya C</p><br />
                                <form class="form" action="#" method="post" id="simpansoalno8">
                <ol type="A">
                  <li>
                      <input type="radio" name="jawab" value="a" required="">&ensp;<label><p>Dalam sumber tata hukum di Indonesia Lorem ipsum dolor sit amet, consectetur adipiscing elit.</p>
</label>
                                          </li>
                    <li>
                      <input type="radio" name="jawab" value="b" required="">&ensp;<label><p>Dalam sumber tata hukum di Indonesia Lorem ipsum dolor sit amet, consectetur adipiscing elit.</p>
</label>
                                          </li>
                    <li>
                      <input type="radio" name="jawab" value="c" required="">&ensp;<label><p>Dalam sumber tata hukum di Indonesia Lorem ipsum dolor sit amet, consectetur adipiscing elit.</p>
</label>
                                          </li>
                    <li>
                      <input type="radio" name="jawab" value="d" required="">&ensp;<label><p>Dalam sumber tata hukum di Indonesia Lorem ipsum dolor sit amet, consectetur adipiscing elit.</p>
</label>
                                          </li>
                    <li>
                      <input type="radio" name="jawab" value="e" required="">&ensp;<label><p>Dalam sumber tata hukum di Indonesia Lorem ipsum dolor sit amet, consectetur adipiscing elit.</p>
</label>
                                          </li>
                </ol>
                                <button type="button" class="btn btn-warning btn-flat btn-sm" onclick="kembali('7')">Kembali</button>
                                <button type="submit" class="btn btn-success btn-flat btn-sm" onclick="simpan_lanjutkan(8,4)">Simpan & lanjutkan</button>
                <button type="button" class="btn btn-success btn-flat btn-sm" onclick="lewati('9')">Lewati</button>
              </form>
              </p>
            </div>
                        <div class="tab-pane " id="soal9" role="tabpanel" aria-labelledby="soal9-tab">
              <label><b>Soal : TKP</b></label>
              <p>
                9. <p>warna pelangi?</p><br />
                                <form class="form" action="#" method="post" id="simpansoalno9">
                <ol type="A">
                  <li>
                      <input type="radio" name="jawab" value="a" required="">&ensp;<label><p>merah</p>
</label>
                                          </li>
                    <li>
                      <input type="radio" name="jawab" value="b" required="">&ensp;<label><p>kuning</p>
</label>
                                          </li>
                    <li>
                      <input type="radio" name="jawab" value="c" required="">&ensp;<label><p>hijau</p>
</label>
                                          </li>
                    <li>
                      <input type="radio" name="jawab" value="d" required="">&ensp;<label><p>putih</p>
</label>
                                          </li>
                    <li>
                      <input type="radio" name="jawab" value="e" required="">&ensp;<label><p>hitam</p>
</label>
                                          </li>
                </ol>
                                <button type="button" class="btn btn-warning btn-flat btn-sm" onclick="kembali('8')">Kembali</button>
                                <button type="submit" class="btn btn-success btn-flat btn-sm" onclick="simpan_lanjutkan(9,57)">Simpan & lanjutkan</button>
                <button type="button" class="btn btn-success btn-flat btn-sm" onclick="lewati('10')">Lewati</button>
              </form>
              </p>
            </div>
                        <div class="tab-pane " id="soal10" role="tabpanel" aria-labelledby="soal10-tab">
              <label><b>Soal : TKP</b></label>
              <p>
                10. <p>Soal tkp untuk kebidanan</p><br />
                                <form class="form" action="#" method="post" id="simpansoalno10">
                <ol type="A">
                  <li>
                      <input type="radio" name="jawab" value="a" required="">&ensp;<label><p>Benar</p>
</label>
                                          </li>
                    <li>
                      <input type="radio" name="jawab" value="b" required="">&ensp;<label><p>Salah</p>
</label>
                                          </li>
                    <li>
                      <input type="radio" name="jawab" value="c" required="">&ensp;<label><p>Salah</p>
</label>
                                          </li>
                    <li>
                      <input type="radio" name="jawab" value="d" required="">&ensp;<label><p>Salah</p>
</label>
                                          </li>
                    <li>
                      <input type="radio" name="jawab" value="e" required="">&ensp;<label><p>Salah</p>
</label>
                                          </li>
                </ol>
                                <button type="button" class="btn btn-warning btn-flat btn-sm" onclick="kembali('9')">Kembali</button>
                                <button type="submit" class="btn btn-success btn-flat btn-sm" onclick="simpan_lanjutkan(10,55)">Simpan & lanjutkan</button>
                <button type="button" class="btn btn-success btn-flat btn-sm" onclick="lewati('11')">Lewati</button>
              </form>
              </p>
            </div>
                        <div class="tab-pane " id="soal11" role="tabpanel" aria-labelledby="soal11-tab">
              <label><b>Soal : TIU</b></label>
              <p>
                11. <p>Dalam sumber tata hukum di Indonesia Lorem ipsum dolor sit amet, consectetur adipiscing elit. DIPLOMA 3</p><br />
                                <form class="form" action="#" method="post" id="simpansoalno11">
                <ol type="A">
                  <li>
                      <input type="radio" name="jawab" value="a" required="">&ensp;<label><p>Dalam sumber tata hukum di Indonesia Lorem ipsum dolor sit amet, consectetur adipiscing elit.</p>
</label>
                                          </li>
                    <li>
                      <input type="radio" name="jawab" value="b" required="">&ensp;<label><p>Dalam sumber tata hukum di Indonesia Lorem ipsum dolor sit amet, consectetur adipiscing elit.</p>
</label>
                                          </li>
                    <li>
                      <input type="radio" name="jawab" value="c" required="">&ensp;<label><p>Dalam sumber tata hukum di Indonesia Lorem ipsum dolor sit amet, consectetur adipiscing elit.</p>
</label>
                                          </li>
                    <li>
                      <input type="radio" name="jawab" value="d" required="">&ensp;<label><p>Dalam sumber tata hukum di Indonesia Lorem ipsum dolor sit amet, consectetur adipiscing elit.</p>
</label>
                                          </li>
                    <li>
                      <input type="radio" name="jawab" value="e" required="">&ensp;<label><p>Dalam sumber tata hukum di Indonesia Lorem ipsum dolor sit amet, consectetur adipiscing elit.</p>
</label>
                                          </li>
                </ol>
                                <button type="button" class="btn btn-warning btn-flat btn-sm" onclick="kembali('10')">Kembali</button>
                                <button type="submit" class="btn btn-success btn-flat btn-sm" onclick="simpan_lanjutkan(11,48)">Simpan & lanjutkan</button>
                <button type="button" class="btn btn-success btn-flat btn-sm" onclick="lewati('12')">Lewati</button>
              </form>
              </p>
            </div>
                        <div class="tab-pane " id="soal12" role="tabpanel" aria-labelledby="soal12-tab">
              <label><b>Soal : TIU</b></label>
              <p>
                12. <p>Dalam sumber tata hukum di Indonesia Lorem ipsum dolor sit amet, consectetur adipiscing elit. DIPLOMA 3</p><br />
                                <form class="form" action="#" method="post" id="simpansoalno12">
                <ol type="A">
                  <li>
                      <input type="radio" name="jawab" value="a" required="">&ensp;<label><p>Dalam sumber tata hukum di Indonesia Lorem ipsum dolor sit amet, consectetur adipiscing elit.</p>
</label>
                                          </li>
                    <li>
                      <input type="radio" name="jawab" value="b" required="">&ensp;<label><p>Dalam sumber tata hukum di Indonesia Lorem ipsum dolor sit amet, consectetur adipiscing elit.</p>
</label>
                                          </li>
                    <li>
                      <input type="radio" name="jawab" value="c" required="">&ensp;<label><p>Dalam sumber tata hukum di Indonesia Lorem ipsum dolor sit amet, consectetur adipiscing elit.</p>
</label>
                                          </li>
                    <li>
                      <input type="radio" name="jawab" value="d" required="">&ensp;<label><p>Dalam sumber tata hukum di Indonesia Lorem ipsum dolor sit amet, consectetur adipiscing elit.</p>
</label>
                                          </li>
                    <li>
                      <input type="radio" name="jawab" value="e" required="">&ensp;<label><p>Dalam sumber tata hukum di Indonesia Lorem ipsum dolor sit amet, consectetur adipiscing elit.</p>
</label>
                                          </li>
                </ol>
                                <button type="button" class="btn btn-warning btn-flat btn-sm" onclick="kembali('11')">Kembali</button>
                                <button type="submit" class="btn btn-success btn-flat btn-sm" onclick="simpan_lanjutkan(12,26)">Simpan & lanjutkan</button>
                <button type="button" class="btn btn-success btn-flat btn-sm" onclick="lewati('13')">Lewati</button>
              </form>
              </p>
            </div>
                        <div class="tab-pane " id="soal13" role="tabpanel" aria-labelledby="soal13-tab">
              <label><b>Soal : TIU</b></label>
              <p>
                13. <p>Dalam sumber tata hukum di Indonesia Lorem ipsum dolor sit amet, consectetur adipiscing elit. DIPLOMA 3</p><br />
                                <form class="form" action="#" method="post" id="simpansoalno13">
                <ol type="A">
                  <li>
                      <input type="radio" name="jawab" value="a" required="">&ensp;<label><p>Dalam sumber tata hukum di Indonesia Lorem ipsum dolor sit amet, consectetur adipiscing elit.</p>
</label>
                                          </li>
                    <li>
                      <input type="radio" name="jawab" value="b" required="">&ensp;<label><p>Dalam sumber tata hukum di Indonesia Lorem ipsum dolor sit amet, consectetur adipiscing elit.</p>
</label>
                                          </li>
                    <li>
                      <input type="radio" name="jawab" value="c" required="">&ensp;<label><p>Dalam sumber tata hukum di Indonesia Lorem ipsum dolor sit amet, consectetur adipiscing elit.</p>
</label>
                                          </li>
                    <li>
                      <input type="radio" name="jawab" value="d" required="">&ensp;<label><p>Dalam sumber tata hukum di Indonesia Lorem ipsum dolor sit amet, consectetur adipiscing elit.</p>
</label>
                                          </li>
                    <li>
                      <input type="radio" name="jawab" value="e" required="">&ensp;<label><p>Dalam sumber tata hukum di Indonesia Lorem ipsum dolor sit amet, consectetur adipiscing elit.</p>
</label>
                                          </li>
                </ol>
                                <button type="button" class="btn btn-warning btn-flat btn-sm" onclick="kembali('12')">Kembali</button>
                                <button type="submit" class="btn btn-success btn-flat btn-sm" onclick="simpan_lanjutkan(13,20)">Simpan & lanjutkan</button>
                <button type="button" class="btn btn-success btn-flat btn-sm" onclick="lewati('14')">Lewati</button>
              </form>
              </p>
            </div>
                        <div class="tab-pane " id="soal14" role="tabpanel" aria-labelledby="soal14-tab">
              <label><b>Soal : TIU</b></label>
              <p>
                14. <p>Dalam sumber tata hukum di Indonesia Lorem ipsum dolor sit amet, consectetur adipiscing elit. DIPLOMA 3</p><br />
                                <form class="form" action="#" method="post" id="simpansoalno14">
                <ol type="A">
                  <li>
                      <input type="radio" name="jawab" value="a" required="">&ensp;<label><p>Dalam sumber tata hukum di Indonesia Lorem ipsum dolor sit amet, consectetur adipiscing elit.</p>
</label>
                                          </li>
                    <li>
                      <input type="radio" name="jawab" value="b" required="">&ensp;<label><p>Dalam sumber tata hukum di Indonesia Lorem ipsum dolor sit amet, consectetur adipiscing elit.</p>
</label>
                                          </li>
                    <li>
                      <input type="radio" name="jawab" value="c" required="">&ensp;<label><p>Dalam sumber tata hukum di Indonesia Lorem ipsum dolor sit amet, consectetur adipiscing elit.</p>
</label>
                                          </li>
                    <li>
                      <input type="radio" name="jawab" value="d" required="">&ensp;<label><p>Dalam sumber tata hukum di Indonesia Lorem ipsum dolor sit amet, consectetur adipiscing elit.</p>
</label>
                                          </li>
                    <li>
                      <input type="radio" name="jawab" value="e" required="">&ensp;<label><p>Dalam sumber tata hukum di Indonesia Lorem ipsum dolor sit amet, consectetur adipiscing elit.</p>
</label>
                                          </li>
                </ol>
                                <button type="button" class="btn btn-warning btn-flat btn-sm" onclick="kembali('13')">Kembali</button>
                                <button type="submit" class="btn btn-success btn-flat btn-sm" onclick="simpan_lanjutkan(14,22)">Simpan & lanjutkan</button>
                <button type="button" class="btn btn-success btn-flat btn-sm" onclick="lewati('15')">Lewati</button>
              </form>
              </p>
            </div>
                        <div class="tab-pane " id="soal15" role="tabpanel" aria-labelledby="soal15-tab">
              <label><b>Soal : TIU</b></label>
              <p>
                15. <p>Dalam sumber tata hukum di Indonesia Lorem ipsum dolor sit amet, consectetur adipiscing elit. STRATA 1</p><br />
                                <form class="form" action="#" method="post" id="simpansoalno15">
                <ol type="A">
                  <li>
                      <input type="radio" name="jawab" value="a" required="">&ensp;<label><p>Dalam sumber tata hukum di Indonesia Lorem ipsum dolor sit amet, consectetur adipiscing elit.</p>
</label>
                                          </li>
                    <li>
                      <input type="radio" name="jawab" value="b" required="">&ensp;<label><p>Dalam sumber tata hukum di Indonesia Lorem ipsum dolor sit amet, consectetur adipiscing elit.</p>
</label>
                                          </li>
                    <li>
                      <input type="radio" name="jawab" value="c" required="">&ensp;<label><p>Dalam sumber tata hukum di Indonesia Lorem ipsum dolor sit amet, consectetur adipiscing elit.</p>
</label>
                                          </li>
                    <li>
                      <input type="radio" name="jawab" value="d" required="">&ensp;<label><p>Dalam sumber tata hukum di Indonesia Lorem ipsum dolor sit amet, consectetur adipiscing elit.</p>
</label>
                                          </li>
                    <li>
                      <input type="radio" name="jawab" value="e" required="">&ensp;<label><p>Dalam sumber tata hukum di Indonesia Lorem ipsum dolor sit amet, consectetur adipiscing elit.</p>
</label>
                                          </li>
                </ol>
                                <button type="button" class="btn btn-warning btn-flat btn-sm" onclick="kembali('14')">Kembali</button>
                                <button type="submit" class="btn btn-success btn-flat btn-sm" onclick="simpan_lanjutkan(15,51)">Simpan & lanjutkan</button>
                <button type="button" class="btn btn-success btn-flat btn-sm" onclick="lewati('16')">Lewati</button>
              </form>
              </p>
            </div>
                        <div class="tab-pane " id="soal16" role="tabpanel" aria-labelledby="soal16-tab">
              <label><b>Soal : TIU</b></label>
              <p>
                16. <p>Dalam sumber tata hukum di Indonesia Lorem ipsum dolor sit amet, consectetur adipiscing elit. DIPLOMA 3</p><br />
                                <form class="form" action="#" method="post" id="simpansoalno16">
                <ol type="A">
                  <li>
                      <input type="radio" name="jawab" value="a" required="">&ensp;<label><p>Dalam sumber tata hukum di Indonesia Lorem ipsum dolor sit amet, consectetur adipiscing elit.</p>
</label>
                                          </li>
                    <li>
                      <input type="radio" name="jawab" value="b" required="">&ensp;<label><p>Dalam sumber tata hukum di Indonesia Lorem ipsum dolor sit amet, consectetur adipiscing elit.</p>
</label>
                                          </li>
                    <li>
                      <input type="radio" name="jawab" value="c" required="">&ensp;<label><p>Dalam sumber tata hukum di Indonesia Lorem ipsum dolor sit amet, consectetur adipiscing elit.</p>
</label>
                                          </li>
                    <li>
                      <input type="radio" name="jawab" value="d" required="">&ensp;<label><p>Dalam sumber tata hukum di Indonesia Lorem ipsum dolor sit amet, consectetur adipiscing elit.</p>
</label>
                                          </li>
                    <li>
                      <input type="radio" name="jawab" value="e" required="">&ensp;<label><p>Dalam sumber tata hukum di Indonesia Lorem ipsum dolor sit amet, consectetur adipiscing elit.</p>
</label>
                                          </li>
                </ol>
                                <button type="button" class="btn btn-warning btn-flat btn-sm" onclick="kembali('15')">Kembali</button>
                                <button type="submit" class="btn btn-success btn-flat btn-sm" onclick="simpan_lanjutkan(16,50)">Simpan & lanjutkan</button>
                <button type="button" class="btn btn-success btn-flat btn-sm" onclick="lewati('17')">Lewati</button>
              </form>
              </p>
            </div>
                        <div class="tab-pane " id="soal17" role="tabpanel" aria-labelledby="soal17-tab">
              <label><b>Soal : TIU</b></label>
              <p>
                17. <p>Dalam sumber tata hukum di Indonesia Lorem ipsum dolor sit amet, consectetur adipiscing elit. DIPLOMA 3</p><br />
                                <form class="form" action="#" method="post" id="simpansoalno17">
                <ol type="A">
                  <li>
                      <input type="radio" name="jawab" value="a" required="">&ensp;<label><p>Dalam sumber tata hukum di Indonesia Lorem ipsum dolor sit amet, consectetur adipiscing elit.</p>
</label>
                                          </li>
                    <li>
                      <input type="radio" name="jawab" value="b" required="">&ensp;<label><p>Dalam sumber tata hukum di Indonesia Lorem ipsum dolor sit amet, consectetur adipiscing elit.</p>
</label>
                                          </li>
                    <li>
                      <input type="radio" name="jawab" value="c" required="">&ensp;<label><p>Dalam sumber tata hukum di Indonesia Lorem ipsum dolor sit amet, consectetur adipiscing elit.</p>
</label>
                                          </li>
                    <li>
                      <input type="radio" name="jawab" value="d" required="">&ensp;<label><p>Dalam sumber tata hukum di Indonesia Lorem ipsum dolor sit amet, consectetur adipiscing elit.</p>
</label>
                                          </li>
                    <li>
                      <input type="radio" name="jawab" value="e" required="">&ensp;<label><p>Dalam sumber tata hukum di Indonesia Lorem ipsum dolor sit amet, consectetur adipiscing elit.</p>
</label>
                                          </li>
                </ol>
                                <button type="button" class="btn btn-warning btn-flat btn-sm" onclick="kembali('16')">Kembali</button>
                                <button type="submit" class="btn btn-success btn-flat btn-sm" onclick="simpan_lanjutkan(17,52)">Simpan & lanjutkan</button>
                <button type="button" class="btn btn-success btn-flat btn-sm" onclick="lewati('18')">Lewati</button>
              </form>
              </p>
            </div>
                        <div class="tab-pane " id="soal18" role="tabpanel" aria-labelledby="soal18-tab">
              <label><b>Soal : TIU</b></label>
              <p>
                18. <p>Dalam sumber tata hukum di Indonesia Lorem ipsum dolor sit amet, consectetur adipiscing elit. DIPLOMA 3</p><br />
                                <form class="form" action="#" method="post" id="simpansoalno18">
                <ol type="A">
                  <li>
                      <input type="radio" name="jawab" value="a" required="">&ensp;<label><p>Dalam sumber tata hukum di Indonesia Lorem ipsum dolor sit amet, consectetur adipiscing elit.</p>
</label>
                                          </li>
                    <li>
                      <input type="radio" name="jawab" value="b" required="">&ensp;<label><p>Dalam sumber tata hukum di Indonesia Lorem ipsum dolor sit amet, consectetur adipiscing elit.</p>
</label>
                                          </li>
                    <li>
                      <input type="radio" name="jawab" value="c" required="">&ensp;<label><p>Dalam sumber tata hukum di Indonesia Lorem ipsum dolor sit amet, consectetur adipiscing elit.</p>
</label>
                                          </li>
                    <li>
                      <input type="radio" name="jawab" value="d" required="">&ensp;<label><p>Dalam sumber tata hukum di Indonesia Lorem ipsum dolor sit amet, consectetur adipiscing elit.</p>
</label>
                                          </li>
                    <li>
                      <input type="radio" name="jawab" value="e" required="">&ensp;<label><p>Dalam sumber tata hukum di Indonesia Lorem ipsum dolor sit amet, consectetur adipiscing elit.</p>
</label>
                                          </li>
                </ol>
                                <button type="button" class="btn btn-warning btn-flat btn-sm" onclick="kembali('17')">Kembali</button>
                                <button type="submit" class="btn btn-success btn-flat btn-sm" onclick="simpan_lanjutkan(18,24)">Simpan & lanjutkan</button>
                <button type="button" class="btn btn-success btn-flat btn-sm" onclick="lewati('19')">Lewati</button>
              </form>
              </p>
            </div>
                        <div class="tab-pane " id="soal19" role="tabpanel" aria-labelledby="soal19-tab">
              <label><b>Soal : TIU</b></label>
              <p>
                19. <p>Dalam sumber tata hukum di Indonesia Lorem ipsum dolor sit amet, consectetur adipiscing elit. DIPLOMA 3</p><br />
                                <form class="form" action="#" method="post" id="simpansoalno19">
                <ol type="A">
                  <li>
                      <input type="radio" name="jawab" value="a" required="">&ensp;<label><p>Dalam sumber tata hukum di Indonesia Lorem ipsum dolor sit amet, consectetur adipiscing elit.</p>
</label>
                                          </li>
                    <li>
                      <input type="radio" name="jawab" value="b" required="">&ensp;<label><p>Dalam sumber tata hukum di Indonesia Lorem ipsum dolor sit amet, consectetur adipiscing elit.</p>
</label>
                                          </li>
                    <li>
                      <input type="radio" name="jawab" value="c" required="">&ensp;<label><p>Dalam sumber tata hukum di Indonesia Lorem ipsum dolor sit amet, consectetur adipiscing elit.</p>
</label>
                                          </li>
                    <li>
                      <input type="radio" name="jawab" value="d" required="">&ensp;<label><p>Dalam sumber tata hukum di Indonesia Lorem ipsum dolor sit amet, consectetur adipiscing elit.</p>
</label>
                                          </li>
                    <li>
                      <input type="radio" name="jawab" value="e" required="">&ensp;<label><p>Dalam sumber tata hukum di Indonesia Lorem ipsum dolor sit amet, consectetur adipiscing elit.</p>
</label>
                                          </li>
                </ol>
                                <button type="button" class="btn btn-warning btn-flat btn-sm" onclick="kembali('18')">Kembali</button>
                                <button type="submit" class="btn btn-success btn-flat btn-sm" onclick="simpan_lanjutkan(19,18)">Simpan & lanjutkan</button>
                <button type="button" class="btn btn-success btn-flat btn-sm" onclick="lewati('20')">Lewati</button>
              </form>
              </p>
            </div>
                        <div class="tab-pane " id="soal20" role="tabpanel" aria-labelledby="soal20-tab">
              <label><b>Soal : TIU</b></label>
              <p>
                20. <p>Dalam sumber tata hukum di Indonesia Lorem ipsum dolor sit amet, consectetur adipiscing elit. DIPLOMA 3</p><br />
                                <form class="form" action="#" method="post" id="simpansoalno20">
                <ol type="A">
                  <li>
                      <input type="radio" name="jawab" value="a" required="">&ensp;<label><p>Dalam sumber tata hukum di Indonesia Lorem ipsum dolor sit amet, consectetur adipiscing elit.</p>
</label>
                                          </li>
                    <li>
                      <input type="radio" name="jawab" value="b" required="">&ensp;<label><p>Dalam sumber tata hukum di Indonesia Lorem ipsum dolor sit amet, consectetur adipiscing elit.</p>
</label>
                                          </li>
                    <li>
                      <input type="radio" name="jawab" value="c" required="">&ensp;<label><p>Dalam sumber tata hukum di Indonesia Lorem ipsum dolor sit amet, consectetur adipiscing elit.</p>
</label>
                                          </li>
                    <li>
                      <input type="radio" name="jawab" value="d" required="">&ensp;<label><p>Dalam sumber tata hukum di Indonesia Lorem ipsum dolor sit amet, consectetur adipiscing elit.</p>
</label>
                                          </li>
                    <li>
                      <input type="radio" name="jawab" value="e" required="">&ensp;<label><p>Dalam sumber tata hukum di Indonesia Lorem ipsum dolor sit amet, consectetur adipiscing elit.</p>
</label>
                                          </li>
                </ol>
                                <button type="button" class="btn btn-warning btn-flat btn-sm" onclick="kembali('19')">Kembali</button>
                                <button type="submit" class="btn btn-success btn-flat btn-sm" onclick="simpan_lanjutkan(20,46)">Simpan & lanjutkan</button>
                <button type="button" class="btn btn-success btn-flat btn-sm" onclick="lewati('21')">Lewati</button>
              </form>
              </p>
            </div>
                        <div class="tab-pane " id="soal21" role="tabpanel" aria-labelledby="soal21-tab">
              <label><b>Soal : TWK</b></label>
              <p>
                21. <p>Dalam sumber tata hukum di Indonesia Lorem ipsum dolor sit amet, consectetur adipiscing elit. DIPLOMA 3</p><br />
                                <form class="form" action="#" method="post" id="simpansoalno21">
                <ol type="A">
                  <li>
                      <input type="radio" name="jawab" value="a" required="">&ensp;<label><p>Dalam sumber tata hukum di Indonesia Lorem ipsum dolor sit amet, consectetur adipiscing elit.</p>
</label>
                                          </li>
                    <li>
                      <input type="radio" name="jawab" value="b" required="">&ensp;<label><p>Dalam sumber tata hukum di Indonesia Lorem ipsum dolor sit amet, consectetur adipiscing elit.</p>
</label>
                                          </li>
                    <li>
                      <input type="radio" name="jawab" value="c" required="">&ensp;<label><p>Dalam sumber tata hukum di Indonesia Lorem ipsum dolor sit amet, consectetur adipiscing elit.</p>
</label>
                                          </li>
                    <li>
                      <input type="radio" name="jawab" value="d" required="">&ensp;<label><p>Dalam sumber tata hukum di Indonesia Lorem ipsum dolor sit amet, consectetur adipiscing elit.</p>
</label>
                                          </li>
                    <li>
                      <input type="radio" name="jawab" value="e" required="">&ensp;<label><p>Dalam sumber tata hukum di Indonesia Lorem ipsum dolor sit amet, consectetur adipiscing elit.</p>
</label>
                                          </li>
                </ol>
                                <button type="button" class="btn btn-warning btn-flat btn-sm" onclick="kembali('20')">Kembali</button>
                                <button type="submit" class="btn btn-success btn-flat btn-sm" onclick="simpan_lanjutkan(21,36)">Simpan & lanjutkan</button>
                <button type="button" class="btn btn-success btn-flat btn-sm" onclick="lewati('22')">Lewati</button>
              </form>
              </p>
            </div>
                        <div class="tab-pane " id="soal22" role="tabpanel" aria-labelledby="soal22-tab">
              <label><b>Soal : TWK</b></label>
              <p>
                22. <p>ini soal twk jawabannya A</p><br />
                                <form class="form" action="#" method="post" id="simpansoalno22">
                <ol type="A">
                  <li>
                      <input type="radio" name="jawab" value="a" required="">&ensp;<label><p>Dalam sumber tata hukum di Indonesia Lorem ipsum dolor sit amet, consectetur adipiscing elit.</p>
</label>
                                          </li>
                    <li>
                      <input type="radio" name="jawab" value="b" required="">&ensp;<label><p>Dalam sumber tata hukum di Indonesia Lorem ipsum dolor sit amet, consectetur adipiscing elit.</p>
</label>
                                          </li>
                    <li>
                      <input type="radio" name="jawab" value="c" required="">&ensp;<label><p>Dalam sumber tata hukum di Indonesia Lorem ipsum dolor sit amet, consectetur adipiscing elit.</p>
</label>
                                          </li>
                    <li>
                      <input type="radio" name="jawab" value="d" required="">&ensp;<label><p>Dalam sumber tata hukum di Indonesia Lorem ipsum dolor sit amet, consectetur adipiscing elit.</p>
</label>
                                          </li>
                    <li>
                      <input type="radio" name="jawab" value="e" required="">&ensp;<label><p>Dalam sumber tata hukum di Indonesia Lorem ipsum dolor sit amet, consectetur adipiscing elit.</p>
</label>
                                          </li>
                </ol>
                                <button type="button" class="btn btn-warning btn-flat btn-sm" onclick="kembali('21')">Kembali</button>
                                <button type="submit" class="btn btn-success btn-flat btn-sm" onclick="simpan_lanjutkan(22,7)">Simpan & lanjutkan</button>
                <button type="button" class="btn btn-success btn-flat btn-sm" onclick="lewati('23')">Lewati</button>
              </form>
              </p>
            </div>
                        <div class="tab-pane " id="soal23" role="tabpanel" aria-labelledby="soal23-tab">
              <label><b>Soal : TWK</b></label>
              <p>
                23. <p>Sila pertama</p><br />
                                <form class="form" action="#" method="post" id="simpansoalno23">
                <ol type="A">
                  <li>
                      <input type="radio" name="jawab" value="a" required="">&ensp;<label><p>Ketuhanan yg maha esa</p>
</label>
                                          </li>
                    <li>
                      <input type="radio" name="jawab" value="b" required="">&ensp;<label><p>asdfsfas</p>
</label>
                                          </li>
                    <li>
                      <input type="radio" name="jawab" value="c" required="">&ensp;<label><p>sfasfsaf</p>
</label>
                                          </li>
                    <li>
                      <input type="radio" name="jawab" value="d" required="">&ensp;<label><p>fsafsaf</p>
</label>
                                          </li>
                    <li>
                      <input type="radio" name="jawab" value="e" required="">&ensp;<label><p>safsafs</p>
</label>
                                          </li>
                </ol>
                                <button type="button" class="btn btn-warning btn-flat btn-sm" onclick="kembali('22')">Kembali</button>
                                <button type="submit" class="btn btn-success btn-flat btn-sm" onclick="simpan_lanjutkan(23,66)">Simpan & lanjutkan</button>
                <button type="button" class="btn btn-success btn-flat btn-sm" onclick="lewati('24')">Lewati</button>
              </form>
              </p>
            </div>
                        <div class="tab-pane " id="soal24" role="tabpanel" aria-labelledby="soal24-tab">
              <label><b>Soal : TWK</b></label>
              <p>
                24. <p>Dalam sumber tata hukum di Indonesia Lorem ipsum dolor sit amet, consectetur adipiscing elit. DIPLOMA 3</p><br />
                                <form class="form" action="#" method="post" id="simpansoalno24">
                <ol type="A">
                  <li>
                      <input type="radio" name="jawab" value="a" required="">&ensp;<label><p>Dalam sumber tata hukum di Indonesia Lorem ipsum dolor sit amet, consectetur adipiscing elit.</p>
</label>
                                          </li>
                    <li>
                      <input type="radio" name="jawab" value="b" required="">&ensp;<label><p>Dalam sumber tata hukum di Indonesia Lorem ipsum dolor sit amet, consectetur adipiscing elit.</p>
</label>
                                          </li>
                    <li>
                      <input type="radio" name="jawab" value="c" required="">&ensp;<label><p>Dalam sumber tata hukum di Indonesia Lorem ipsum dolor sit amet, consectetur adipiscing elit.</p>
</label>
                                          </li>
                    <li>
                      <input type="radio" name="jawab" value="d" required="">&ensp;<label><p>Dalam sumber tata hukum di Indonesia Lorem ipsum dolor sit amet, consectetur adipiscing elit.</p>
</label>
                                          </li>
                    <li>
                      <input type="radio" name="jawab" value="e" required="">&ensp;<label><p>Dalam sumber tata hukum di Indonesia Lorem ipsum dolor sit amet, consectetur adipiscing elit.</p>
</label>
                                          </li>
                </ol>
                                <button type="button" class="btn btn-warning btn-flat btn-sm" onclick="kembali('23')">Kembali</button>
                                <button type="submit" class="btn btn-success btn-flat btn-sm" onclick="simpan_lanjutkan(24,12)">Simpan & lanjutkan</button>
                <button type="button" class="btn btn-success btn-flat btn-sm" onclick="lewati('25')">Lewati</button>
              </form>
              </p>
            </div>
                        <div class="tab-pane " id="soal25" role="tabpanel" aria-labelledby="soal25-tab">
              <label><b>Soal : TWK</b></label>
              <p>
                25. <p>Dalam sumber tata hukum di Indonesia Lorem ipsum dolor sit amet, consectetur adipiscing elit. DIPLOMA 3</p><br />
                                <form class="form" action="#" method="post" id="simpansoalno25">
                <ol type="A">
                  <li>
                      <input type="radio" name="jawab" value="a" required="">&ensp;<label><p>Dalam sumber tata hukum di Indonesia Lorem ipsum dolor sit amet, consectetur adipiscing elit.</p>
</label>
                                          </li>
                    <li>
                      <input type="radio" name="jawab" value="b" required="">&ensp;<label><p>Dalam sumber tata hukum di Indonesia Lorem ipsum dolor sit amet, consectetur adipiscing elit.</p>
</label>
                                          </li>
                    <li>
                      <input type="radio" name="jawab" value="c" required="">&ensp;<label><p>Dalam sumber tata hukum di Indonesia Lorem ipsum dolor sit amet, consectetur adipiscing elit.</p>
</label>
                                          </li>
                    <li>
                      <input type="radio" name="jawab" value="d" required="">&ensp;<label><p>Dalam sumber tata hukum di Indonesia Lorem ipsum dolor sit amet, consectetur adipiscing elit.</p>
</label>
                                          </li>
                    <li>
                      <input type="radio" name="jawab" value="e" required="">&ensp;<label><p>Dalam sumber tata hukum di Indonesia Lorem ipsum dolor sit amet, consectetur adipiscing elit.</p>
</label>
                                          </li>
                </ol>
                                <button type="button" class="btn btn-warning btn-flat btn-sm" onclick="kembali('24')">Kembali</button>
                                <button type="submit" class="btn btn-success btn-flat btn-sm" onclick="simpan_lanjutkan(25,34)">Simpan & lanjutkan</button>
                <button type="button" class="btn btn-success btn-flat btn-sm" onclick="lewati('26')">Lewati</button>
              </form>
              </p>
            </div>
                        <div class="tab-pane " id="soal26" role="tabpanel" aria-labelledby="soal26-tab">
              <label><b>Soal : TWK</b></label>
              <p>
                26. <p>Dalam sumber tata hukum di Indonesia Lorem ipsum dolor sit amet, consectetur adipiscing elit. DIPLOMA 3</p><br />
                                <form class="form" action="#" method="post" id="simpansoalno26">
                <ol type="A">
                  <li>
                      <input type="radio" name="jawab" value="a" required="">&ensp;<label><p>Dalam sumber tata hukum di Indonesia Lorem ipsum dolor sit amet, consectetur adipiscing elit.</p>
</label>
                                          </li>
                    <li>
                      <input type="radio" name="jawab" value="b" required="">&ensp;<label><p>Dalam sumber tata hukum di Indonesia Lorem ipsum dolor sit amet, consectetur adipiscing elit.</p>
</label>
                                          </li>
                    <li>
                      <input type="radio" name="jawab" value="c" required="">&ensp;<label><p>Dalam sumber tata hukum di Indonesia Lorem ipsum dolor sit amet, consectetur adipiscing elit.</p>
</label>
                                          </li>
                    <li>
                      <input type="radio" name="jawab" value="d" required="">&ensp;<label><p>Dalam sumber tata hukum di Indonesia Lorem ipsum dolor sit amet, consectetur adipiscing elit.</p>
</label>
                                          </li>
                    <li>
                      <input type="radio" name="jawab" value="e" required="">&ensp;<label><p>Dalam sumber tata hukum di Indonesia Lorem ipsum dolor sit amet, consectetur adipiscing elit.</p>
</label>
                                          </li>
                </ol>
                                <button type="button" class="btn btn-warning btn-flat btn-sm" onclick="kembali('25')">Kembali</button>
                                <button type="submit" class="btn btn-success btn-flat btn-sm" onclick="simpan_lanjutkan(26,14)">Simpan & lanjutkan</button>
                <button type="button" class="btn btn-success btn-flat btn-sm" onclick="lewati('27')">Lewati</button>
              </form>
              </p>
            </div>
                        <div class="tab-pane " id="soal27" role="tabpanel" aria-labelledby="soal27-tab">
              <label><b>Soal : TWK</b></label>
              <p>
                27. <p>Dalam sumber tata hukum di Indonesia Lorem ipsum dolor sit amet, consectetur adipiscing elit. DIPLOMA 3</p><br />
                                <form class="form" action="#" method="post" id="simpansoalno27">
                <ol type="A">
                  <li>
                      <input type="radio" name="jawab" value="a" required="">&ensp;<label><p>Dalam sumber tata hukum di Indonesia Lorem ipsum dolor sit amet, consectetur adipiscing elit.</p>
</label>
                                          </li>
                    <li>
                      <input type="radio" name="jawab" value="b" required="">&ensp;<label><p>Dalam sumber tata hukum di Indonesia Lorem ipsum dolor sit amet, consectetur adipiscing elit.</p>
</label>
                                          </li>
                    <li>
                      <input type="radio" name="jawab" value="c" required="">&ensp;<label><p>Dalam sumber tata hukum di Indonesia Lorem ipsum dolor sit amet, consectetur adipiscing elit.</p>
</label>
                                          </li>
                    <li>
                      <input type="radio" name="jawab" value="d" required="">&ensp;<label><p>Dalam sumber tata hukum di Indonesia Lorem ipsum dolor sit amet, consectetur adipiscing elit.</p>
</label>
                                          </li>
                    <li>
                      <input type="radio" name="jawab" value="e" required="">&ensp;<label><p>Dalam sumber tata hukum di Indonesia Lorem ipsum dolor sit amet, consectetur adipiscing elit.</p>
</label>
                                          </li>
                </ol>
                                <button type="button" class="btn btn-warning btn-flat btn-sm" onclick="kembali('26')">Kembali</button>
                                <button type="submit" class="btn btn-success btn-flat btn-sm" onclick="simpan_lanjutkan(27,38)">Simpan & lanjutkan</button>
                <button type="button" class="btn btn-success btn-flat btn-sm" onclick="lewati('28')">Lewati</button>
              </form>
              </p>
            </div>
                        <div class="tab-pane " id="soal28" role="tabpanel" aria-labelledby="soal28-tab">
              <label><b>Soal : TWK</b></label>
              <p>
                28. <p>Dalam sumber tata hukum di Indonesia Lorem ipsum dolor sit amet, consectetur adipiscing elit. DIPLOMA 3</p><br />
                                <form class="form" action="#" method="post" id="simpansoalno28">
                <ol type="A">
                  <li>
                      <input type="radio" name="jawab" value="a" required="">&ensp;<label><p>Dalam sumber tata hukum di Indonesia Lorem ipsum dolor sit amet, consectetur adipiscing elit.</p>
</label>
                                          </li>
                    <li>
                      <input type="radio" name="jawab" value="b" required="">&ensp;<label><p>Dalam sumber tata hukum di Indonesia Lorem ipsum dolor sit amet, consectetur adipiscing elit.</p>
</label>
                                          </li>
                    <li>
                      <input type="radio" name="jawab" value="c" required="">&ensp;<label><p>Dalam sumber tata hukum di Indonesia Lorem ipsum dolor sit amet, consectetur adipiscing elit.</p>
</label>
                                          </li>
                    <li>
                      <input type="radio" name="jawab" value="d" required="">&ensp;<label><p>Dalam sumber tata hukum di Indonesia Lorem ipsum dolor sit amet, consectetur adipiscing elit.</p>
</label>
                                          </li>
                    <li>
                      <input type="radio" name="jawab" value="e" required="">&ensp;<label><p>Dalam sumber tata hukum di Indonesia Lorem ipsum dolor sit amet, consectetur adipiscing elit.</p>
</label>
                                          </li>
                </ol>
                                <button type="button" class="btn btn-warning btn-flat btn-sm" onclick="kembali('27')">Kembali</button>
                                <button type="submit" class="btn btn-success btn-flat btn-sm" onclick="simpan_lanjutkan(28,8)">Simpan & lanjutkan</button>
                <button type="button" class="btn btn-success btn-flat btn-sm" onclick="lewati('29')">Lewati</button>
              </form>
              </p>
            </div>
                        <div class="tab-pane " id="soal29" role="tabpanel" aria-labelledby="soal29-tab">
              <label><b>Soal : TWK</b></label>
              <p>
                29. <p>nama presiden</p><br />
                                <form class="form" action="#" method="post" id="simpansoalno29">
                <ol type="A">
                  <li>
                      <input type="radio" name="jawab" value="a" required="">&ensp;<label><p>soekarno</p>
</label>
                                          </li>
                    <li>
                      <input type="radio" name="jawab" value="b" required="">&ensp;<label><p>jokowi</p>
</label>
                                          </li>
                    <li>
                      <input type="radio" name="jawab" value="c" required="">&ensp;<label><p>sby</p>
</label>
                                          </li>
                    <li>
                      <input type="radio" name="jawab" value="d" required="">&ensp;<label><p>gusdur</p>
</label>
                                          </li>
                    <li>
                      <input type="radio" name="jawab" value="e" required="">&ensp;<label><p>megawati</p>
</label>
                                          </li>
                </ol>
                                <button type="button" class="btn btn-warning btn-flat btn-sm" onclick="kembali('28')">Kembali</button>
                                <button type="submit" class="btn btn-success btn-flat btn-sm" onclick="simpan_lanjutkan(29,64)">Simpan & lanjutkan</button>
                <button type="button" class="btn btn-success btn-flat btn-sm" onclick="lewati('30')">Lewati</button>
              </form>
              </p>
            </div>
                        <div class="tab-pane " id="soal30" role="tabpanel" aria-labelledby="soal30-tab">
              <label><b>Soal : TWK</b></label>
              <p>
                30. <p>hari kemerdekaan?</p><br />
                                <form class="form" action="#" method="post" id="simpansoalno30">
                <ol type="A">
                  <li>
                      <input type="radio" name="jawab" value="a" required="">&ensp;<label><p>sfsfasfsfa</p>
</label>
                                          </li>
                    <li>
                      <input type="radio" name="jawab" value="b" required="">&ensp;<label><p>17 agustus</p>
</label>
                                          </li>
                    <li>
                      <input type="radio" name="jawab" value="c" required="">&ensp;<label><p>dgfdgd</p>
</label>
                                          </li>
                    <li>
                      <input type="radio" name="jawab" value="d" required="">&ensp;<label><p>dgdg</p>
</label>
                                          </li>
                    <li>
                      <input type="radio" name="jawab" value="e" required="">&ensp;<label><p>dgdg</p>
</label>
                                          </li>
                </ol>
                                <button type="button" class="btn btn-warning btn-flat btn-sm" onclick="kembali('29')">Kembali</button>
                                <button type="submit" class="btn btn-success btn-flat btn-sm" onclick="simpan_lanjutkan(30,67)">Simpan & lanjutkan</button>
                <button type="button" class="btn btn-success btn-flat btn-sm" onclick="lewati('31')">Lewati</button>
              </form>
              </p>
            </div>
                      </div>
        </div>
        <div class="col-lg-3 col-sm-12" id="t4_nomor">
          <button class="btn btn-sm btn-danger" onclick="openSoal(1)" style="width: 35px;margin-bottom:2px;" data="jawabid30" id="no1">1</button>
              <button class="btn btn-sm btn-danger" onclick="openSoal(2)" style="width: 35px;margin-bottom:2px;" data="jawabid28" id="no2">2</button>
              <button class="btn btn-sm btn-danger" onclick="openSoal(3)" style="width: 35px;margin-bottom:2px;" data="jawabid1" id="no3">3</button>
              <button class="btn btn-sm btn-danger" onclick="openSoal(4)" style="width: 35px;margin-bottom:2px;" data="jawabid56" id="no4">4</button>
              <button class="btn btn-sm btn-danger" onclick="openSoal(5)" style="width: 35px;margin-bottom:2px;" data="jawabid32" id="no5">5</button>
              <button class="btn btn-sm btn-danger" onclick="openSoal(6)" style="width: 35px;margin-bottom:2px;" data="jawabid58" id="no6">6</button>
              <button class="btn btn-sm btn-danger" onclick="openSoal(7)" style="width: 35px;margin-bottom:2px;" data="jawabid6" id="no7">7</button>
              <button class="btn btn-sm btn-danger" onclick="openSoal(8)" style="width: 35px;margin-bottom:2px;" data="jawabid4" id="no8">8</button>
              <button class="btn btn-sm btn-danger" onclick="openSoal(9)" style="width: 35px;margin-bottom:2px;" data="jawabid57" id="no9">9</button>
              <button class="btn btn-sm btn-danger" onclick="openSoal(10)" style="width: 35px;margin-bottom:2px;" data="jawabid55" id="no10">10</button>
              <button class="btn btn-sm btn-danger" onclick="openSoal(11)" style="width: 35px;margin-bottom:2px;" data="jawabid48" id="no11">11</button>
              <button class="btn btn-sm btn-danger" onclick="openSoal(12)" style="width: 35px;margin-bottom:2px;" data="jawabid26" id="no12">12</button>
              <button class="btn btn-sm btn-danger" onclick="openSoal(13)" style="width: 35px;margin-bottom:2px;" data="jawabid20" id="no13">13</button>
              <button class="btn btn-sm btn-danger" onclick="openSoal(14)" style="width: 35px;margin-bottom:2px;" data="jawabid22" id="no14">14</button>
              <button class="btn btn-sm btn-danger" onclick="openSoal(15)" style="width: 35px;margin-bottom:2px;" data="jawabid51" id="no15">15</button>
              <button class="btn btn-sm btn-danger" onclick="openSoal(16)" style="width: 35px;margin-bottom:2px;" data="jawabid50" id="no16">16</button>
              <button class="btn btn-sm btn-danger" onclick="openSoal(17)" style="width: 35px;margin-bottom:2px;" data="jawabid52" id="no17">17</button>
              <button class="btn btn-sm btn-danger" onclick="openSoal(18)" style="width: 35px;margin-bottom:2px;" data="jawabid24" id="no18">18</button>
              <button class="btn btn-sm btn-danger" onclick="openSoal(19)" style="width: 35px;margin-bottom:2px;" data="jawabid18" id="no19">19</button>
              <button class="btn btn-sm btn-danger" onclick="openSoal(20)" style="width: 35px;margin-bottom:2px;" data="jawabid46" id="no20">20</button>
              <button class="btn btn-sm btn-danger" onclick="openSoal(21)" style="width: 35px;margin-bottom:2px;" data="jawabid36" id="no21">21</button>
              <button class="btn btn-sm btn-danger" onclick="openSoal(22)" style="width: 35px;margin-bottom:2px;" data="jawabid7" id="no22">22</button>
              <button class="btn btn-sm btn-danger" onclick="openSoal(23)" style="width: 35px;margin-bottom:2px;" data="jawabid66" id="no23">23</button>
              <button class="btn btn-sm btn-danger" onclick="openSoal(24)" style="width: 35px;margin-bottom:2px;" data="jawabid12" id="no24">24</button>
              <button class="btn btn-sm btn-danger" onclick="openSoal(25)" style="width: 35px;margin-bottom:2px;" data="jawabid34" id="no25">25</button>
              <button class="btn btn-sm btn-danger" onclick="openSoal(26)" style="width: 35px;margin-bottom:2px;" data="jawabid14" id="no26">26</button>
              <button class="btn btn-sm btn-danger" onclick="openSoal(27)" style="width: 35px;margin-bottom:2px;" data="jawabid38" id="no27">27</button>
              <button class="btn btn-sm btn-danger" onclick="openSoal(28)" style="width: 35px;margin-bottom:2px;" data="jawabid8" id="no28">28</button>
              <button class="btn btn-sm btn-danger" onclick="openSoal(29)" style="width: 35px;margin-bottom:2px;" data="jawabid64" id="no29">29</button>
              <button class="btn btn-sm btn-danger" onclick="openSoal(30)" style="width: 35px;margin-bottom:2px;" data="jawabid67" id="no30">30</button>
                        <br>
          <br>
          <button onclick="selesai_ujian()" class="btn btn-sm btn-success btn-flat btn-block">Selesai Ujian</button>
        </div>
      </div>
    </section><!-- End About Lists Section -->
    <!-- Modal Gambar -->
    <!-- Modal -->
    <div class="modal fade" id="exampleModalCenter" tabindex="-1" role="dialog" aria-labelledby="exampleModalCenterTitle" aria-hidden="true">
      <div class="modal-dialog modal-dialog-centered modal-lg" role="document">
        <div class="modal-content">
          <div class="modal-header">
            <button type="button" class="close" data-dismiss="modal" aria-label="Close">
              <span aria-hidden="true">&times;</span>
            </button>
          </div>
          <div class="modal-body">
            
          </div>
        </div>
      </div>
    </div>
    <!-- END Modal Gambar -->
  </main><!-- End #main -->

  
  <!-- Vendor JS Files -->
  <script src="https://njuah-njuah.com/site/assets/vendor/jquery/jquery.min.js"></script>
  <script src="https://njuah-njuah.com/site/assets/vendor/bootstrap/js/bootstrap.bundle.min.js"></script>
  <script src="https://njuah-njuah.com/site/assets/vendor/jquery.easing/jquery.easing.min.js"></script>
  <script src="https://njuah-njuah.com/site/assets/vendor/php-email-form/validate.js"></script>
  <script src="https://njuah-njuah.com/site/assets/vendor/jquery-sticky/jquery.sticky.js"></script>
  <script src="https://njuah-njuah.com/site/assets/vendor/venobox/venobox.min.js"></script>
  <script src="https://njuah-njuah.com/site/assets/vendor/waypoints/jquery.waypoints.min.js"></script>
  <script src="https://njuah-njuah.com/site/assets/vendor/counterup/counterup.min.js"></script>
  <script src="https://njuah-njuah.com/site/assets/vendor/isotope-layout/isotope.pkgd.min.js"></script>
  <script src="https://njuah-njuah.com/site/assets/vendor/aos/aos.js"></script>

  <!-- Template Main JS File -->
  <script src="https://demo.njuah-njuah.com/assets/js/main.js"></script>

  <!-- Tree Family -->
  <script src="https://njuah-njuah.com/site/assets/vendor/jquery-ui-1.12.1/jquery-ui.min.js"></script>
  <script src="https://njuah-njuah.com/site/assets/vendor/family-tree/js/jQuery.jHTree.js"></script>

  <script src="https://demo.njuah-njuah.com/assets/countdown/jquery.countdownTimer.js"></script>
  <!-- auto scroll -->
  <script src="https://njuah-njuah.com/site/assets/vendor/auto-scroll/js/jquery.autoscroll.js"></script>
</body>

</html>  <script type="text/javascript">
    // $(document).on("keydown", disableF5);
    $(document).on("keydown", disableCtrlR);

    // window.onbeforeunload = function(){
    //     return "Yay! Selesai ujian?";
    //   }

    $(function(){
      // document.documentElement.requestFullscreen();

      window.history.pushState(null, "", window.location.href);        
      window.onpopstate = function() {
            window.history.pushState(null, "", window.location.href);
        };

      $(window).on("contextmenu",function(){
        return false;
      });

      $("#clock").countdowntimer({
          startDate : '2021-01-18 20:39:37',
          dateAndTime : '2021-01-18 20:42:35',
          size : "lg",
          displayFormat: "HMS",
          timeUp : load_hasil_ujian,
      });
    })

    function selesai_ujian(){
      if(confirm("Anda yakin sudah selesai?")){
        load_hasil_ujian();
      }
    }

    load_hasil_ujian = function(){
      $.ajax({
        url:'https://demo.njuah-njuah.com/cat/load_hasil_ujian',
        type:'get',
        beforeSend:function(){
          $("#t4_ujian").html("Memuat hasil ujian...");
          $("#t4_nomor").html("");
          setTimeout(function() { 
              location.reload();
          }, 30000);
        },
        success:function(e){
          $("#t4_ujian").html(e);
        }
      })
    }

    function disableF5(e){
      if ((e.which || e.keyCode)== 116) e.preventDefault();
    }

    function disableCtrlR(e){
      if (e.ctrlKey) e.preventDefault();
    }

    function simpan_lanjutkan(no,id){
      var now = no+1;
      var jlh_soal = 30;
      
      if(now > jlh_soal){
        now = 1;
      }

      $(".tab-pane").removeClass('active');
      $("#soal"+now).addClass('active');

      $("#simpansoalno"+no).on("submit",function(){
        $.ajax({
          url : 'https://demo.njuah-njuah.com/cat/simpan_jawaban?id='+id,
          type : 'post',
          data : $(this).serialize(),
          beforeSend:function(){

          },
          success:function(e){
            console.log(e);
            if(e=="OK"){
              $("#no"+no).attr("class","btn btn-sm btn-success");
            }
          },
          error:function(x){
            $("#no"+no).attr("class","btn btn-sm btn-warning");
          }
        })
        return false;
      })
    }

    function lewati(no){
      var jlh_soal = 30;
      
      if(no > jlh_soal){
        no = 1;
      }

      $(".tab-pane").removeClass('active');
      $("#soal"+no).addClass('active');
    }

    function openSoal(no){
      $(".tab-pane").removeClass('active');
      $("#soal"+no).addClass('active');
    }

    function kembali(no){
      $(".tab-pane").removeClass('active');
      $("#soal"+no).addClass('active');
    }

    function zoomx(pict){
      $(".modal-body").html('<img class="img img-fluid img-thumbnail" src="https://demo.njuah-njuah.com/assets/uploads/'+pict+'">');
      $("#exampleModalCenter").modal('toggle');
    }

  </script>