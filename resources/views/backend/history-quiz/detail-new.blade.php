@extends('template.default')

@section('content')

<div class="app-page-title">
    <div class="page-title-wrapper">
        <div class="page-title-heading">
            <div class="page-title-icon">
                <i class="pe-7s-copy-file icon-gradient bg-ripe-malin">
                </i>
            </div>
            <div>Total Seluruh Paket Soal Yang Pernah Dikerjakan
            </div>
        </div>
    </div>
</div>

<div class="row">
    <div class="col-lg-12">
        <div class="main-card mb-3 card">
            <div class="card-body">
                <div class="table-responsive">
                <table class="mb-0 table table-bordered" style="text-align: center;">
                    <thead>
                    <tr>
                        <th>#</th>
                        <th>Terakhir Tes</th>
                        <th>Nilai TWK</th>
                        <th>Nilai TIU</th>
                        <th>Nilai TKP</th>
                        <th>Total</th>
                        <th>Keterangan</th>
                        <th>Aksi</th>
                    </tr>
                    </thead>
                    <tbody>
                    @foreach($data as $item)
                    <tr>
                        <th scope="row">{{ $loop->iteration }}.</th>
                        <td>{{ $item->end_time->diffForHumans() }}</td>
                        <td><span class="badge badge-{{ ($item->twk()->sum('point') >= 65) ? 'success' : 'danger' }}">{{ $item->twk()->sum('point') }}</span></td>
                        <td><span class="badge badge-{{ ($item->tiu()->sum('point') >= 80) ? 'success' : 'danger' }}">{{ $item->tiu()->sum('point') }}</span></td>
                        <td><span class="badge badge-{{ ($item->tkp()->sum('point') >= 166) ? 'success' : 'danger' }}">{{ $item->tkp()->sum('point') }}</span></td>
                        <td><span class="badge badge-primary">{{ $item->total()->sum('point') }}</span></td>
                        <td>
                            @if ($item->twk()->sum('point') >= 65 && $item->tiu()->sum('point') >= 80 && $item->tkp()->sum('point') >= 166)
                            <span class="badge badge-success">Lolos Passing Grade</span>
                            @else
                            <span class="badge badge-danger">Tidak Lolos Passing Grade</span>
                            @endif
                        </td>
                        {{-- <td><a href="{{ route('history.show', [$item->id, Auth::user()->id]) }}" class="btn btn-primary">Detail</a></td> --}}
                        <td>
                            @if (Auth::user()->login_active === 'cpns')
                            <a href="{{ route('history.show', [$item->id, Auth::user()->id]) }}" class="btn btn-info">
                            @elseif (Auth::user()->login_active === 'kedinasan')
                            <a href="{{ route('history.show.kedinasan', [$item->id, Auth::user()->id]) }}" class="btn btn-info">
                            @endif
                            <i class="fa fa-arrow-right"></i> Pembahasan</a>
                        </td>
                    </tr>
                    @endforeach
                    </tbody>
                </table>
                </div>
            </div>
        </div>
    </div>
</div>

@endsection