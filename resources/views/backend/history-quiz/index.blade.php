@extends('template.default')

@section('content')

<div class="app-page-title">
    <div class="page-title-wrapper">
        <div class="page-title-heading">
            <div class="page-title-icon">
                <i class="pe-7s-copy-file icon-gradient bg-ripe-malin">
                </i>
            </div>
            <div>Total Seluruh Paket Soal Yang Pernah Dikerjakan
            </div>
        </div>
    </div>
</div>

<div class="row">
    <div class="col-lg-12">
        <div class="main-card mb-3 card">
            <div class="card-body">
                <div class="table-responsive">
                <table class="mb-0 table table-bordered" style="text-align: center;">
                    <thead>
                    <tr>
                        <th>#</th>
                        <th>Judul Paket</th>
                        <th>Terakhir Tes</th>
                        <th>Aksi</th>
                    </tr>
                    </thead>
                    <tbody>
                    @if (Auth::user()->login_active === 'cpns')
                        @foreach($package as $item)
                            @if ($item->question->count() > 0)
                            <tr>
                                <th scope="row">{{ $loop->iteration }}.</th>
                                <td>Paket {{ $item->name }}</td>
                                <td>
                                    @if (is_null($item->answer))
                                        -
                                    @else
                                        {{ $item->answer->end_time->diffForHumans() }}</td>
                                    @endif
                                <td>
                                    @if ($item->urut <= 33)
                                    <a href="{{ route('history.quiz.detail', [$item->id, Auth::user()->id]) }}" class="btn btn-primary">Detail</a>
                                    @elseif ($item->urut >= 34)
                                    <a href="{{ route('history.quiz.detail.new', [$item->id, Auth::user()->id]) }}" class="btn btn-danger">Detail</a>
                                    @endif
                                </td>
                            </tr>
                            @endif
                        @endforeach
                    @elseif (Auth::user()->login_active === 'kedinasan')
                        @foreach($packagesEleven->take(1) as $item)
                        <tr>
                            <th scope="row">{{ $loop->iteration }}.</th>
                            <td>Paket 1</td>
                            <td>
                                @if (is_null($item->answerKedinasan))
                                    -
                                @else
                                    {{ $item->answerKedinasan->end_time->diffForHumans() }}</td>
                                @endif
                            <td><a href="{{ route('history.quiz.detail.kedinasan', [$item->id, Auth::user()->id]) }}" class="btn btn-primary">Detail</a></td>
                        </tr>
                        @endforeach
                        @foreach($package->take(10)->skip(1) as $item)
                            {{-- {{dd($item->answerKedinasan->end_time)}} --}}
                            @if ($item->question->count() > 0)
                            <tr>
                                <th scope="row">{{ $loop->iteration }}.</th>
                                <td>Paket {{ $item->name }}</td>
                                <td>
                                    @if (is_null($item->answerKedinasan))
                                        -
                                    @else
                                        {{ $item->answerKedinasan->end_time->diffForHumans() }}</td>
                                    @endif
                                <td><a href="{{ route('history.quiz.detail.kedinasan', [$item->id, Auth::user()->id]) }}" class="btn btn-primary">Detail</a></td>
                            </tr>
                            @endif
                        @endforeach
                    @endif
                    </tbody>
                </table>
                </div>
            </div>
        </div>
    </div>
</div>

@endsection