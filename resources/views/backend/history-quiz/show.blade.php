@extends('template.default')
  <!-- Vendor CSS Files -->
  <link href="{{ asset('quiz/assets/vendor/css/bootstrap.min.css') }}" rel="stylesheet">
  <link href="{{ asset('quiz/assets/vendor/css/icofont.min.css') }}" rel="stylesheet">
  <link href="{{ asset('quiz/assets/vendor/css/boxicons.min.css') }}" rel="stylesheet">
  <link href="{{ asset('quiz/assets/vendor/css/animate.min.css') }}" rel="stylesheet">

  <link href="{{ asset('quiz/assets/vendor/css/venobox.css') }}" rel="stylesheet">
  <link href="{{ asset('quiz/assets/vendor/css/aos.css') }}" rel="stylesheet">

  <!-- Template Main CSS File -->
  <link href="{{ asset('quiz/assets/vendor/css/style.css') }}" rel="stylesheet">

  <!-- Tree Family -->
  <link href="{{ asset('quiz/assets/vendor/css/jquery-ui.min.css') }}" rel="stylesheet"></script>
  <link href="{{ asset('quiz/assets/vendor/css/jHTree.css') }}" rel="stylesheet">
  <style>
    .btn {
        font-size: .6rem !important;
    }
  </style>  
@section('content')

<div class="row">
    <div class="col-md-8">
        <div class="main-card mb-3 card">
            <div class="card-body">
                <div class=col-sm-12" id="t4_ujian">
                    <div class="tab-content">
                        @foreach ($datas as $item)
                        <div class="tab-pane {{ ($loop->iteration == 1) ? 'active' : '' }} " id="soal{{ $loop->iteration }}" role="tabpanel" aria-labelledby="soal{{ $loop->iteration }}-tab">
                          <label><b>Soal: {{ $item->question->jenis }}</b></label>
                            <hr>
                            <p style="padding-left: 25px">
                                <span style="position: absolute;  margin-left: -1.8rem; font-weight: bold;">{{ $loop->iteration }}.</span>
                                @if (!is_null($item->question->img_question))
                                <img src="{{ config('app.image') }}/{{ $item->question->img_question }}" style="max-width:300px; max-height:200px">
                                @endif
                                <b>{!! $item->question->question !!}</b>
                            </p>
                            <br />
                            <form class="form" action="#" method="post" id="simpansoalno{{ $loop->iteration }}">
                            <ol type="A">
                                <li>
                                  <input type="radio" name="jawab" value="a" {{ ($item->option_a == 1) ? 'checked' : '' }}>
                                    &ensp;
                                    @if ($item->question->answer == $item->answer && $item->option_a == 1)
                                    <span style="color: green; ">
                                      @if (!is_null($item->question->img_option_a))
                                      <img src="{{ config('app.image') }}/{{ $item->question->img_option_a }}" style="max-width:300px; max-height:200px">
                                      @endif
                                      {!! $item->question->option_a !!}
                                    </span> 
                                    @elseif ($item->question->answer != $item->answer && $item->option_a == 1)
                                    <span style="color: red; ">
                                      @if (!is_null($item->question->img_option_a))
                                      <img src="{{ config('app.image') }}/{{ $item->question->img_option_a }}" style="max-width:300px; max-height:200px">
                                      <b>x</b>
                                      @endif
                                      {!! $item->question->option_a !!}
                                    </span> 
                                    @elseif ($item->question->answer == 'a')
                                    <span style="color: green; ">
                                      @if (!is_null($item->question->img_option_a))
                                      <img src="{{ config('app.image') }}/{{ $item->question->img_option_a }}" style="max-width:300px; max-height:200px">
                                      <i class="fa fa-check"></i>
                                      @endif
                                      {!! $item->question->option_a !!}
                                    </span> 
                                    @else
                                    <span>
                                      @if (!is_null($item->question->img_option_a))
                                      <img src="{{ config('app.image') }}/{{ $item->question->img_option_a }}" style="max-width:300px; max-height:200px">
                                      @endif
                                      {!! $item->question->option_a !!}
                                    </span> 
                                    @endif
                                </li>
                                <br>
                                <li>
                                    <input type="radio" name="jawab" value="b" {{ ($item->option_b == 1) ? 'checked' : '' }}>
                                    &ensp;
                                    @if ($item->question->answer == $item->answer && $item->option_b == 1)
                                    <span style="color: green; ">
                                      @if (!is_null($item->question->img_option_b))
                                      <img src="{{ config('app.image') }}/{{ $item->question->img_option_b }}" style="max-width:300px; max-height:200px">
                                      @endif
                                      {!! $item->question->option_b !!}
                                    </span> 
                                    @elseif ($item->question->answer != $item->answer && $item->option_b == 1)
                                    <span style="color: red; ">
                                      @if (!is_null($item->question->img_option_b))
                                      <img src="{{ config('app.image') }}/{{ $item->question->img_option_b }}" style="max-width:300px; max-height:200px">
                                      <b>x</b>
                                      @endif
                                      {!! $item->question->option_b !!}
                                    </span> 
                                    @elseif ($item->question->answer == 'b')
                                    <span style="color: green; ">
                                      @if (!is_null($item->question->img_option_b))
                                      <img src="{{ config('app.image') }}/{{ $item->question->img_option_b }}" style="max-width:300px; max-height:200px">
                                      <i class="fa fa-check"></i>
                                      @endif
                                      {!! $item->question->option_b !!}
                                    </span> 
                                    @else
                                    <span>
                                      @if (!is_null($item->question->img_option_b))
                                      <img src="{{ config('app.image') }}/{{ $item->question->img_option_b }}" style="max-width:300px; max-height:200px">
                                      @endif
                                      {!! $item->question->option_b !!}
                                    </span> 
                                    @endif
                                </li>
                                <br>
                                <li>
                                    <input type="radio" name="jawab" value="c" {{ ($item->option_c == 1) ? 'checked' : '' }}>
                                    &ensp;
                                    @if ($item->question->answer == $item->answer && $item->option_c == 1)
                                    <span style="color: green; ">
                                      @if (!is_null($item->question->img_option_c))
                                      <img src="{{ config('app.image') }}/{{ $item->question->img_option_c }}" style="max-width:300px; max-height:200px">
                                      @endif
                                      {!! $item->question->option_c !!}
                                    </span> 
                                    @elseif ($item->question->answer != $item->answer && $item->option_c == 1)
                                    <span style="color: red; ">
                                      @if (!is_null($item->question->img_option_c))
                                      <img src="{{ config('app.image') }}/{{ $item->question->img_option_c }}" style="max-width:300px; max-height:200px">
                                      <b>x</b>
                                      @endif
                                      {!! $item->question->option_c !!}
                                    </span> 
                                    @elseif ($item->question->answer == 'c')
                                    <span style="color: green; ">
                                      @if (!is_null($item->question->img_option_c))
                                      <img src="{{ config('app.image') }}/{{ $item->question->img_option_c }}" style="max-width:300px; max-height:200px">
                                      <i class="fa fa-check"></i>
                                      @endif
                                      {!! $item->question->option_c !!}
                                    </span> 
                                    @else
                                    <span>
                                      @if (!is_null($item->question->img_option_c))
                                      <img src="{{ config('app.image') }}/{{ $item->question->img_option_c }}" style="max-width:300px; max-height:200px">
                                      @endif
                                      {!! $item->question->option_c !!}
                                    </span> 
                                    @endif
                                </li>
                                <br>
                                <li>
                                  <input type="radio" name="jawab" value="d" {{ ($item->option_d == 1) ? 'checked' : '' }}>
                                  &ensp;
                                  @if ($item->question->answer == $item->answer && $item->option_d == 1)
                                    <span style="color: green; ">
                                    @if (!is_null($item->question->img_option_d))
                                      <img src="{{ config('app.image') }}/{{ $item->question->img_option_d }}" style="max-width:300px; max-height:200px">
                                      @endif
                                      {!! $item->question->option_d !!}
                                    </span> 
                                    @elseif ($item->question->answer != $item->answer && $item->option_d == 1)
                                    <span style="color: red; ">
                                      @if (!is_null($item->question->img_option_d))
                                      <img src="{{ config('app.image') }}/{{ $item->question->img_option_d }}" style="max-width:300px; max-height:200px">
                                      <b>x</b>
                                      @endif
                                      {!! $item->question->option_d !!}
                                    </span> 
                                    @elseif ($item->question->answer == 'd')
                                    <span style="color: green; ">
                                      @if (!is_null($item->question->img_option_d))
                                      <img src="{{ config('app.image') }}/{{ $item->question->img_option_d }}" style="max-width:300px; max-height:200px">
                                      <i class="fa fa-check"></i>
                                      @endif
                                      {!! $item->question->option_d !!}
                                    </span> 
                                    @else
                                    <span>
                                      @if (!is_null($item->question->img_option_d))
                                      <img src="{{ config('app.image') }}/{{ $item->question->img_option_d }}" style="max-width:300px; max-height:200px">
                                      @endif
                                      {!! $item->question->option_d !!}
                                    </span> 
                                  @endif
                                </li>
                                <br>
                                <li>
                                  <input type="radio" name="jawab" value="e" {{ ($item->option_e == 1) ? 'checked' : '' }}>
                                  &ensp;
                                  @if ($item->question->answer == $item->answer && $item->option_e == 1)
                                  <span style="color: green; ">
                                  @if (!is_null($item->question->img_option_e))
                                    <img src="{{ config('app.image') }}/{{ $item->question->img_option_e }}" style="max-width:300px; max-height:200px">
                                    @endif
                                    {!! $item->question->option_e !!}
                                  </span> 
                                  @elseif ($item->question->answer != $item->answer && $item->option_e == 1)
                                  <span style="color: red; ">
                                    @if (!is_null($item->question->img_option_e))
                                    <img src="{{ config('app.image') }}/{{ $item->question->img_option_e }}" style="max-width:300px; max-height:200px">
                                    <b>x</b>
                                    @endif
                                    {!! $item->question->option_e !!}
                                  </span> 
                                  @elseif ($item->question->answer == 'e')
                                  <span style="color: green; ">
                                    @if (!is_null($item->question->img_option_e))
                                    <img src="{{ config('app.image') }}/{{ $item->question->img_option_e }}" style="max-width:300px; max-height:200px">
                                    <i class="fa fa-check"></i>
                                    @endif
                                    {!! $item->question->option_e !!}
                                  </span> 
                                  @else
                                  <span>
                                    @if (!is_null($item->question->img_option_e))
                                    <img src="{{ config('app.image') }}/{{ $item->question->img_option_e }}" style="max-width:300px; max-height:200px">
                                    @endif
                                    {!! $item->question->option_e !!}
                                  </span> 
                                  @endif
                                </li>
                            </ol>

                            @if ($item->question->answer == $item->answer)
                            <center><span class="badge badge-success">Jawaban Benar</span></center>
                            @else
                            <center><span class="badge badge-danger" >Jawaban Salah</span></center>
                            @endif

                            <hr>
                            <b>PEMBAHASAN</b>
                            <br>
                            <br>
                            <div class="table-responsive" style="border: 2px solid green; padding: 20px; border-radius: 15px;">
                                <p>{!! $item->question->discussion !!}</p>
                                @if (!is_null($item->question->img_discussion))
                                  <img src="{{ config('app.image') }}/{{ $item->question->img_discussion }}" style="max-width:500px; max-height:500px">
                                @endif
                                @if ($item->jenis == 'TKP')
                                <div class="col-md-3">
                                  <b>Jawaban A </b> = &nbsp;&nbsp;<span class="badge badge-primary">{{ $item->question->value_a }}</span> <br><br>
                                  <b>Jawaban B </b> = &nbsp;&nbsp;<span class="badge badge-primary">{{ $item->question->value_b }}</span> <br><br>
                                  <b>Jawaban C </b> = &nbsp;&nbsp;<span class="badge badge-primary">{{ $item->question->value_c }}</span> <br><br>
                                  <b>Jawaban D </b> = &nbsp;&nbsp;<span class="badge badge-primary">{{ $item->question->value_d }}</span> <br><br>
                                  <b>Jawaban E </b> = &nbsp;&nbsp;<span class="badge badge-primary">{{ $item->question->value_e }}</span> <br><br>
                                </div>
                                @endif
                            </div>
                            <hr>

                            @if($loop->iteration != 1)
                            <button type="button" class="btn btn-warning btn-flat" onclick="kembali({{ $loop->iteration }}-1)">Kembali</button>
                            @endif
                            @if($loop->iteration != $datas->count())
                            <button type="button" class="btn btn-success btn-flat" onclick="lewati({{ $loop->iteration }}+1)">Lewati</button>
                            @endif
                          </form>
                        </div>
                        @endforeach
                    </div>
                </div>
            </div>
        </div>
    </div>

    <div class="col-md-4">
        <div class="main-card mb-3 card">
            <div class="card-body">
                <div class="" id="t4_nomor">
                    <p><b>TWK</b></p>
                    @foreach ($datas as $item)
                    @php
                      if ($item->question->answer == $item->answer) { $button = 'btn-success'; }
                      else { $button = 'btn-danger'; }
                    @endphp
                    @if ($item->jenis == 'TWK')
                    <button 
                      class="btn btn-sm {{ $button }}" 
                      onclick="openSoal({{ $loop->iteration }})" 
                      style="width: 35px;margin-bottom:2px;" data="jawabid{{ $item->id }}" id="no{{ $loop->iteration }}">
                      {{ $loop->iteration }}
                    </button>
                    @endif
                    @endforeach
                    <br>
                    <hr>
                    <p><b>TIU</b></p>
                    @foreach ($datas as $item)
                    @php
                      if ($item->question->answer == $item->answer) { $button = 'btn-success'; }
                      else { $button = 'btn-danger'; }
                    @endphp
                    @if ($item->jenis == 'TIU')
                    <button 
                      class="btn btn-sm {{ $button }}" 
                      onclick="openSoal({{ $loop->iteration }})" 
                      style="width: 35px;margin-bottom:2px;" data="jawabid{{ $item->id }}" id="no{{ $loop->iteration }}">
                      {{ $loop->iteration }}
                    </button>
                    @endif
                    @endforeach
                    <br>
                    <hr>
                    <p><b>TKP</b></p>
                    @foreach ($datas as $item)
                    @php
                      if ($item->question->answer == $item->answer) { $button = 'btn-success'; }
                      else { $button = 'btn-danger'; }
                    @endphp
                    @if ($item->jenis == 'TKP')
                    <button 
                      class="btn btn-sm {{ $button }}" 
                      onclick="openSoal({{ $loop->iteration }})" 
                      style="width: 35px;margin-bottom:2px;" data="jawabid{{ $item->id }}" id="no{{ $loop->iteration }}">
                      {{ $loop->iteration }}
                    </button>
                    @endif
                    @endforeach
                    <br>
                    <hr>
                    <a href="{{ route('history.quiz') }}" class="btn btn-primary btn-sm">Lihat Semua Pembahasan</a>
                    <br>
                </div>
            </div>
        </div>
    </div>
</div>


@endsection

<!-- Vendor JS Files -->
<script src="{{ asset('quiz/assets/vendor/js/jquery.min.js') }}"></script>
<script src="{{ asset('quiz/assets/vendor/js/bootstrap.bundle.min.js') }}"></script>
<script src="{{ asset('quiz/assets/vendor/js/jquery.easing.min.js') }}"></script>
<script src="{{ asset('quiz/assets/vendor/js/validate.js') }}"></script>
<script src="{{ asset('quiz/assets/vendor/js/jquery.sticky.js') }}"></script>
<script src="{{ asset('quiz/assets/vendor/js/venobox.min.js') }}"></script>
<script src="{{ asset('quiz/assets/vendor/js/jquery.waypoints.min.js') }}"></script>
<script src="{{ asset('quiz/assets/vendor/js/counterup.min.js') }}"></script>
<script src="{{ asset('quiz/assets/vendor/js/isotope.pkgd.min.js') }}"></script>
<script src="{{ asset('quiz/assets/vendor/js/aos.js') }}"></script>

<!-- Template Main JS File -->
<script src="{{ asset('quiz/assets/vendor/js/main.js') }}"></script>

<!-- Tree Family -->
<script src="{{ asset('quiz/assets/vendor/js/jquery-ui.min.js') }}"></script>
<script src="{{ asset('quiz/assets/vendor/js/jQuery.jHTree.js') }}"></script>

<script src="{{ asset('quiz/assets/vendor/js/jquery.countdownTimer.js') }}"></script>

<script type="text/javascript">
  $.ajaxSetup({
      headers: {
          'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
      }
  });
</script>
<script type="text/javascript">
  $.ajaxSetup({
      headers: {
          'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
      }
  });
</script>
<script type="text/javascript">
    // $(document).on("keydown", disableF5);
    $(document).on("keydown", disableCtrlR);

    // window.onbeforeunload = function(){
    //     return "Yay! Selesai ujian?";
    //   }

    $(function(){
      // document.documentElement.requestFullscreen();

      window.history.pushState(null, "", window.location.href);        
      window.onpopstate = function() {
            window.history.pushState(null, "", window.location.href);
        };

      $(window).on("contextmenu",function(){
        return false;
      });

      $("#clock").countdowntimer({
          startDate : '2021-01-18 20:39:37',
          dateAndTime : '2021-01-18 20:42:35',
          size : "lg",
          displayFormat: "HMS",
          timeUp : load_hasil_ujian,
      });
    })

    function selesai_ujian(){
      if(confirm("Anda yakin sudah selesai?")){
        load_hasil_ujian();
      }
    }

    load_hasil_ujian = function(){
      $.ajax({
        url:'/review',
        type:'get',
        beforeSend:function(){
          $("#t4_ujian").html("Memuat hasil ujian...");
          $("#t4_nomor").html("");
          // setTimeout(function() { 
          //     location.reload();
          // }, 30000); //reload
        },
        success:function(e){
          $("#t4_ujian").html(e);
        }
      })
    }

    function disableF5(e){
      if ((e.which || e.keyCode)== 116) e.preventDefault();
    }

    function disableCtrlR(e){
      if (e.ctrlKey) e.preventDefault();
    }

    function simpan_lanjutkan(no,id){
      var now = no+1;
      var jlh_soal = {{ $datas->count() }};
      
      if(now > jlh_soal){
        now = 1;
      }

      $(".tab-pane").removeClass('active');
      $("#soal"+now).addClass('active');

      $("#simpansoalno"+no).on("submit",function(){
        $.ajax({
          url : 'store/'+id,
          method : 'POST',
          data : $(this).serialize(),
          beforeSend:function(){

          },
          success:function(e){
            if(e=="OK"){
              $("#no"+no).attr("class","btn btn-sm btn-success");
            }
          },
          error:function(x){
            $("#no"+no).attr("class","btn btn-sm btn-warning");
          }
        })
        return false;
      })
    }

    function lewati(no){
      var jlh_soal = 100;
      
      if(no > jlh_soal){
        no = 1;
      }

      $(".tab-pane").removeClass('active');
      $("#soal"+no).addClass('active');
    }

    function openSoal(no){
      $(".tab-pane").removeClass('active');
      $("#soal"+no).addClass('active');
    }

    function kembali(no){
      $(".tab-pane").removeClass('active');
      $("#soal"+no).addClass('active');
    }

    function zoomx(pict){
      $(".modal-body").html('<img class="img img-fluid img-thumbnail" src="https://demo.njuah-njuah.com/assets/uploads/'+pict+'">');
      $("#exampleModalCenter").modal('toggle');
    }

  </script>