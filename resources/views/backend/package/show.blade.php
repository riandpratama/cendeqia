@extends('template.default')

@section('content')

<div class="app-page-title">
    <div class="page-title-wrapper">
        <div class="page-title-heading">
            <div class="page-title-icon">
                <i class="pe-7s-display2 icon-gradient bg-love-kiss">
                </i>
            </div>
            <div>Petunjuk Pengerjaan Soal
            </div>
        </div>
    </div>
</div>

<div class="main-card card">
    <div class="card-body">
        <div class="tops text-center">
          <span style="color: #2575b1; font-size: 25px;">Paket</span>
          <span style="color: #feb81b; font-size: 35px;" class="numbers d-block">
            @if (Auth::user()->login_active === 'cpns')
                {{ $package->name }}
            @elseif (Auth::user()->login_active === 'kedinasan')
                @if ($package->name == 11)
                1
                @else
                {{ $package->name }}
                @endif
            @endif
          </span>
        </div>
        <div class="py-2"></div>
            <div class="infos">
              <span style="color: #2575b1; font-size: 20px;" class="d-block text-center">PETUNJUK</span>
              <div class="py-1 my-1"></div>
              <br>
              <span class=" ml-2" style="color: #2575b1;">
                {!! $descriptionQuiz->keterangan !!}
              </span>
              <!-- <p class=" ml-2" style="color: #2575b1;">Waktu Pengerjaan 90 menit</p>
              <p class=" ml-2" style="color: #2575b1;">Petunjuk:<br />
                1. Terdapat 100 soal<br />
                2. Nomor 1-30 merupakan soal Tes Wawasan Kebangsaan (TWK)<br />
                3. Nomor 31-65 merupakan soal Tes Intelegensi Umum (TIU)<br />
                4. Nomor 66-100 merupakan soal Tes Karakteristik Pribadi (TKP)<br />
                5. Kriteria Penilaian sesuai dengan PERMENPAN No 24 Tahun 2019</p> -->

              <div class="py-2"></div>

              <div class="text-center bloc_nsl_btn">
                <a href="{{ route('package.index') }}" class="btn btn-info">KEMBALI</a>
                @if (isset($history) && $history->status == 0)
                    <a href="{{ route('quiz',[$history->id, $history->user_id]) }}" class="btn btn-warning">UJIAN BELUM SELESAI HARAP LANJUTKAN</a>
                @else
                    @if (Auth::user()->login_active === 'cpns')
                    <form action="{{ route('package.store', $package->id) }}" method="post" style="display: inline;">
                        @csrf
                        <button type="submit" class="btn btn-warning btnxl_set"
                            onclick="return confirm('Waktu akan berjalan ketika anda klik OK. Anda yakin ingin memulai nya?')">
                            MULAI
                        </button>
                    </form>
                    @elseif (Auth::user()->login_active === 'kedinasan')
                    <form action="{{ route('package.store.kedinasan', $package->id) }}" method="post" style="display: inline;">
                        @csrf
                        <button type="submit" class="btn btn-warning btnxl_set"
                            onclick="return confirm('Waktu akan berjalan ketika anda klik OK. Anda yakin ingin memulai nya?')">
                            MULAI
                        </button>
                    </form>
                    @endif
                @endif
                @if (Auth::user()->login_active === 'cpns')
                    @if (isset($history) && $history->status == 1)
                        <a href="{{ route('history.quiz.detail',[$history->package_id, $history->user_id]) }}" class="btn btn-success">PEMBAHASAN</a>
                    @endif
                @elseif (Auth::user()->login_active === 'kedinasan')
                    @if (isset($historyKedinasan) && $historyKedinasan->status == 1)
                        <a href="{{ route('history.quiz.detail.kedinasan',[$historyKedinasan->package_id, $historyKedinasan->user_id]) }}" class="btn btn-success">PEMBAHASAN</a>
                    @endif
                @endif
              </div>

              <div class="clear"></div>
            </div>
    </div>
</div>

@endsection