@extends('template.default')

@section('content')

<div class="app-page-title">
    <div class="page-title-wrapper">
        <div class="page-title-heading">
            <div class="page-title-icon">
                <i class="pe-7s-display2 icon-gradient bg-amy-crisp">
                </i>
            </div>
            <div>Total Seluruh Paket
            </div>
        </div>
    </div>
</div>

<div class="main-card card">
    <div class="card-body">
        <div class="col-12">
            <div class="row">
                @if (Auth::user()->login_active === 'cpns')
                    @foreach($packages as $item)
                        @if ($item->question->count() > 0 || $item->urut == 36)
                        <div class="col-md-2">
                            <a href="{{ route('package.show', $item->id) }}" style="color: black; text-decoration: none;">
                            <div class="main-card mb-3 card bg-warning">
                                <div class="card-body">
                                    <center>
                                        <h6 class="card-title" style="color: white;"><b>Paket</b></h6>
                                        <p style="font-size: 24px; color: white;"><b>{{ $item->name }}</b></p>
                                    </center>
                                </div>
                            </div>
                            </a>
                        </div>
                        @else
                        <div class="col-md-2">
                            {{-- <a href="" style="color: black; text-decoration: none;"> --}}
                            <div class="main-card mb-3 card bg-secondary">
                                <div class="card-body">
                                    <i class="pe-7s-lock " style="position: absolute; font-size: 15px; color: black; font-weight: bold;"></i>
                                    <center>
                                        <h6 class="card-title" style="color: white;"><b>Paket</b></h6>
                                        <p style="font-size: 24px; color: white;"><b>{{ $item->name }}</b></p>
                                    </center>
                                </div>
                            </div>
                            {{-- </a> --}}
                        </div>
                        @endif
                    @endforeach
                @elseif (Auth::user()->login_active === 'kedinasan')
                    @foreach($packagesEleven->take(1) as $item)
                        <div class="col-md-3">
                            <a href="{{ route('package.show', $item->id) }}" style="color: black; text-decoration: none;">
                            <div class="main-card mb-3 card bg-warning">
                                <div class="card-body">
                                    <center>
                                        <h6 class="card-title" style="color: white;"><b>Paket</b></h6>
                                        <p style="font-size: 24px; color: white;"><b>1</b></p>
                                    </center>
                                </div>
                            </div>
                            </a>
                        </div>
                    @endforeach
                    @foreach($packages->take(10)->skip(1) as $item)
                        @if ($item->question->count() > 0)
                        <div class="col-md-3">
                            <a href="{{ route('package.show', $item->id) }}" style="color: black; text-decoration: none;">
                            <div class="main-card mb-3 card bg-warning">
                                <div class="card-body">
                                    <center>
                                        <h6 class="card-title" style="color: white;"><b>Paket</b></h6>
                                        <p style="font-size: 24px; color: white;"><b>{{ $loop->iteration + 1 }}</b></p>
                                    </center>
                                </div>
                            </div>
                            </a>
                        </div>
                        @else
                        <div class="col-md-3">
                            {{-- <a href="" style="color: black; text-decoration: none;"> --}}
                            <div class="main-card mb-3 card bg-secondary">
                                <div class="card-body">
                                    <i class="pe-7s-lock " style="position: absolute; font-size: 15px; color: black; font-weight: bold;"></i>
                                    <center>
                                        <h6 class="card-title" style="color: white;"><b>Paket</b></h6>
                                        <p style="font-size: 24px; color: white;"><b>{{ $item->name }}</b></p>
                                    </center>
                                </div>
                            </div>
                            {{-- </a> --}}
                        </div>
                        @endif
                    @endforeach
                @endif
	    	</div>
	    </div>
    </div>
</div>
@endsection