@extends('template.default')

@section('content')

<div class="app-page-title">
    <div class="page-title-wrapper">
        <div class="page-title-heading">
            <div class="page-title-icon">
                <i class="pe-7s-eyedropper icon-gradient bg-strong-bliss">
                </i>
            </div>
            <div>Tambah Saran & Komentar </b>
            </div>
        </div>
    </div>
</div>

<div class="row">
    <div class="col-md-12">
        <div class="main-card mb-3 card">
            <div class="card-body">
                <form action="{{ route('kritik-saran.store') }}" method="POST" enctype="multipart/form-data">
                    @csrf
                    <div class="form-row">
                        <div class="col-md-12">
                            <div class="position-relative form-group">
                                <label for="description" class="" style="font-weight: bold;">Isi Saran & Komentar</label>
                                <textarea name="description" id="mytextarea" class="form-control" rows="10"></textarea>
                            </div>
                        </div>
                    </div>
                    <a href="{{ route('kritik-saran.index') }}" class="mt-2 btn btn-danger" >BATAL</a>
                    <button type="submit" class="mt-2 btn btn-primary" onclick="return confirm('Anda yakin ingin menyimpan?')">SIMPAN</button>
                </form>
            </div>
        </div>
    </div>
</div>
<script src="https://cdn.tiny.cloud/1/w6vzdd6iabbywgbtaez9oijzj2jymhc500w1obh9ftcvxlkd/tinymce/5/tinymce.min.js" referrerpolicy="origin"/></script>
<script>
  tinymce.init({
    selector: '#mytextarea'
  });
</script>
@endsection