@extends('template.default')

@section('content')

<div class="app-page-title">
    <div class="page-title-wrapper">
        <div class="page-title-heading">
            <div class="page-title-icon">
                <i class="pe-7s-eyedropper icon-gradient bg-strong-bliss">
                </i>
            </div>
            <div>Seluruh Riwayat Saran dan Komentar </b>
            </div>
        </div>    
    </div>
</div>

<div class="row">
    <div class="col-md-12">
        <div class="main-card mb-3 card">
            <div class="card-body">
                <a href="{{ route('kritik-saran.create') }}" class="mt-2 btn btn-primary">Tambah Saran & Komentar</a>
                <hr>
                <div class="form-row">
                    <div class="col-lg-12">
                        <div class="main-card mb-3 card">
                            <div class="card-body">
                                <h5 class="card-title">Data Riwayat Saran & Komentar</h5>
                                <div class="table-responsive">
                                    <table class="mb-0 table table-hover">
                                        <thead>
                                        <tr>
                                            <th>#</th>
                                            <th>Pada Tanggal</th>
                                            <th>Isi Saran & Komentar</th>
                                        </tr>
                                        </thead>
                                        <tbody>
                                        @foreach($data as $item)
                                        <tr>
                                            <th scope="row">{{ $loop->iteration }}</th>
                                            <td>{{ date('d/m/Y H:i:s', strtotime($item->created_at)) }}</td>
                                            <td>{!! $item->description !!}</td>
                                        </tr>
                                        @endforeach
                                        </tbody>
                                    </table>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection