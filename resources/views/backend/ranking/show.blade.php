@extends('template.default')
<link href="{{ asset('frontend/assets/css/jquery.dataTables.min.css') }}" rel="stylesheet">
@section('content')

<div class="row">
    <div class="col-md-12">
        <div class="main-card mb-3 card">
            <div class="card-header"><b>Peringkat</b></div>
            <div class="table-responsive" style="padding:20px;">
                <table class="align-middle mb-0 table table-borderless table-striped table-hover" id="example">
                    <thead>
                    <tr>
                        <th class="text-center">Peringkat</th>
                        <th>Nama</th>
                        <th class="text-center">Nilai TWK</th>
                        <th class="text-center">Nilai TIU</th>
                        <th class="text-center">Nilai TKP</th>
                        <th class="text-center">Total</th>
                        <th class="text-center">Status</th>
                    </tr>
                    </thead>
                    <tbody>
                        @foreach($data->unique('user_id') as $item)
                        <tr style="background-color: {{ ($item->user_id == Auth::user()->id ?  '#FCB71A' : '') }}">
                            {{-- <td class="text-center text-muted">
                                @if ($loop->iteration === 1)
                                <i class="pe-7s-medal icon-gradient bg-strong-bliss"></i>
                                @elseif ($loop->iteration === 2)
                                <i class="pe-7s-star icon-gradient bg-strong-bliss"></i>
                                @elseif ($loop->iteration === 3)
                                <i class="pe-7s-like2 icon-gradient bg-ripe-malin"></i>
                                @else
                                <i class=" icon-gradient bg-strong-bliss">&nbsp;&nbsp;&nbsp;&nbsp;</i>
                                @endif
                                {{ $loop->iteration }}
                            </td> --}}
                            <td class="text-center text-muted"></td>
                            <td>
                                <div class="widget-content p-0">
                                    <div class="widget-content-wrapper">
                                        <div class="widget-content-left mr-3">
                                            <div class="widget-content-left">
                                                @if (is_null($item->user->photo))
                                                    <img width="40" height="40" class="rounded-circle" src="{{ asset('backend/assets/img/default.jpg') }}" alt="">
                                                @else
                                                    <img width="40" height="40" class="rounded-circle" src="{{ asset('storage/'.$item->user->photo) }}" alt="">
                                                @endif
                                            </div>
                                        </div>
                                        <div class="widget-content-left flex2">
                                            <div class="widget-heading">{{ $item->user->name }}</div>
                                            <div class="widget-subheading opacity-7">{{ $item->end_time->diffForHumans() }}</div>
                                        </div>
                                    </div>
                                </div>
                            </td>
                            <td class="text-center"><span class="badge badge-{{ ($item->twk()->sum('point') >= 60) ? 'success' : 'danger' }}">{{ $item->twk()->sum('point') }}</span></td>
                            <td class="text-center"><span class="badge badge-{{ ($item->tiu()->sum('point') >= 80) ? 'success' : 'danger' }}">{{ $item->tiu()->sum('point') }}</span></td>
                            <td class="text-center"><span class="badge badge-{{ ($item->tkp()->sum('point') >= 126) ? 'success' : 'danger' }}">{{ $item->tkp()->sum('point') }}</span></td>
                            <td class="text-center"><span class="badge badge-primary">{{ $item->total()->sum('point') }}</span></td>
                            <td class="text-center">
                                @if ($item->twk()->sum('point') >= 60 && $item->tiu()->sum('point') >= 80 && $item->tkp()->sum('point') >= 126)
                                    <span class="badge badge-success">Lolos Passing Grade</span>
                                @else
                                    <span class="badge badge-danger">Tidak Lolos Passing Grade</span>
                                @endif
                            </td>
                        </tr>
                        @endforeach
                    </tbody>
                </table>
            </div>
        </div>
    </div>
</div>



@endsection

@section('js')
<script type="text/javascript" src="{{ asset('frontend/assets/js/jquery-3.5.1.js') }}"></script>
<script type="text/javascript" src="{{ asset('frontend/assets/js/jquery.dataTables.min.js') }}"></script>
<script type="text/javascript">
    $(document).ready(function() {
        var t = $('#example').DataTable( {
            "columnDefs": [ {
                "searchable": false,
                "orderable": false,
                "targets": 0
            } ],
            "order": [[ 5, 'desc' ]],
            "lengthMenu": [[25, 50, 100, -1], [25, 50, 100, "All"]]
        } );

        t.on( 'order.dt search.dt', function () {
            t.column(0, {search:'applied', order:'applied'}).nodes().each( function (cell, i) {
                cell.innerHTML = i+1;
            } );
        } ).draw();
    } );
</script>
@endsection