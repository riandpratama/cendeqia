@extends('template.default')

@section('content')

<div class="main-card card">
    <div class="card-body">
        <div class="col-12">
            <div class="row">
	            @foreach($packages as $item)
                    @if ($item->question->count() > 0)
                    <div class="col-md-2">
                        <a href="{{ route('ranking.show', $item->id) }}" style="color: black; text-decoration: none;">
                        <div class="main-card mb-3 card bg-primary">
                            <div class="card-body">
                                <center>
                                    <h6 class="card-title" style="color: white;"><b>Paket</b></h6>
                                    <p style="font-size: 24px; color: white;"><b>{{ $item->name }}</b></p>
                                </center>
                            </div>
                        </div>
                        </a>
                    </div>
                    @else
    	            <div class="col-md-2">
    	            	{{-- <a href="" style="color: black; text-decoration: none;"> --}}
    	            	<div class="main-card mb-3 card bg-secondary">
    		                <div class="card-body">
                                <i class="pe-7s-lock " style="position: absolute; font-size: 15px; color: black; font-weight: bold;"></i>
    		                	<center>
    		                		<h6 class="card-title" style="color: white;"><b>Paket</b></h6>
    		                    	<p style="font-size: 24px; color: white;"><b>{{ $item->name }}</b></p>
    		                    </center>
    		                </div>
    		            </div>
    		            {{-- </a> --}}
    		        </div>
                    @endif
		        @endforeach
	    	</div>
	    </div>
    </div>
</div>
@endsection