@extends('template.default')
<style>
  .card-body {
    flex: 1 1 auto;
    padding: 0.5rem !important;
  }
</style>
@section('content')

<div class="">
    <div class="row">
      <div class="col-md-9">
          <div class="main-card mb-8 card">
              <div class="card-body text-center">
                <iframe src="https://drive.google.com/file/d/1sECMLXg4uB3tFWv431XzC3zT2HHMxGWG/preview" width="100%" height="500" frameborder="0"></iframe>
              </div>
          </div>
      </div>
      <div class="col-md-3">
        <div class="main-card mb-2 card">
            <div class="card-body text-center">
              <a href="{{route('theory.index')}}" class="btn btn-block" style="background-color: #3f6ad8; color: white;">
                <i class="metismenu-icon pe-7s-arrow-left"></i>
                KEMBALI
              </a>
              <a href="{{asset('theory/BUKU DIGITAL TKP.pdf')}}" download tabindex="0" class="btn btn-block" style="background-color: #FCB71A; color: black;">
                  <i class="metismenu-icon pe-7s-download"></i>
                  DOWNLOAD
              </a>
            </div>
        </div>
      </div>
    </div>
  </div>
@endsection