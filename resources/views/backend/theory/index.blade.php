@extends('template.default')
<style>
  .card-body {
    flex: 1 1 auto;
    padding: 0.5rem !important;
  }
</style>
@section('content')

<div class="">
    <div class="row">
      @if (Auth::user()->login_active === 'cpns')

      @foreach($theory as $item)
      <div class="col-md-3">
        <a href="{{route('theory.show', $item->id) }}">
          <div class="main-card mb-3 card">
              <div class="card-body">
                  <div id="carouselExampleControls1" class="carousel slide" data-ride="carousel">
                      <div class="carousel-inner">
                          <div class="carousel-item active">
                            <img class="d-block w-100" src="{{ config('app.image') }}/{{ $item->image_path }}" alt="First slide">
                          </div>
                      </div>
                  </div>
              </div>
          </div>
        </a>
      </div>
      @endforeach

      @endif
    </div>
</div>
@endsection