@extends('template.default')

@section('content')

<div class="app-page-title">
    <div class="page-title-wrapper">
        <div class="page-title-heading">
            <div class="page-title-icon">
                <i class="pe-7s-filter icon-gradient bg-warm-flame">
                </i>
            </div>
            <div>Total Nilai
            </div>
        </div>
        <div class="page-title-actions">
            <button type="button" data-toggle="tooltip" title="Example Tooltip" data-placement="bottom" class="btn-shadow mr-3 btn btn-dark">
                <i class="fa fa-star"></i>
            </button>
        </div>
    </div>
</div>

<div class="main-card card">
    <div class="card-body">
        <div class="col-12">
            <div class="row">
	            <div class="main-card mb-3 card col-12">
                    <div class="card-body">
                        <div class="text-center">
                            <div class="mb-2 mr-2 dropleft btn-group">
                                <p>
                                    <span style="font-size: 30px;font-weight: 500;color: #3f6ad8;">Nilai</span>
                                    <br>
                                    <span style="font-size: 40px; font-weight: bold; color: #f7b924;">{{ $total->total_point }}</span>
                                </p>
                            </div>
                        </div>
                        <br>
                        <div class="text-center">
                            @foreach($akumulasi as $item)
                            <div class="dropleft btn-group" style="padding-left: 50px; padding-right: 50px;">
                                <p>
                                    <span style="font-size: 22px;font-weight: 600;color: #3f6ad8;">{{ $item->jenis }}</span>
                                    <br>
                                    <span style="font-size: 40px; font-weight: bold; color: #f7b924;">{{ $item->total_point }}</span>
                                </p>
                            </div>
                            @endforeach
                        </div>
                        <br>
                        <div class="text-center">
                            @if ($detail->package->urut <= 33)
                                @if ($detail->twk()->sum('point') >= 60 && $detail->tiu()->sum('point') >= 80 && $detail->tkp()->sum('point') >= 126)
                                <p style="color: #3ac47d; font-weight: bold; font-size: 20px;"> SELAMAT ANDA LOLOS PASSING GRADE </p>
                                @else
                                <p style="color: red; font-weight: bold; font-size: 20px;"> ANDA BELUM LOLOS PASSING GRADE </p>
                                @endif
                            @elseif($detail->package->urut >= 34)
                                @if ($detail->twk()->sum('point') >= 65 && $detail->tiu()->sum('point') >= 80 && $detail->tkp()->sum('point') >= 166)
                                <p style="color: #3ac47d; font-weight: bold; font-size: 20px;"> SELAMAT ANDA LOLOS PASSING GRADE </p>
                                @else
                                <p style="color: red; font-weight: bold; font-size: 20px;"> ANDA BELUM LOLOS PASSING GRADE </p>
                                @endif
                            @else
                                <p style="color: red; font-weight: bold; font-size: 20px;"> Nothing </p>
                            @endif
                        </div>
                        <br>
                        <div class="text-center bloc_nsl_btn">
                            <a href="{{ route('history.quiz') }}" class="btn btn-primary">BERANDA</a>
                            @if (Auth::user()->login_active === 'cpns')
                            <a href="{{ route('history.show', [$detail->id, $detail->user_id]) }}" class="btn btn-warning">PEMBAHASAN</a>
                            @elseif (Auth::user()->login_active === 'kedinasan')
                            <a href="{{ route('history.show.kedinasan', [$detail->id, $detail->user_id]) }}" class="btn btn-warning">PEMBAHASAN</a>
                            @endif
                        </div>
                    </div>
                </div>
	    	</div>
	    </div>
    </div>
</div>
@endsection