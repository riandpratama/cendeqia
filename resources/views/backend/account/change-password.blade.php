@extends('template.default')

@section('content')

<div class="app-page-title">
    <div class="page-title-wrapper">
        <div class="page-title-heading">
            <div class="page-title-icon">
                <i class="pe-7s-user icon-gradient bg-strong-bliss">
                </i>
            </div>
            <div>Ganti Password
            </div>
        </div>   
    </div>
</div>

<div class="row">
    <div class="col-md-6">
        <div class="main-card mb-3 card">
            <div class="card-body">
                @foreach ($errors->all() as $error)
                    <p class="text-danger">{{ $error }}</p>
                 @endforeach
                <form action="{{ route('account.updateChangePassword') }}" method="POST" enctype="multipart/form-data">
                    @csrf
                    <div class="form-row">
                        <div class="col-md-12">
                            <div class="position-relative form-group">
                                <label for="password" class="">Password Saat ini</label>
                                <input id="password" type="password" class="form-control" name="current_password" autocomplete="current-password">
                            </div>
                            <div class="position-relative form-group">
                                <label for="new_password" class="">Password Baru</label>
                                <input id="new_password" type="password" class="form-control" name="new_password" autocomplete="current-password">
                            </div>
                            <div class="position-relative form-group">
                                <label for="new_confirm_password" class="">Konfirmasi Password Baru</label>
                                <input id="new_confirm_password" type="password" class="form-control" name="new_confirm_password" autocomplete="current-password">
                            </div>
                        </div>
                    </div>
                    <a href="{{ route('account.index') }}" class="mt-2 btn btn-danger" >BATAL</a>
                    <button type="submit" class="mt-2 btn btn-primary" onclick="return confirm('Anda yakin ingin mengupdate?')">SIMPAN</button>
                </form>
            </div>
        </div>
    </div>
</div>
@endsection

@section('js')
<script src="https://cdnjs.cloudflare.com/ajax/libs/jquery/1.9.1/jquery.min.js"></script>
<script>
    $('input[type=file]').change(function (e) {
        if(this.files[0].size > 1000000){
           alert("File melebihi kapasitas upload 1 Mb, Mohon Upload file di bawah 1 Mb.");
           this.value = "";
        };
    })
</script>

@endsection