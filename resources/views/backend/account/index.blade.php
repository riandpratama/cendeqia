@extends('template.default')

@section('content')

<div class="app-page-title">
    <div class="page-title-wrapper">
        <div class="page-title-heading">
            <div class="page-title-icon">
                <i class="pe-7s-user icon-gradient bg-strong-bliss">
                </i>
            </div>
            <div>Akun Profile </b>
            </div>
        </div>    
    </div>
</div>

<div class="row">
    <div class="col-md-12">
        <div class="main-card mb-3 card">
            <div class="card-body">
                <li class="list-group-item">
                    <div class="widget-content p-0">
                        <div class="widget-content-wrapper">
                            <div class="widget-content-left mr-3">
                                @if (is_null(Auth::user()->photo))
                                    <img width="152" height="152" class="rounded-circle" src="{{ asset('backend/assets/img/default.jpg') }}" alt="">
                                @else
                                    <img width="152" height="152" class="rounded-circle" src="{{ asset('storage/'.Auth::user()->photo) }}" alt="">
                                @endif
                            </div>
                            <div class="widget-content-left">
                                <div class="widget-heading">{{ Auth::user()->name }}</div>
                                <div class="widget-subheading">Member Cendeqia</div>
                            </div>
                        </div>
                    </div>
                </li>
                <hr>
                <a href="{{ route('account.edit') }}" class="mt-2 btn btn-primary">Edit Data</a>
                <a href="{{ route('account.editChangePassword') }}" class="mt-2 btn btn-info">Ganti Password</a>
                <hr>
                <div class="form-row">
                    <div class="col-md-6">
                        <div class="position-relative form-group">
                            <label for="email" class="">Email</label>
                            <input name="email" id="email" value="{{ Auth::user()->email }}" type="email" readonly="" class="form-control">
                        </div>
                        <div class="position-relative form-group">
                            <label for="gender" class="">Jenis Kelamin</label>
                            <select name="select" id="gender" class="form-control" disabled="">
                                <option value="Laki-laki" @if(Auth::user()->gender == 'Laki-laki') selected @endif>Laki-laki</option>
                                    <option value="Perempuan" @if(Auth::user()->gender == 'Perempuan') selected @endif>Perempuan</option>
                            </select>
                        </div>
                    </div>
                    <div class="col-md-6">
                        <div class="position-relative form-group">
                            <label for="name" class="">Nama Sosial Media Instagram</label>
                            <input name="name" id="name" type="text" value="{{ Auth::user()->social_media }}" readonly="" class="form-control">
                        </div>
                        <div class="position-relative form-group">
                            <label for="nomor_telephone" class="">Nomor Telepon</label>
                            <input name="nomor_telephone" id="nomor_telephone" value="{{ Auth::user()->phone }}" placeholder="Nomor Telepon" type="number" readonly="" class="form-control">
                        </div>
                    </div>
                </div>
                <div class="form-row">
                    <div class="col-md-2">
                        <div class="position-relative form-group">
                            <label for="province" class="">Provinsi</label>
                            <input name="province" id="province" type="text" value="{{ Auth::user()->province }}" class="form-control" readonly="">
                        </div>
                    </div>
                    <div class="col-md-4">
                        <div class="position-relative form-group">
                            <label for="city" class="">Kabupaten/Kota</label>
                            <input name="city" id="city" type="text" value="{{ Auth::user()->city }}" class="form-control" readonly="">
                        </div>
                    </div>
                    <div class="col-md-6">
                        <div class="position-relative form-group">
                            <label for="address" class="">Alamat Lengkap</label>
                            <input name="address" id="address" type="text" value="{{ Auth::user()->address }}" class="form-control" readonly="">
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection