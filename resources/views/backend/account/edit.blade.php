@extends('template.default')

@section('content')

<div class="app-page-title">
    <div class="page-title-wrapper">
        <div class="page-title-heading">
            <div class="page-title-icon">
                <i class="pe-7s-user icon-gradient bg-strong-bliss">
                </i>
            </div>
            <div>Edit Akun Profile </b>
            </div>
        </div> 
    </div>
</div>

<div class="row">
    <div class="col-md-12">
        <div class="main-card mb-3 card">
            <div class="card-body">
                <form action="{{ route('account.update') }}" method="POST" enctype="multipart/form-data">
                    @csrf
                    <div class="form-row">
                        <div class="col-md-6">
                            <div class="position-relative form-group">
                                <label for="name" class="">Nama Akun</label>
                                <input name="name" id="name" type="text" value="{{ Auth::user()->name }}" class="form-control">
                            </div>
                            <div class="position-relative form-group">
                                <label for="examplePassword11" class="">Jenis Kelamin</label>
                                <select name="gender" id="gender" class="form-control">
                                    <option value="Laki-laki" @if(Auth::user()->gender == 'Laki-laki') selected @endif>Laki-laki</option>
                                    <option value="Perempuan" @if(Auth::user()->gender == 'Perempuan') selected @endif>Perempuan</option>
                                </select>
                            </div>
                        </div>
                        <div class="col-md-6">
                            <div class="position-relative form-group">
                                <label for="email" class="">Email <i style="color: red; font-size: 10px;">*Email Tidak dapat diubah.</i></label>
                                <input name="email" id="email" value="{{ Auth::user()->email }}" type="email" readonly="" class="form-control">
                            </div>
                            <div class="position-relative form-group">
                                <label for="phone" class="">Nomor Telepon</label>
                                <input name="phone" id="phone" placeholder="Nomor Telepon" value="{{ Auth::user()->phone }}" type="number" class="form-control">
                            </div>
                        </div>
                    </div>
                    <div class="form-row">
                        <div class="col-md-2">
                            <div class="position-relative form-group">
                                <label for="province" class="">Provinsi</label>
                                <input name="province" id="province" type="text" value="{{ Auth::user()->province }}" class="form-control">
                            </div>
                        </div>
                        <div class="col-md-4">
                            <div class="position-relative form-group">
                                <label for="city" class="">Kabupaten/Kota</label>
                                <input name="city" id="city" type="text" value="{{ Auth::user()->city }}" class="form-control">
                            </div>
                        </div>
                        <div class="col-md-6">
                            <div class="position-relative form-group">
                                <label for="address" class="">Alamat Lengkap</label>
                                <input name="address" id="address" type="text" value="{{ Auth::user()->address }}" class="form-control">
                            </div>
                        </div>
                    </div>

                    <div class="form-row">
                        <div class="col-md-6">
                            <div class="position-relative form-group">
                                <label for="social_media" class="">Social Media (Instagram)</label>
                                <input name="social_media" id="social_media" type="text" value="{{ Auth::user()->social_media }}" class="form-control">
                            </div>
                        </div>
                        <div class="col-md-6">
                            <div class="position-relative form-group">
                                <label for="photo" class="">Upload Foto</label>
                                <input name="photo" id="photo" type="file" class="form-control-file" accept="mage/jpeg, image/png">
                                <small class="form-text text-muted">Upload Foto harus berekstensi png,jpg,jpeg dan maksimal size 1MB.</small>
                            </div>
                        </div>
                    </div>
                    <a href="{{ route('account.index') }}" class="mt-2 btn btn-danger" >BATAL</a>
                    <button type="submit" class="mt-2 btn btn-primary" onclick="return confirm('Anda yakin ingin mengupdate?')">SIMPAN</button>
                </form>
            </div>
        </div>
    </div>
</div>
@endsection

@section('js')
<script src="https://cdnjs.cloudflare.com/ajax/libs/jquery/1.9.1/jquery.min.js"></script>
<script>
    $('input[type=file]').change(function (e) {
        if(this.files[0].size > 1000000){
           alert("File melebihi kapasitas upload 1 Mb, Mohon Upload file di bawah 1 Mb.");
           this.value = "";
        };
    })
</script>

@endsection