<div class="app-sidebar sidebar-shadow bg-primary sidebar-text-light" style="background-color: #2A75AE !important;">
    <div class="app-header__logo">
        <div class="logo-src"></div>
        <div class="header__pane ml-auto">
            <div>
                <button type="button" class="hamburger close-sidebar-btn hamburger--elastic" data-class="closed-sidebar">
                    <span class="hamburger-box">
                        <span class="hamburger-inner"></span>
                    </span>
                </button>
            </div>
        </div>
    </div>
    <div class="app-header__mobile-menu">
        <div>
            <button type="button" class="hamburger hamburger--elastic mobile-toggle-nav">
                <span class="hamburger-box">
                    <span class="hamburger-inner"></span>
                </span>
            </button>
        </div>
    </div>
    <div class="app-header__menu">
        <span>
            <button type="button" class="btn-icon btn-icon-only btn btn-primary btn-sm mobile-toggle-header-nav">
                <span class="btn-icon-wrapper">
                    <i class="fa fa-ellipsis-v fa-w-6"></i>
                </span>
            </button>
        </span>
    </div>
    <div class="scrollbar-sidebar">
        <div class="app-sidebar__inner">
            <ul class="vertical-nav-menu">
                <li class="app-sidebar__heading">Home</li>
                @if (Auth::user()->login_active == 'cpns')
                <li>
                    <a href="{{ route('home') }}">
                        <i class="metismenu-icon pe-7s-rocket"></i>
                        Halaman Utama
                    </a>
                </li>
                @elseif (Auth::user()->login_active == 'kedinasan')
                <li>
                    <a href="{{ route('home') }}">
                        <i class="metismenu-icon pe-7s-rocket"></i>
                        Halaman Utama
                    </a>
                </li>
                @endif
                <li class="app-sidebar__heading">Menu Utama</li>
                <li>
                    <a href="{{ route('package.index') }}">
                        <i class="metismenu-icon pe-7s-display2"></i>
                        Paket Soal & Simulasi
                    </a>
                </li>
                <li>
                    <a href="{{ route('history.quiz') }}">
                        <i class="metismenu-icon pe-7s-copy-file"></i>
                        Riwayat & Pembahasan
                    </a>
                </li>
                @if (Auth::user()->login_active == 'cpns')
                <li>
                    <a href="{{ route('theory.index') }}">
                        <i class="metismenu-icon pe-7s-album"></i>
                        {{-- <i class="metismenu-icon pe-7s-door-lock"></i> --}}
                        Materi
                    </a>
                </li>
                <li>
                    <a>
                    {{-- <a href="{{ route('ranking.index') }}"> --}}
                        {{-- <i class="metismenu-icon pe-7s-graph2"></i> --}}
                        <i class="metismenu-icon pe-7s-door-lock"></i>
                        Peringkat
                    </a>
                </li>
                @endif

                <li class="app-sidebar__heading">Saran & Komentar</li>
                <li>
                    <a href="{{ route('kritik-saran.index') }}">
                        <i class="metismenu-icon pe-7s-eyedropper"></i>
                        Saran & Komentar
                    </a>
                </li>
                <li class="app-sidebar__heading">Akun</li>
                <li>
                    <a href="{{ route('account.index') }}">
                        <i class="metismenu-icon pe-7s-user"></i>
                        Kelola Akun
                    </a>
                </li>
                <li class="app-sidebar__heading">Logout</li>
                <li>
                    <a href="{{ route('logout') }}"
                        onclick="event.preventDefault();
                        document.getElementById('logout-forms').submit()" tabindex="0" class="btn" style="background-color: #FCB71A; color: black;">
                        <i class="metismenu-icon pe-7s-left-arrow"></i>
                        Logout
                    </a>
                    <form id="logout-forms" action="{{ route('logout') }}" method="POST" style="display: none;">
                        @csrf
                    </form>
                </li>
            </ul>
        </div>
    </div>
</div>