<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta http-equiv="Content-Language" content="en">
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8"/>
    <title>CENDEQIA</title>
    <meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no, shrink-to-fit=no" />
    <meta name="description" content="Cendeqia">
    <meta name="msapplication-tap-highlight" content="no">
    <link href="{{ asset('frontend/assets/image/ICON.png')}}" rel="icon">
    <link href="{{ asset('backend/assets/css/main.css') }}" rel="stylesheet">
    <style>
        .app-header__logo .logo-src {
            width: 136px !important;
            background: url(/frontend/assets/image/logo2.png);
            background-repeat: no-repeat;
        }
    </style>
</head>