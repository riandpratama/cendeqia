<div class="app-wrapper-footer">
    <div class="app-footer">
        <div class="app-footer__inner">
            <div class="app-footer-left">
                <ul class="nav">
                    <li class="nav-item">
                        <a href="javascript:void(0);" class="nav-link">
                            &copy; Copyright <strong><span> &nbsp;Cendeqia</span></strong>. All Rights Reserved
                        </a>
                    </li>
                    
                </ul>
            </div>
        </div>
    </div>
</div>