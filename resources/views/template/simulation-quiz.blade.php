<!DOCTYPE html>
<html lang="en">

@include('template.partials._head')

<body>

	<div class="app-container app-theme-white body-tabs-shadow closed-sidebar fixed-header">
		<div class="app-main">
			<div class="app-main__outer">
				<div class="app-main__inner">
					@yield('content')
				</div>
				@include('template.partials._footer')
			</div>
		</div>
	</div>

  @include('template.partials._javascript')

</body>

</html>