<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <link rel="icon" type="image/ico" href="http://cendeqia.com/dev/public/images/logo/favicon.ico">
    <link rel="stylesheet" href="http://cendeqia.com/dev/public/css/font-awesome.min.css">
    <!--[if IE]>
    <link rel="shortcut icon" href="/favicon.ico" type="image/vnd.microsoft.icon">
    <![endif]-->

    <!-- CSRF Token -->
    <meta name="csrf-token" content="4D0CCwlUea0UZqmzkqeh0BWFXfz9DkDlMzOkkXSm">
    
    <title>Cendeqia</title>

    <link rel="preconnect" href="https://fonts.gstatic.com">
    <link href="https://fonts.googleapis.com/css2?family=Lato:ital,wght@0,300;0,400;0,700;0,900;1,300;1,400;1,700&display=swap" rel="stylesheet">

    <!-- Styles -->
      <link href="http://cendeqia.com/dev/public/css/app.css" rel="stylesheet">
  <link rel="stylesheet" href="http://cendeqia.com/dev/public/css/font-awesome.min.css">
  <link rel="stylesheet" href="http://cendeqia.com/dev/public/css/styles.css">
</head>
<body>
    <div id="app" style="position: relative;">
        <nav class="navbar navbar-default navbar-static-top bg-white">
    <div class="nav-bar">
      <div class="container">
        <div class="row">
          <div class="col-md-6">
            <div class="navbar-header">
                            <h4 class="heading">
                <a href="http://cendeqia.com/dev/public" title="Cendeqia">
                  <img src="http://cendeqia.com/dev/public/images/logo/logo_1606580724Artboard 1.png" class="img-responsive" alt="Cendeqia">
                </a>
              </h4>
              
            </div>
          </div>
          <div class="col-md-6">
            <div class="collapse navbar-collapse" id="app-navbar-collapse">
              <ul class="nav navbar-nav navbar-right">
                <!-- Authentication Links -->
                                  <li class="dropdown">
                    <a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-expanded="false" aria-haspopup="true">
                      Prada Wisnu <span class="caret"></span>
                    </a>
                    <ul class="dropdown-menu">
                                              
                        <li><a href="http://cendeqia.com/dev/public/profil" title="Profil">Profil</a></li>
                                            <li>
                        <a href="http://cendeqia.com/dev/public/logout"
                        onclick="event.preventDefault();
                        document.getElementById('logout-form').submit();">
                        Logout
                      </a>
                      <form id="logout-form" action="http://cendeqia.com/dev/public/logout" method="POST" style="display: none;">
                        <input type="hidden" name="_token" value="4D0CCwlUea0UZqmzkqeh0BWFXfz9DkDlMzOkkXSm">
                      </form>
                      </li>
                    </ul>
                  </li>
                
                  <li><a href="http://cendeqia.com/dev/public/pages/about-us">TENTANG</a></li>
                  <li><a href="http://cendeqia.com/dev/public/faqs">FAQ</a></li>
                  <li><a href="http://cendeqia.com/dev/public/pages/contact-us">KONTAK</a></li>
              </ul>
            </div>
          </div>
        </div>
      </div>
    </div>
  </nav>

          
  <div class="outers_wrap_dashboard ins_page">
            <div class="container">
          <div class="row">
            <div class="col-md-12 back-white3">
              
              <div class="p-4 blocks_start_packet">
                <div class="py-2"></div>

                <div class="tops text-center">
                  <span class="tp1 d-block">History</span>
                  <span class="numbers d-block"></span>
                </div>
                <div class="py-2"></div>
                <div class="infos">
                  <span class="d-block text-center"></span>
                  <div class="py-1 my-1"></div>
                  <table class="table">
                    <thead>
                      <tr>
                        <th scope="col">#</th>
                        <th scope="col">Paket</th>
                        <th scope="col">Terakhir Tes</th>
                        <th scope="col">Aksi</th>
                      </tr>
                    </thead>
                    <tbody>
                      @foreach($data as $item)
                      <tr>
                        <th scope="row">{{ $loop->iteration }}</th>
                        <td>Paket {{ $item->package->name }}</td>
                        <td>{{ $item->end_time }}</td>
                        <td>
                          <a href="{{ route('review.quiz', [$item->id, Auth::user()->id]) }}">Detail</a>
                        </td>
                      </tr>
                      @endforeach
                    </tbody>
                  </table>
                  <div class="py-2"></div>

                  <div class="text-center bloc_nsl_btn">
                    <a href="{{ route('home') }}" class="btn btn-info">KEMBALI</a>
                    &nbsp;&nbsp;&nbsp;&nbsp;
                  </div>

                  <div class="clear"></div>
                </div>

                <div class="clear clearfix"></div>
              </div>

              <div class="py-3"></div>
            </div>
          </div>
      </div>
      </div>
        <br>
        <br>
    </div>
       <div class="flying_foot">
        <div class="container" >
            <div class="row">
                <div class="col-md-6">
                    © 2021 Cendeqia.id All Rights Reserved.
                </div>
                <div class="col-md-6">
                </div>
            </div>
        </div>
    </div>
    <!-- Scripts -->
    <script src="http://cendeqia.com/dev/public/js/app.js"></script>
    
  <script type="text/javascript">
     $( document ).ready(function() {
         $('.sessionmodal').addClass("active");
         setTimeout(function() {
             $('.sessionmodal').removeClass("active");
        }, 4500);

        $('body').addClass('home_dashboard');
      });
  </script>
  
<style type="text/css">
  .navbar-default{
    margin-bottom: 0 !important;
  }
</style>

</body>
</html>
